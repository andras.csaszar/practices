/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hetlinkrolfeladatrecursive;

/**
 *
 * @author andra
 */
public class HetLinkrolFeladatRecursive {

    static void threeGroupedNumber(int number) {

        while (number > 0) {
            //16077216 --> 16 077 216

            if (number / 1000 > 1000) {
                number = number / 1000;
            }

            System.out.print(number % 1000);
//            number = number / 1000;
            System.out.print(" ");

        }

    }

    static String formatInteger(int i) {
        String str = "" + i;

        if (i <=99) {
            str = "0"+str;
        }

    }

    static void writeNumberInFormat(int n) {

        int[] arr = new int[100];
        int index = 0;
        if (index == 0) {
            index = 1;
        }
        while (n > 0) {
            arr[index] = n % 1000;
            index++;
            n = n / 1000;
        }
        for (int i = index - 1; i >= 0; i--) {
            String str = arr[i] + " ";
            if (arr[i] <= 99 && i != index - 1) {

                str = "0" + str;
            }

            if (arr[i] <= 9 && i != index - 1) {

                str = "0" + str;
            }

            System.out.print(str);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*
         3. feladat:
         Három számjegyenkénti felosztás
         Írj függvényt, amely a paraméterként kapott pozitív egész számot három számjegyenként csoportosított
         formában írja ki. Pl.: 16 077 216. Használj rekurziót!
         */

        threeGroupedNumber(16077216);
        writeNumberInFormat(16077216);

    }

}
