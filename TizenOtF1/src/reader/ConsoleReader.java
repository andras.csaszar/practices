/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import store.StoreAdd;
import store.StringStore;

/**
 *
 * @author andra
 */
public class ConsoleReader {

    private static final String STOP = "exit";

    public void readByScanner(StoreAdd store) {
       try( Scanner scanner = new Scanner(System.in)){
        String word = null;

        do {
            word = scanner.next();
            store.add(word);
        } while (!STOP.equals(word));

       }
       /*scanner.close();*/
    }

    public void readyByInputStreamReader(StoreAdd store) {
        /*
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(System.in));
            while (true) {
                String line = br.readLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(ConsoleReader.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ex) {
                    Logger.getLogger(ConsoleReader.class.getName()).log(Level.SEVERE, null, ex);
                    System.out.println(ex);
                }
            }
        }
*/
        try (BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                String line = br2.readLine();
                if (line == null || STOP.equals(line)) {
                    break;
                }
                store.add(line);
            }
        } catch (IOException ex) {
            Logger.getLogger(ConsoleReader.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex);
        }

    }

}
