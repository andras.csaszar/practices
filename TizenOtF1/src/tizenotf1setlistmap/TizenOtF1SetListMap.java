/*
Felhasználó szavakat ad meg, az "exit" szóig.
ezeket tároljuk, 3 művelete van:
1. hány különböző szót adott meg és melyek ezek
2. Írjuk ki a megadott szavakat fordított sorrendben
3. Hosszak szerint csoportosítsuk a szavakat

 */
package tizenotf1setlistmap;

import reader.ConsoleReader;
import report.Reporting;
import store.StringStore;

/**
 *
 * @author andra
 */
public class TizenOtF1SetListMap {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        StringStore store = new StringStore();
        ConsoleReader reader = new ConsoleReader();
        reader.readyByInputStreamReader(store);
        Reporting report = new Reporting(store);
        report.generateReports();
        
    }
    
}
