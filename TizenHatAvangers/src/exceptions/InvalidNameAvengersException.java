/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author andra
 */
public class InvalidNameAvengersException extends AvengersException{

    public InvalidNameAvengersException(String message) {
        super(message);
    }
    
}
