/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.students.service;

import hu.braininghub.bh11.students.dto.StudentDto;
import hu.braininghub.bh11.students.mapper.StudentMapper;
import hu.braininghub.bh11.students.repository.StudentDao;
import hu.braininghub.bh11.students.repository.entity.Student;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.mock;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author andra
 */
@ExtendWith(MockitoExtension.class)
public class NeptunTest {

    @Mock
    private StudentDao dao;

    @Mock
    private StudentMapper mapper;

    private Neptun underTest;

    @BeforeEach
    void init() {
        underTest = new Neptun();
        underTest.setDao(dao);
        underTest.setMapper(mapper);
    }

    @Test
    void testGetStudentsWithEmptyList() {
        //Given
        Mockito.when(dao.findAll()).thenReturn(new ArrayList<>());
        //When
        List<StudentDto> students = underTest.getStudents();
        //Then
        Assertions.assertEquals(new ArrayList<>(), students);
        
    }

    @Test
    void testGetStudentsWith1Element() {
        //Given
        Student s = mock(Student.class);
        StudentDto dto = mock(StudentDto.class);
        Mockito.when(dao.findAll()).thenReturn(Arrays.asList(s));
        Mockito.when(mapper.toDto(s)).thenReturn(dto);

        //When
        List<StudentDto> students = underTest.getStudents();
        //Then
        Assertions.assertEquals(Arrays.asList(dto), students);
    }

    @Test
    void testGetStudentsWith2Element() {
        //Given
        Student s1 = mock(Student.class);
        Student s2 = mock(Student.class);
        StudentDto dto1 = mock(StudentDto.class);
        StudentDto dto2 = mock(StudentDto.class);

        Mockito.when(dao.findAll()).thenReturn(Arrays.asList(s1, s2));
        Mockito.when(Arrays.asList((mapper.toDto(s1)), (mapper.toDto(s2)))).thenReturn(Arrays.asList(dto1, dto2));

        //When 
        List<StudentDto> students = underTest.getStudents();

        //Then
        Assertions.assertEquals(Arrays.asList(dto1, dto2), students);
    }

    @Test
    void testGetStudentById() {
        //Given
        Student s1 = mock(Student.class);
        s1.setId(2);
        StudentDto dto1 = mock(StudentDto.class);
        Mockito.when(dao.findById(2).orElseThrow()).thenReturn(s1);
        Mockito.when(mapper.toDto(s1)).thenReturn(dto1);

        //When
        StudentDto studentDto = underTest.getStudentById(2);
        //Then
        Assertions.assertEquals(dto1, studentDto);
    }

}
