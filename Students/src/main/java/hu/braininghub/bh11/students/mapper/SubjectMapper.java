/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.students.mapper;

import hu.braininghub.bh11.students.dto.SubjectDto;
import hu.braininghub.bh11.students.repository.entity.Subject;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.inject.Inject;

/**
 *
 * @author andra
 */
@Singleton
@LocalBean
public class SubjectMapper implements Mapper<Subject, SubjectDto> {

    @Inject
    private StudentMapper studentMapper;

    @Override
    public Subject toEntity(SubjectDto dto) {
        Subject s = new Subject();
        s.setId(dto.getId());
        s.setDescription(dto.getDescription());
        s.setName(dto.getName());

        return s;
        //Házi feladat
    }

    @Override
    public SubjectDto toDto(Subject entity) {
       SubjectDto s = new SubjectDto();
       s.setDescription(entity.getDescription());
       s.setId(entity.getId());
       s.setName(entity.getName());
       
       
        
        return s;
    }

}
