/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.students.servlet;

import hu.braininghub.bh11.students.dto.StudentDto;
import hu.braininghub.bh11.students.service.Neptun;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author andra
 */
@WebServlet(name = "StudentServlet", urlPatterns = "/students")
public class StudentServlet extends HttpServlet {

    private static final String NUMBER_OF_REQ = "nrReq";

    @Inject
    private Neptun neptun;
    private int clientCounter;
    private String lastSession = "";
    private Map<String, Integer> clientCounters = new HashMap<>();

    @Override
    public void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        if (session.getAttribute(NUMBER_OF_REQ) == null) {
            session.setAttribute(NUMBER_OF_REQ, 0);

        } else {
            session.setAttribute(NUMBER_OF_REQ, (int) session.getAttribute(NUMBER_OF_REQ) + 1);

        }
        super.service(req, resp);
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<StudentDto> students = neptun.getStudents();

        clientCounters.put(req.getSession().getId(), clientCounter);

        if (lastSession.equals(req.getSession().getId())) {
            clientCounter++;
        }
        lastSession = req.getSession().getId();

        req.setAttribute("clientCounter", clientCounter);
        req.setAttribute("students", students);
        req.getRequestDispatcher("/WEB-INF/students.jsp").forward(req, resp);

    }
}
