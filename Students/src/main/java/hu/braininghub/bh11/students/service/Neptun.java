/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.students.service;

import hu.braininghub.bh11.students.dto.StudentDto;
import hu.braininghub.bh11.students.mapper.StudentMapper;
import hu.braininghub.bh11.students.repository.StudentDao;
import hu.braininghub.bh11.students.repository.entity.Student;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import javax.ejb.Singleton;
import javax.inject.Inject;

/**
 *
 * @author andra
 */
@Singleton
public class Neptun {

    @Inject
    private StudentDao dao;

    @Inject
    private StudentMapper mapper;

    public List<StudentDto> getStudents() {

        Iterable<Student> students = dao.findAll();

        List<StudentDto> dtos = new ArrayList<>();
        students.forEach(student -> dtos.add(mapper.toDto(student)));
        return dtos;
    }

    public StudentDto getStudentById(int id) throws NoSuchElementException {
      
        return mapper.toDto(dao.findById(id).orElseThrow());
//HF
    }

    protected void setDao(StudentDao dao) {
        this.dao = dao;
    }

    protected void setMapper(StudentMapper mapper) {
        this.mapper = mapper;
    }

}
