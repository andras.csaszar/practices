/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.students.servlet;

import hu.braininghub.bh11.students.service.Neptun;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author andra
 */
@WebServlet(name = "StudentDetailsServlet", urlPatterns = "/details")
public class StudentDetailsServlet extends HttpServlet {
@Inject
Neptun service;

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    
        int id = Integer.parseInt(req.getParameter("id"));
        req.setAttribute("studentById", service.getStudentById(id));
        
        //id-t el kell küldeni a service-hez, ami majd elmegy a dao-hoz
        // az visszajön a service-be, vissza jön a servlet-be, és elmegy a jsp-be
        
        
            req.getRequestDispatcher("/WEB-INF/studentDetail.jsp").forward(req, resp);
    }
}
