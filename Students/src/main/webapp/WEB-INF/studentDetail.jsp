<%-- 
    Document   : studentDetail
    Created on : 2020. márc. 1., 14:30:02
    Author     : andra
--%>

<%@page import="hu.braininghub.bh11.students.dto.StudentDto"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Student</h1>
        
        <c:set var="stud" value="${'studentById'}"/>
       
        <c:out value="${stud.name}"/>

        <h1>Details</h1>

        <table class="table table-striped table-dark">
            <tr>
                <td>
                    <c:out value="${stud}"/> 
                </td>

            </tr>


        </table>

    </body>
</html>
