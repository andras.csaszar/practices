/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h3f9;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class H3F9 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //9. A felhasználótól kérj be 2 egész számot 10 és 500 között.
        //Határozd meg a két pozitív egész szám legnagyobb közös osztóját!
        Scanner sc = new Scanner(System.in);
        int number1 = 0;
        int number2 = 0;
        boolean isNumber1Valid;
        boolean isNumber2Valid;
        System.out.println("Adjon meg 2 számot 10 és 500 között, és megmondom azok legnagyobb közös osztóját.");
        do {
            System.out.println("Adja meg az első számot 10 és 500 között!");
            if (sc.hasNextInt()) {
                number1 = sc.nextInt();
                if (number1 >= 10 && number1 <= 500) {
                    isNumber1Valid = true;
                } else {
                    isNumber1Valid = false;
                    System.out.println("10 és 500 között kell lennie!");
                }
            } else {
                isNumber1Valid = false;
                sc.next();
            }

        } while (!isNumber1Valid);

        do {
            System.out.println("Adja meg a második számot 10 és 500 között!");
            if (sc.hasNextInt()) {
                number2 = sc.nextInt();
                if (number2 >= 10 && number2 <= 500) {
                    isNumber2Valid = true;
                } else {
                    isNumber2Valid = false;
                    System.out.println("10 és 500 között kell lennie!");
                }
            } else {
                isNumber2Valid = false;
                sc.next();
            }

        } while (!isNumber2Valid);

        System.out.println("Number1: " + number1);
        System.out.println("Number2: " + number2);

        int maxCommonDivisor = 1;
        for (int i = 1; i <= (Math.min(number1, number2)); i++) {
            //todo: i prímszám-e
            int countDivisorOfi=0;
            for (int j = 1; j <=i;j++){
            if(i%j==0){countDivisorOfi++;}
            }
            
            //Ha countDivisorOfi <=2 ( akkor prím)
            
            
            if (number1 % i == 0 && number2 % i == 0) {

                if (countDivisorOfi<=2 && i > maxCommonDivisor) {
                    maxCommonDivisor = i;
                }
            }
        }

        System.out.println("A legnagyobb közös osztó:" + maxCommonDivisor);
        }
    }


