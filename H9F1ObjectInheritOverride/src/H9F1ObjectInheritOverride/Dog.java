/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package H9F1ObjectInheritOverride;

/**
 *
 * @author andra
 */
public class Dog extends Animal {

    static int countOfDogs = 0;
    static Dog[] dogs;
    private String color;

    public Dog(String name, int age, int countOfLegs, String color) {
        super(name, age, countOfLegs);
        setColor(color);
        countOfDogs++;
       
    }

    @Override
    public void say() {
        System.out.println("I am a dog from animal. So Hello! wau");
    }

    @Override
    public String toString() {
        return "This is a dog, with properties: (" + getName() + ", " + "age, " + getCountOfLegs() + ", " + color + "\n";
    }

    public String getColor() {
        return this.color;
    }

    public void setColor(String color) {
        this.color = color;
    }

}
