/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package H9F1ObjectInheritOverride;

/**
 *
 * @author andra
 */
public class Cat extends Animal {

    static int countOfCats =0;
    private boolean domestic;
    

    public Cat(String name, int age, int countOfLegs, boolean domestic) {
        super(name, age, countOfLegs);
        setDomestic(domestic);
        countOfCats++;
        
    }
    
    @Override
    public void say(){
        System.out.println("I am a cat from animal. So Hello! meow");
    }
    
    @Override
    public String toString(){
      return  "This is a cat, with properties: ("+getName()+", "+"age, "+getCountOfLegs()+", "+domestic+"\n";
    }

    public boolean getDomestic() {
        return this.domestic;
    }

    public void setDomestic(boolean domestic) {
        this.domestic = domestic;
    }

}
