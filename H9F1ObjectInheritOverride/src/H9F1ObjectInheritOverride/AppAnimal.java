package H9F1ObjectInheritOverride;

import java.util.Scanner;

public class AppAnimal {

    static final int NUMBER_OF_ANIMALS = 10;
    static Animal[] anims;

//    static void createArray() {
//        animals = new Animal[NUMBER_OF_ANIMALS];
//    }
//    static void fillUpAnimals() {
//
//    }
    static void addElementsToArray() {

    }

    static void sayHello() {
        for (Animal anim : Animal.animals) {
            anim.say();
        }
    }

    public static void main(String[] args) {
//        createArray();
        anims = new Animal[NUMBER_OF_ANIMALS];

        Animal dog1 = new Dog("Dog1", 10, 4, "White");
        dog1.addToAnimals();
        Animal dog2 = new Dog("Dog2", 5, 4, "Grey");
        dog2.addToAnimals();
        Animal dog3 = new Dog("Dog3", 3, 2, "Brown");
        dog3.addToAnimals();

        Animal cat1 = new Cat("Cat1", 6, 4, true);
        cat1.addToAnimals();
        Animal cat2 = new Cat("Cat2", 1, 4, false);
        cat2.addToAnimals();
        Animal cat3 = new Cat("Cat3", 3, 4, true);
        cat3.addToAnimals();

        Animal pig1 = new Pig("Pig1", 7, 4, 200.5);
        pig1.addToAnimals();
        Animal pig2 = new Pig("Pig2", 10, 3, 321.8);
        pig1.addToAnimals();
        Animal pig3 = new Pig("Pig3", 11, 4, 400.0);
        pig1.addToAnimals();
        Animal pig4 = new Pig("Pig4", 2, 3, 250.0);
        pig1.addToAnimals();

        sayHello();
    }
}
