/*
 Négy osztály: 
 - Animal (name:String, age:int, countOfLegs:int, method: void say()) 
 - Dog, Animal utód (color: String)
 - Cat, Animal utód (domestic: boolean)
 - Kedvenc állatod, Animal utód (légy kreatív)
 */
package H9F1ObjectInheritOverride;

/**
 *
 * @author andra
 */
public class Animal {

    static int countOfAnimals = 0;
//    static Animal[] animals;
    private String name;
    private int age;
    private int countOfLegs;
    static public Animal[] animals = new Animal[10];

    public Animal(String name, int age, int countOfLegs) {
        setName(name);
        setAge(age);
        setCountOfLegs(countOfLegs);
        countOfAnimals++;

    }

    public void say(){
    
        System.out.println("I am animal. So Hello!");
    
    }

    @Override
    public String toString() {
        return "This is an animal, with properties: ("+name+", "+"age, "+countOfLegs+"\n";
    }
    
    
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getCountOfLegs() {
        return this.countOfLegs;
    }

    public void setCountOfLegs(int countOfLegs) {
        this.countOfLegs = countOfLegs;
    }
    
//    public void createArrayFromAnimalsExisting(){
//    animals = new Animal[countOfAnimals];
//    }
    public void addToAnimals(){
     
     animals[countOfAnimals-1] = this;
    }
   

}
