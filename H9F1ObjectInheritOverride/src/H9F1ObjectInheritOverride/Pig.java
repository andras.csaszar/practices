/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package H9F1ObjectInheritOverride;

/**
 *
 * @author andra
 */
public class Pig extends Animal {

    static int countOfPigs = 0;
    private double weightKG;

    public Pig(String name, int age, int countOfLegs, double weightKG) {
        super(name, age, countOfLegs);
        setWeightKG(weightKG);
        countOfPigs++;
     
    }

    @Override
    public void say() {
        System.out.println("I am a pig from animbal. So Hello! hröff");
    }

    @Override
    public String toString() {
        return  "This is a pig, with properties: ("+getName()+", "+"age, "+getCountOfLegs()+", "+weightKG+"\n";
    }

    public double getWeightKG() {
        return weightKG;
    }

    public void setWeightKG(double weightKG) {
        this.weightKG = weightKG;
    }

}
