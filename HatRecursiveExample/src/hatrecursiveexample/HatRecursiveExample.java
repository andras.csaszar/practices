/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hatrecursiveexample;

/**
 *
 * @author andra
 */
public class HatRecursiveExample {

    static void m(int a) {
        if (a == 0) {
            return;
        }

        m(--a);
        System.out.println(a);
        
        /*Így előbb hívja meg az összes önmagát, és utána elkezdi kiírni. Tehát
        ha megfordítod, fordított sorrendbe csorognak vissza a számok
        m(--a);
        System.out.println(a);
        */
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        m(10);

    }

}
