//<Császár András BH11>;
package bh11zh01;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class Runners {

    final static Scanner SCANNER = new Scanner(System.in);
    final static int fullRunner = 1;
    final static double halfRunner = 2;
    final static int threePartRunner = 3;

    static int actualApplication = -1;
    static int maxNumberOfFullRunnerPlaces = -1;
    static int maxNumberOfThreePartRunnerPlaces = -1;
    static int maxNumberOfHalfPartRunnerPlaces = -1;
    static boolean isFirstApplication = true;
    static int numberOfFullRunnerPlaces = -1;
    static int numberOfThreePartRunnerPlaces = -1;
    static int numberOfHalfRunnerPlaces = -1;
    static int maxOfApplicants = -1;
    static int availableKiloMeters = -1;

    static boolean isValidInputMaxApplicants(int input) {
        return input > 0;
    }

    static boolean isValidInputDistances(int input) {
        return input == 1 || input == 2 || input == 3;
    }

    static int getNumberFromUser() {

        int number = -1;

        do {
            if (SCANNER.hasNextInt()) {
                number = SCANNER.nextInt();
            } else {
                SCANNER.next();
            }

        } while (!isValidInputMaxApplicants(number));
        return number;
    }

    static int getDistanceFromUser() {
        System.out.print("Which distance you apply for?\n 1 - 21 km; 2 - 10.5 km; 3 - 7 km\n");
        int number = -1;

        do {
            if (SCANNER.hasNextInt()) {
                number = SCANNER.nextInt();
            } else {
                SCANNER.next();
            }

        } while (!isValidInputDistances(number));

        return number;

    }

    static void setMaxOfApplicants() {
        System.out.println("Please give me the maximum number of applicants!");
        maxOfApplicants = getNumberFromUser();

    }

    static void calculateInitialMaxPlaces() {

        if (isFirstApplication) {
            maxNumberOfFullRunnerPlaces = maxOfApplicants;
            if (maxOfApplicants % 3 == 0) {
                maxNumberOfThreePartRunnerPlaces = maxOfApplicants / 3;
            }
            if (maxOfApplicants % 2 == 0) {
                maxNumberOfHalfPartRunnerPlaces = maxOfApplicants / 2;
            }
        }
    }
//
//    static void setAvailableKiloMeters() {
//        availableKiloMeters = maxOfApplicants * 21;
//    }

    static void startApplication() {
        actualApplication = getDistanceFromUser();

    }

    static void getNewMaximumPlaces() {

        if ((numberOfFullRunnerPlaces + numberOfHalfRunnerPlaces) > 0 && (maxOfApplicants - (numberOfFullRunnerPlaces + numberOfHalfRunnerPlaces)) > 3) {
            numberOfThreePartRunnerPlaces = (maxOfApplicants - (numberOfFullRunnerPlaces + numberOfHalfRunnerPlaces)) / 3;

        } else {
            numberOfThreePartRunnerPlaces = 0;
        }

        if ((numberOfFullRunnerPlaces + numberOfThreePartRunnerPlaces) > 0 && (maxOfApplicants - ((numberOfFullRunnerPlaces + numberOfThreePartRunnerPlaces))) > 2) {
            numberOfHalfRunnerPlaces = (maxOfApplicants - ((numberOfFullRunnerPlaces + numberOfThreePartRunnerPlaces))) / 2;
        } else {
            numberOfHalfRunnerPlaces = 0;
        }

        if ((numberOfHalfRunnerPlaces + numberOfThreePartRunnerPlaces) > 0 && (maxOfApplicants - ((numberOfHalfRunnerPlaces + numberOfThreePartRunnerPlaces))) > 0) {
            numberOfHalfRunnerPlaces = (maxOfApplicants - ((numberOfHalfRunnerPlaces + numberOfThreePartRunnerPlaces)));
        } else {
            numberOfHalfRunnerPlaces = 0;
        }
    }

    static void recalculatePlacesAtApplication() {
        if (isFirstApplication) {

            switch (actualApplication) {
                case 1:
                    getNewMaximumPlaces();
                    numberOfFullRunnerPlaces = maxNumberOfFullRunnerPlaces - 1;
                    break;
                case 2:
                    getNewMaximumPlaces();
                    numberOfHalfRunnerPlaces = maxNumberOfHalfPartRunnerPlaces - 1;
                    break;

                case 3:
                    getNewMaximumPlaces();
                    numberOfThreePartRunnerPlaces = maxNumberOfThreePartRunnerPlaces - 1;
                    break;
            }
            isFirstApplication = false;

        } else {

            switch (actualApplication) {
                case 1:

                    numberOfFullRunnerPlaces--;
                    break;
                case 2:

                    numberOfHalfRunnerPlaces--;
                    break;
                case 3:

                    numberOfThreePartRunnerPlaces--;
                    break;
            }
        }
    }

    static void application() {
        calculateInitialMaxPlaces();
        //printField();
        while ((true)) {

            startApplication();

            recalculatePlacesAtApplication();
            printPlaces();
        }
        SCANNER.close();
        printPlaces();

    }

    static void printPlaces() {
        System.out.println("FullRunner places (21km): " + numberOfFullRunnerPlaces);
        System.out.println("HalfRunner places (10.5km): " + numberOfHalfRunnerPlaces);
        System.out.println("FullThreePartRunner places (7km): " + numberOfThreePartRunnerPlaces);
    }

    public static void main(String[] args) {

        setMaxOfApplicants();
        application();

    }
}
