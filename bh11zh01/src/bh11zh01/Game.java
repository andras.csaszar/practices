//<Császár András BH11>;
package bh11zh01;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class Game {

    final static int NUMBER_OF_PLAYER = 1;
    final static int NUMBER_OF_LETTERS = 9;
    final static int SIZE = 10;
    final static int LOWERLETTERS_MIN = 97;
    final static int LOWERLETTERS_MAX = 123; //this is z +1

    final static char PLAYER = '@';
    final static char EMPTY = '\0';
    final static Scanner SCANNER = new Scanner(System.in);

    static boolean steppedOnBannedItem = false;
    static int playerX = 0;
    static int playerY = 0;
    static int minOfLetterOnField = 5000;
    static int numberOfSteps = 0;
    static char bannedSteppedItem = '~';
    static char[][] field = new char[SIZE][SIZE];

    static void placeAnItem(int x, int y, char character) {
        field[x][y] = character;
    }

    static void setPlayer() {
        field[playerX][playerY] = PLAYER;
    }

    //ASCII [97-123[ -- these are the letters in ASCII (lower case)
    static int drawNumberInRange(int from, int to) {
        return (int) (Math.random() * (to - from) + from);
    }

    static char drawCharacter() {
        return (char) (drawNumberInRange(LOWERLETTERS_MIN, LOWERLETTERS_MAX));
    }

    static void refreshMinOfLettersOnField() {
        int minOfLetters = Integer.MAX_VALUE;
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (/*field[i][j] != PLAYER && field[i][j] != EMPTY*/
                        field[i][j] >= LOWERLETTERS_MIN && field[i][j] < LOWERLETTERS_MAX && field[i][j] <= minOfLetters) {
                    minOfLetters = field[i][j];
                }
            }
        }
        minOfLetterOnField = minOfLetters;
    }

    static void setInitialPositions(int numberOfInstance, boolean isRepeatable) {
        for (int i = 0; i < numberOfInstance; i++) {
            int x = drawNumberInRange(0, SIZE);
            int y = drawNumberInRange(0, SIZE);

            if (field[x][y] == EMPTY) {
                placeAnItem(x, y, drawCharacter());
            } else if (field[x][y] == drawCharacter() && isRepeatable) {
                placeAnItem(x, y, drawCharacter());
            } else if (field[x][y] == drawCharacter() && !isRepeatable) {
                i--;
            }
        }
    }

    static boolean isPlayerInLastRow() {
        return playerX == field.length - 1;
    }

    static boolean isPlayerInFirstRow() {
        return playerX == 0;
    }

    static boolean isPlayerInLastCol() {
        return playerY == field[0].length - 1;
    }

    static boolean isPlayerInFirstCol() {
        return playerY == 0;
    }

    static void handleUp() {

        if (!isPlayerInFirstRow()) {

            if (isBannedItemOnNextPlace(playerX - 1, playerY)) {
                bannedSteppedItem = field[playerX - 1][playerY];
                placeAnItem(playerX, playerY, EMPTY);
                playerX--;
                placeAnItem(playerX, playerY, PLAYER);
                steppedOnBannedItem = true;

            } else {

                if (steppedOnBannedItem) {
                    placeAnItem(playerX, playerY, bannedSteppedItem);
                } else {
                    placeAnItem(playerX, playerY, EMPTY);
                }

                playerX--;
                placeAnItem(playerX, playerY, PLAYER);
                steppedOnBannedItem = false;
            }

        }
    }

    static void handleDown() {
        if (!isPlayerInLastRow()) {

            if (isBannedItemOnNextPlace(playerX + 1, playerY)) {
                bannedSteppedItem = field[playerX + 1][playerY];
                placeAnItem(playerX, playerY, EMPTY);
                playerX++;
                placeAnItem(playerX, playerY, PLAYER);
                steppedOnBannedItem = true;

            } else {

                if (steppedOnBannedItem) {
                    placeAnItem(playerX, playerY, bannedSteppedItem);
                } else {
                    placeAnItem(playerX, playerY, EMPTY);
                }

                playerX++;
                placeAnItem(playerX, playerY, PLAYER);
                steppedOnBannedItem = false;
            }

        }

    }

    static void handleLeft() {
        if (!isPlayerInFirstCol()) {

            if (isBannedItemOnNextPlace(playerX, playerY - 1)) {
                bannedSteppedItem = field[playerX][playerY - 1];
                placeAnItem(playerX, playerY, EMPTY);
                playerY--;
                placeAnItem(playerX, playerY, PLAYER);
                steppedOnBannedItem = true;

            } else {

                if (steppedOnBannedItem) {
                    placeAnItem(playerX, playerY, bannedSteppedItem);
                } else {
                    placeAnItem(playerX, playerY, EMPTY);
                }

                playerY--;
                placeAnItem(playerX, playerY, PLAYER);
                steppedOnBannedItem = false;
            }

        }

    }

    static void handleRight() {
        if (!isPlayerInLastCol()) {

            if (isBannedItemOnNextPlace(playerX, playerY + 1)) {
                bannedSteppedItem = field[playerX][playerY + 1];
                placeAnItem(playerX, playerY, EMPTY);
                playerY++;
                placeAnItem(playerX, playerY, PLAYER);
                steppedOnBannedItem = true;

            } else {

                if (steppedOnBannedItem) {
                    placeAnItem(playerX, playerY, bannedSteppedItem);
                } else {
                    placeAnItem(playerX, playerY, EMPTY);
                }

                playerY++;
                placeAnItem(playerX, playerY, PLAYER);
                steppedOnBannedItem = false;
            }

        }

    }

    static void performMove(char character) {

        switch (character) {

            case 'w':
                handleUp();
                numberOfSteps++;
                break;
            case 's':
                handleDown();
                numberOfSteps++;
                break;
            case 'a':
                handleLeft();
                numberOfSteps++;
                break;
            case 'd':
                handleRight();
                numberOfSteps++;
                break;

        }

    }

    static void printField() {
        System.out.println("----------------" + "Steps taken: " + numberOfSteps + " Next char: " + (char) minOfLetterOnField);
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (field[i][j] == EMPTY) {
                    System.out.print("_");
                } else {
                    System.out.print(field[i][j]);
                }
            }
            System.out.println();
        }
        System.out.println("----------------");
    }

    static boolean hasFoundCharacterOnField() {

        boolean characterFound = false;
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (field[i][j] != EMPTY && field[i][j] != PLAYER) {
                    characterFound = true;
                }
            }
        }
        return characterFound;
    }

    static boolean isBannedItemOnNextPlace(int x, int y) {
        return hasFoundCharacterOnField() && field[x][y] > minOfLetterOnField;
    }

    static boolean isValidInput(int inputCharacter) {
        return inputCharacter == 'A' || inputCharacter == 'a' || inputCharacter == 'S'
                || inputCharacter == 's' || inputCharacter == 'W' || inputCharacter == 'w'
                || inputCharacter == 'D' || inputCharacter == 'd';
    }

    static int getMoveFromUser() {
        //L - 76, l - 108
        //R - 82, r - 114 (n+32)
        //U - 85
        //D - 68
        int characterASCII = -1;

        do {
            if (SCANNER.hasNext()) {
                characterASCII = SCANNER.next().charAt(0);
            } else {
                SCANNER.nextLine();
            }

        } while (!isValidInput(characterASCII));
        return characterASCII;
    }

    static void play() {

        //printField();
        while (hasFoundCharacterOnField()) {
            refreshMinOfLettersOnField();
            printField();
            performMove((char) getMoveFromUser());
        }
        SCANNER.close();
        printField();
    }

    public static void main(String[] args) {
        System.out.println(PLAYER);

        setPlayer();
        setInitialPositions(NUMBER_OF_LETTERS, true);
        play();

    }
}
