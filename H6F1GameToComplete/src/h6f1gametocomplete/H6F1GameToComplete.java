/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h6f1gametocomplete;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class H6F1GameToComplete {

    static final Scanner SCANNER = new Scanner(System.in);

    static final char GOOD_BOMB = 'D';
    static final char BAD_BOMB = '*';
    static final char PLAYER = '8';
    static final char PRESENT = '=';

    static final int MIN_SIZE = 10;
    static final int MAX_SIZE = 15;

    static final int MIN_NUMBER_OF_BOMBS = 8;
    static final int MAX_NUMBER_OF_BOMBS = 20;
    static final int NUMBER_OF_PRESENTS = 3;

    static final char EMPTY = '\0';

    static int lifePoint = 5;
    static int playerX = 0;
    static int playerY = 0;

    static int getNumber(int from, int to) {
       // int number = from - 1; // nem tudjuk, mit kapunk, a range-nél legyen kisebb-- hogy invalid legyen

        //boolean isValidNumber = false;
        // int number;
        do {
            System.out.printf("Give me a number between %d and %d. ", from, to);
            if (SCANNER.hasNextInt()) {
                int number = SCANNER.nextInt();
                if (number >= from && number <= to) {
                    //isValidNumber = true;
                    return number;
                }
            } else {
                swallowString();
            }
        } while (true /*!isValidNumber*/);

    }

    static void swallowString() {
        SCANNER.next();
    }

    static void printField(char[][] field) {
        System.out.printf("Life: %d\n", lifePoint);
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (field[i][j] == EMPTY) {
                    System.out.print(" ");
                } else {
                    System.out.print(field[i][j]);
                }

            }
            System.out.println();
        }

    }

    static void play(int row, int col) {
        char[][] field = new char[row][col];
        printField(field);
        putPlayer(field);
        putBombs(field);
        step(field);

    }

    static void step(char[][] field) {

        while (isPlayerAlive() && !isPlayerInTheLastRow(field)) {
            char c = SCANNER.next().charAt(0);
            switch (c) {
                case 'r':
                    stepRight(field);
                    break;
                case 'd':
                    stepDown(field);
                    break;
                case 'u':
                    stepUp(field);
                    break;
                case 'l':
                    stepLeft(field);
                    break;

            }
            printField(field);
        }

    }

    static boolean isPlayerAlive() {
        return lifePoint > 0;
    }

    static void stepRight(char[][] field) {
        if (!isPlayerInTheLastColumn(field)) {
            handleBombs(field, playerX, playerY + 1);

            if (isPlayerAlive()) {
                //field[playerX][playerY] = EMPTY;
                setPlaceToEmpty(field, playerX, playerY);

                playerY++;
                //field[playerX][playerY] = PLAYER;
                setPlaceToPlayer(field, playerX, playerY);
            }

        }
    }

    static void stepLeft(char[][] field) {
        if (!isPlayerInTheFirstColumn()) {
            handleBombs(field, playerX, playerY - 1);

            if (isPlayerAlive()) {
                //field[playerX][playerY] = EMPTY;
                setPlaceToEmpty(field, playerX, playerY);
                playerY--;
                //field[playerX][playerY] = PLAYER;
                setPlaceToPlayer(field, playerX, playerY);
            }
        }
    }

    static boolean isPlayerInTheFirstColumn() {
        return playerY == 0;
    }

    static boolean isPlayerInTheFirstRow() {
        return playerX == 0;
    }

    static void handleBombs(char[][] field, int x, int y) {
        if (field[x][y] == GOOD_BOMB) {
            lifePoint--;
        } else if (field[x][y] == BAD_BOMB) {
            lifePoint = 0;
        } else if (field[x][y] == PRESENT) {
            lifePoint++;
        }

    }

    static void stepUp(char[][] field) {
        if (!isPlayerInTheFirstRow()) {
            handleBombs(field, playerX - 1, playerY);

            if (isPlayerAlive()) {
                //field[playerX][playerY] = EMPTY;
                setPlaceToEmpty(field, playerX, playerY);
                playerX--;
                //field[playerX][playerY] = PLAYER;
                setPlaceToPlayer(field, playerX, playerY);
            }
        }

    }

    static void stepDown(char[][] field) {
        if (!isPlayerInTheLastRow(field)) {
            handleBombs(field, playerX + 1, playerY);

            if (isPlayerAlive()) {
                //field[playerX][playerY] = EMPTY;
                setPlaceToEmpty(field, playerX, playerY);
                playerX++;
                //field[playerX][playerY] = PLAYER;
                setPlaceToPlayer(field, playerX, playerY);
            }

        }
    }

    static void setPlaceToEmpty(char[][] field, int x, int y) {
        field[x][y] = EMPTY;

    }

    static void setPlaceToPlayer(char[][] field, int x, int y) {
        field[x][y] = PLAYER;
    }

    static boolean isPlayerInTheLastRow(char[][] field) {
        return playerX == field.length - 1;

    }

    static boolean isPlayerInTheLastColumn(char[][] field) {
        return playerY == field[0].length - 1;

    }

    static void putPlayer(char[][] field) {
        field[playerX][playerY] = PLAYER;
    }

    static void putBombs(char[][] field) {

        int numberOfBombs = getNumber(MIN_NUMBER_OF_BOMBS, MAX_NUMBER_OF_BOMBS);
        int numberOfGoodBombs = calculateNumberOfGoodBombs(numberOfBombs);
        int numberOfBadBombs = numberOfBombs - numberOfGoodBombs;
        int numberOfPresents = NUMBER_OF_PRESENTS;

        setBombs(field, numberOfGoodBombs, GOOD_BOMB);
        setBombs(field, numberOfBadBombs, BAD_BOMB);
        setBombs(field, numberOfPresents, PRESENT);

        printField(field);

    }

    static int calculateNumberOfGoodBombs(int numberOfBombs) {
        return numberOfBombs / 3;

    }

    /**
     * This method returns a random number betweeen from and to excluding to
     *
     * @param from
     * @param to
     * @return
     */
    static int generateRandomNumber(int from, int to) {
        return (int) (Math.random() * (to - from) + from);

    }

    static void setBombs(char[][] field, int count, char character) {

        for (int i = 0; i < count; i++) {
            int x = generateRandomNumber(0, field.length);
            int y = generateRandomNumber(0, field[i].length);

            if (field[x][y] == EMPTY) {
                field[x][y] = character;

            } else {
                i--;
            }
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        /*
         -Felh. megad 2 egész számot 10 és 15 között -> 2D pálya
         -8 - 20 közötti darab (felh. megadja) bombát generálunk a pályára :a bombák 70%-a halál, 30% -1 élet
         -- 2 típusú bomba van: 1. meghalunk 2. életet veszítünk 
         - a kezdeti életek száma: 5.
         - van 1 játékos, amivel csak jobbra és lefelé tudunk lépni (kezdet: 0,0)
         - cél: az utolsó sorba eljutni úgy, hogy nem halunk meg
         - ha beér a célba, kiírni, hogy hány élet maradt, és hogy hány lépésből oldotta meg
         */
        char[][] field = null;
        int row = getNumber(MIN_SIZE, MAX_SIZE);
        int col = getNumber(MIN_SIZE, MAX_SIZE);

        play(row, col);

    }

}
