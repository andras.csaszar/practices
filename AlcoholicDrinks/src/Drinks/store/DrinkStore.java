/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drinks.store;

import Drinks.AbstractDrink;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author andra
 */
public class DrinkStore {

    private static final int MAX_SIZE = 101;
    private final List<DrinkProducer> drinkProducers = new ArrayList<>();

    public void add(AbstractDrink drink) {
        String producerName = drink.getDrinkProducer().getName();
        DrinkProducer dp = findProducer(producerName);
        if (isSpaceInStore()) {
            dp.add(drink);
            drinkProducers.add(dp);

//            if (findProducer(producerName) != null) {
//                findProducer(producerName).add(drink);
//            } else {
//                drinkProducers.add(new DrinkProducer(producerName));
//            }
        }

    }

    private DrinkProducer findProducer(String producerName) {

            return drinkProducers.stream().filter(d -> producerName.equals(d.getName())).findFirst().orElse(new DrinkProducer(producerName));

    }

    public boolean isSpaceInStore() {

        return drinkProducers.stream().map(dp -> dp.getDrinks()).count() < MAX_SIZE;
    }

    public static int getMAX_SIZE() {
        return MAX_SIZE;
    }

    public List<DrinkProducer> getDrinkProducers() {
        return drinkProducers;
    }

}
