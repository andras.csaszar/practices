/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drinks.store;

import Drinks.AbstractDrink;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author andra
 */
public class DrinkProducer implements Serializable {

    private String name = null;
    private String address = null;

    private final List<AbstractDrink> drinks = new ArrayList<>();

    public DrinkProducer(String name) {
        this(name,"no address yet");
        
    }

    public DrinkProducer(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public void add(AbstractDrink drink) {
        drinks.add(drink);
        orderByDrinkType();
    }

    private void orderByDrinkType() {
        Collections.sort(drinks, (AbstractDrink ad1, AbstractDrink ad2) -> {
            return ad1.getDrinkType().ordinal() - ad2.getDrinkType().ordinal();
        });

    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public List<AbstractDrink> getDrinks() {
        return drinks;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.name);
        hash = 11 * hash + Objects.hashCode(this.address);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DrinkProducer other = (DrinkProducer) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.address, other.address)) {
            return false;
        }
        if (!Objects.equals(this.drinks, other.drinks)) {
            return false;
        }
        return true;
    }

   

    @Override
    public String toString() {
        return "DrinkProducer{" + "name=" + name + ", address=" + address + ", drinks=" + drinks + '}';
    }

}
