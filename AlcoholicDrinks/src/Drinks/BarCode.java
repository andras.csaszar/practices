/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drinks;

import java.io.Serializable;

/**
 *
 * @author andra
 */
public class BarCode implements Comparable<BarCode>, Serializable {

    private static int number = 10_001;

    public BarCode() {
        number++;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public int compareTo(BarCode o) {
        return this.getNumber() - o.getNumber();
    }


}
