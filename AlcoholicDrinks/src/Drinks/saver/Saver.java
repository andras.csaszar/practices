/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drinks.saver;

import Drinks.AbstractDrink;
import Drinks.properties.DrinkType;
import Drinks.reporter.Reporter;
import Drinks.store.DrinkStore;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andra
 */
public class Saver {

    private final DrinkStore drinkStore;
    private static final String SAVED_DATA_FILENAME = "DrinksSaved.txt";
    private final DrinkType typeToSave = DrinkType.WINE;

    public Saver(DrinkStore drinkStore) {
        this.drinkStore = drinkStore;
    }

    public int serializeDrinkStore() {
        List<AbstractDrink> savedProducts = null;
        int countOfSavedItems = 0;
        try (
                FileOutputStream fos = new FileOutputStream(SAVED_DATA_FILENAME);
                ObjectOutputStream oou = new ObjectOutputStream(fos);) {
            oou.writeObject(new Reporter(drinkStore).getProductsToSave(typeToSave));

            //count really saved items
            try (FileInputStream fis = new FileInputStream(SAVED_DATA_FILENAME);
                    ObjectInputStream ois = new ObjectInputStream(fis);) {
                savedProducts = (List<AbstractDrink>) ois.readObject();
                countOfSavedItems = savedProducts.size();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Saver.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(Saver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Saver.class.getName()).log(Level.SEVERE, null, ex);
        }

        return countOfSavedItems;
    }

}
