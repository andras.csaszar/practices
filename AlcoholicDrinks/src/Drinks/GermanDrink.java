/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drinks;

import Drinks.store.DrinkProducer;
import Drinks.properties.DrinkType;
import Drinks.properties.Vignette;

/**
 *
 * @author andra
 */
public class GermanDrink extends AbstractDrink {

    private final Vignette vignette;
    private final DrinkGroup drinkGroup = DrinkGroup.GERMAN;

    public GermanDrink(Vignette vignette, DrinkType drinkType, DrinkProducer drinkProducer, double alcoholContent, int price) {
        super(drinkType, drinkProducer, alcoholContent, price, DrinkGroup.GERMAN);
        this.vignette = vignette;
    }

    public Vignette getVignette() {
        return vignette;
    }

    @Override
    public String toString() {
        return super.toString() + "GermanDrink{" + "vignette=" + vignette + '}';

    }

}
