/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drinks.reporter;

import Drinks.AbstractDrink;
import Drinks.DrinkGroup;
import Drinks.properties.DrinkType;
import Drinks.saver.Saver;
import Drinks.store.DrinkProducer;
import Drinks.store.DrinkStore;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author andra
 */
public class Reporter {

    private DrinkStore drinkSotre;
//    

    public Reporter(DrinkStore drinkSotre) {
        this.drinkSotre = drinkSotre;
    }

    public List<AbstractDrink> getProductsToSave(DrinkType typeToSave) {
        return drinkSotre.getDrinkProducers().stream()
                .map(p -> p.getDrinks())
                .flatMap(d -> d.stream())
                .filter(d -> d.getDrinkType() == typeToSave)
                .collect(Collectors.toList());
    }

    public List<AbstractDrink> getAllProducts() {
        return drinkSotre.getDrinkProducers().stream()
                .map(p -> p.getDrinks())
                .flatMap(d -> d.stream())
                .collect(Collectors.toList());
    }

    public void generateReports() {

        System.out.println("-----------REPORT after saving START-------------");
        printNumberOfSavedItems();
        printCountOfProductPerGroupOnStore();
        printNumberOfWinesOnStore();
        printProducersOfProductsOnStore();
        System.out.println("-----------REPORT END-------------");

    }

    public void printNumberOfSavedItems() {

        System.out.println("Number of items saved: " + new Saver(drinkSotre).serializeDrinkStore());
    }

    public void printCountOfProductPerGroupOnStore() {
        Map<String, Integer> prodPerGroup = countOfProductsPerGroup();
        prodPerGroup.forEach((k, v) -> System.out.println("*****Drink Group:" + k + "|Count: " + v));

    }

    public Map<String, Integer> countOfProductsPerGroup() {
        return getAllProducts().stream()
                .collect(Collectors.toMap(
                                k -> k.getDrinkgroup().name(),
                                v -> 1,
                                (v1, v2) -> v1+v2,
                                HashMap::new
                        ));
    }

    public void printNumberOfWinesOnStore() {
        System.out.println("Bottle of wines on store: " + numberOfWinesOnStore());
    }

    public int numberOfWinesOnStore() {
        return drinkSotre.getDrinkProducers().stream()
                .map(p -> p.getDrinks())
                .flatMap(d -> d.stream())
                .filter(d -> d.getDrinkType() == DrinkType.WINE)
                .collect(Collectors.toList()).size();
    }

    public void printProducersOfProductsOnStore() {
        System.out.println("Producers of current store: ");
        drinkSotre.getDrinkProducers().stream()
                .distinct()
                .forEach(System.out::println);

    }

    public List<DrinkProducer> getAllProducers() {
        return drinkSotre.getDrinkProducers();
    }

}
