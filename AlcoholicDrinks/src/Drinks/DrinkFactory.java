/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drinks;

import Drinks.store.DrinkProducer;
import Drinks.properties.DrinkType;
import Drinks.properties.Vignette;
import Drinks.properties.VignetteColor;
import Drinks.reporter.Reporter;
import Drinks.saver.Saver;
import java.time.LocalDate;

/**
 *
 * @author andra
 *
 */
public class DrinkFactory {

    private static final int OPERATION = 0;
    private static final int TYPE = 1;
    private static final int PRODUCER_NAME = 2;
    private static final int PRICE = 3;
    private static final int ALCOHOL = 4;
    private static final int GROUP = 5;

    public static AbstractDrink create(String[] parameters) {

        DrinkType drinktype = DrinkType.valueOf(parameters[TYPE]);
        String producerName = parameters[PRODUCER_NAME];//TODO producer!!
        int price = Integer.parseInt(parameters[PRICE]);
        double alcohol = Double.parseDouble(parameters[ALCOHOL]);
        DrinkGroup drinkGroup = DrinkGroup.valueOf(parameters[GROUP]);

        DrinkProducer dpNew = new DrinkProducer(producerName);
        switch (drinkGroup) {
            case HUNGARIAN:
                return new HungarianDrink(drinktype, dpNew, alcohol, price);
            case GERMAN:
                return new GermanDrink(new Vignette(doRandomColor(), getCurrentDate()), drinktype, dpNew, alcohol, price);
            case FRENCH:
                return new FrenchDrink(drinktype, dpNew, alcohol, price);

        }
        return null;
    }

    private static VignetteColor doRandomColor() {
        int colorPosition = (int) Math.random() * (VignetteColor.values().length);
        return VignetteColor.values()[colorPosition];

    }

    private static LocalDate getCurrentDate() {
        return LocalDate.now();
    }

}
