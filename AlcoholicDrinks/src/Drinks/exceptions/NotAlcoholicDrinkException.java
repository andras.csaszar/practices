/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drinks.exceptions;

/**
 *
 * @author andra
 */
public class NotAlcoholicDrinkException extends DrinksException {

    public NotAlcoholicDrinkException(String message) {
        super(message);
    }
    
}
