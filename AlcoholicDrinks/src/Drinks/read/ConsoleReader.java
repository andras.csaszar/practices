/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drinks.read;

import Drinks.exceptions.NotAlcoholicDrinkException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andra
 */
public class ConsoleReader {

    private Drinks.read.CommandParser parser = new Drinks.read.CommandParser();
    private static final String STOP = "EXIT";

    public void read() {

        
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {

            while (true) {
                String line = br.readLine().toUpperCase();
                if (line == null || STOP.equals(line)) {
                    break;
                }
                parser.process(line);
            }

        } catch (IOException ex) {
            System.out.println(ex);
        } catch (NotAlcoholicDrinkException ex) {
            Logger.getLogger(ConsoleReader.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        parser.save();
        parser.report();
               
        
    }

}
