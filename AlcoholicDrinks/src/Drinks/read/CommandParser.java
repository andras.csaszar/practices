/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drinks.read;

import Drinks.AbstractDrink;
import Drinks.DrinkFactory;
import Drinks.exceptions.NotAlcoholicDrinkException;
import Drinks.reporter.Reporter;
import Drinks.saver.Saver;
import Drinks.store.DrinkStore;

/**
 *
 * @author andra
 */
public class CommandParser {

    private static final String DELMIMITER = " ";
    private static final double MIN_ALCOHOL_CONTENT = 0.1d;

    private final DrinkStore drinkStore = new DrinkStore();
    private final Reporter reporter = new Reporter(drinkStore);
    private final Saver saver = new Saver(drinkStore);

    public void process(String line) throws NotAlcoholicDrinkException {
        String[] parameters = line.split(DELMIMITER);
        if ("SAVE".equals(parameters[0])) {
            saver.serializeDrinkStore();
            report();
        } else {
            checkAlcoholContentRestriction(Double.parseDouble(parameters[4]));
            AbstractDrink drink = DrinkFactory.create(parameters);
            drinkStore.add(drink);
        }
    }

    public void checkAlcoholContentRestriction(double alcohol) throws NotAlcoholicDrinkException {
        if (alcohol < MIN_ALCOHOL_CONTENT) {
            throw new NotAlcoholicDrinkException("Drink is not alcoholic, content is:" + alcohol);
        }
    }

    public void print() {
        System.out.println(drinkStore.toString());
    }

    public void save() {
        saver.serializeDrinkStore();
    }

    public void report() {
        reporter.generateReports();
    }

}
