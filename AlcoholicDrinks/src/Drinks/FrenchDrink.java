/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drinks;

import Drinks.exceptions.FrenchDrinkWasFrozenBeforeException;
import Drinks.store.DrinkProducer;
import Drinks.properties.DrinkType;
import drinks.ability.Freezable;
import drinks.ability.Transportable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andra
 */
public class FrenchDrink extends AbstractDrink implements Transportable, Freezable {

    private boolean wasFrozen = false;
    private boolean isBeingTransported = false;
    private final DrinkGroup drinkGroup = DrinkGroup.FRENCH;

    public FrenchDrink(DrinkType drinkType, DrinkProducer drinkProducer, double alcoholContent, int price) {
        super(drinkType, drinkProducer, alcoholContent, price, DrinkGroup.FRENCH);
    }

    @Override
    public boolean transport() throws FrenchDrinkWasFrozenBeforeException {
        if (!wasFrozen) {
            isBeingTransported = true;
        } else {

            throw new FrenchDrinkWasFrozenBeforeException("This drink was frozen already, cannot be transported.");
        }
        return isBeingTransported;
    }

    @Override
    public boolean freeze() {
        wasFrozen = true;
        return true;
    }
}

