/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drinks;

import Drinks.store.DrinkProducer;
import Drinks.properties.DrinkType;
import java.io.Serializable;

/**
 *
 * @author andra
 */
public abstract class AbstractDrink implements Serializable {

    private final DrinkType drinkType;
    private final DrinkProducer drinkProducer;
    private final BarCode barCode;
    private final double alcoholContent;
    private final int price;
    private final DrinkGroup drinkgroup;
    private final int barCodeNumber;

    public AbstractDrink(DrinkType drinkType, DrinkProducer drinkProducer, double alcoholContent, int price, DrinkGroup drinkGroup) {
        this.drinkType = drinkType;
        this.drinkProducer = drinkProducer;
        this.barCode = new BarCode();
        this.alcoholContent = alcoholContent;
        this.price = price;
        this.drinkgroup = drinkGroup;
        this.barCodeNumber = barCode.getNumber();
    }

    public DrinkType getDrinkType() {
        return drinkType;
    }

    public DrinkProducer getDrinkProducer() {
        return drinkProducer;
    }

    public BarCode getBarCode() {
        return barCode;
    }

    public double getAlcoholContent() {
        return alcoholContent;
    }

    public int getPrice() {
        return price;
    }

    public DrinkGroup getDrinkgroup() {
        return drinkgroup;
    }

    @Override
    public String toString() {
        return "AbstractDrink{" + "drinkType=" + drinkType + ", drinkProducer=" + drinkProducer + ", barCode=" + barCodeNumber + ", alcoholContent=" + alcoholContent + ", price=" + price + '}'+"\n";
    }

}
