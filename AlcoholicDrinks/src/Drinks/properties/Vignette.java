/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drinks.properties;

import java.io.Serializable;
import java.time.LocalDate;

/**
 *
 * @author andra
 */
public class Vignette implements Serializable{
    
    private final VignetteColor vignetteColor;
    private final LocalDate planDate;

    public Vignette(VignetteColor vignetteColor, LocalDate planDate) {
        this.vignetteColor = vignetteColor;
        this.planDate = planDate;
    }

    public VignetteColor getVignetteColor() {
        return vignetteColor;
    }

    public LocalDate getPlanDate() {
        return planDate;
    }

    @Override
    public String toString() {
        return "Vignette{" + "vignetteColor=" + vignetteColor + ", planDate=" + planDate + '}';
    }
    
    
}
