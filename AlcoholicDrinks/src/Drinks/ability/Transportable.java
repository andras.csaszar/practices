/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drinks.ability;

import Drinks.exceptions.FrenchDrinkWasFrozenBeforeException;

/**
 *
 * @author andra
 */
public interface Transportable {
    public boolean transport() throws FrenchDrinkWasFrozenBeforeException;
}
