/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drinks;

import Drinks.store.DrinkProducer;
import Drinks.properties.DrinkType;
import drinks.ability.Dilutable;
import drinks.ability.Drinkable;

/**
 *
 * @author andra
 */
public class HungarianDrink extends AbstractDrink implements Drinkable, Dilutable{

    private final DrinkGroup drinkGroup = DrinkGroup.HUNGARIAN;
    
    public HungarianDrink(DrinkType drinkType, DrinkProducer drinkProducer,  double alcoholContent, int price) {
        super(drinkType, drinkProducer, alcoholContent, price, DrinkGroup.HUNGARIAN);
    }

    public DrinkGroup getDrinkGroup() {
        return drinkGroup;
    }

    @Override
    public boolean drink() {
    return true;
    }

    @Override
    public boolean dilute() {
    return true;
    }


    
    
    
}
