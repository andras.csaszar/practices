/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carbuilder;

/**
 *
 * @author andra
 */
public class Car {

    private String model;
    private int maxSpeed;
    private int length;
    private int width;
    private String plateNum;

    public static class CarBuilder {

        private String model;
        private int maxSpeed;
        private int length;
        private int width;
        private String plateNum;

        public CarBuilder(int maxSpeed, int length) {
            this.maxSpeed = maxSpeed;
            this.length = length;
        }

        public CarBuilder setModel(String model) {
            this.model = model;
            return this;

        }

        public CarBuilder setWidth(int width) {
            this.width = width;
            return this;
        }

        public CarBuilder setPlateNum(String plateNum) {
            this.plateNum = plateNum;
            return this;
        }
        
        public Car build(){
        return new Car (this);
        }

    }
       private Car (CarBuilder carBuilder){
       this.length = carBuilder.length;
       this.maxSpeed = carBuilder.maxSpeed;
       this.model = carBuilder.model;
       this.plateNum = carBuilder.plateNum;
       this.width = carBuilder.width;
       
       }

    @Override
    public String toString() {
        return "Car{" + "model=" + model + ", maxSpeed=" + maxSpeed + ", length=" + length + ", width=" + width + ", plateNum=" + plateNum + '}';
    }
       

}
