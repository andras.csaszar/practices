/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carbuilder;

import carbuilder.Car.CarBuilder;

/**
 *
 * @author andra
 */
public class CarBuildingPractice {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Car c1 = new CarBuilder(200, 4)
                .setModel("Merci")
                .setPlateNum("MMH-123")
                .setWidth(2)
                .build();
        System.out.println(c1);

    }

}
