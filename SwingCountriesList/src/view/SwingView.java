/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import java.awt.BorderLayout;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author andra
 */
public class SwingView extends JFrame {

    private Controller controller;

    private JTextField inputText;
    private JButton searchButton;
    private JTextArea resultArea;

    public void setController(Controller controller) {
        this.controller = controller;
    }

    public void enableView() {

        construct();
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

    }

    private void construct() {
        buildNorth();
        buildCenter();
    }

    private void buildNorth() {
        JPanel panelNorth = new JPanel();
        inputText = new JTextField(20);
        searchButton = new JButton("Search");

        panelNorth.add(inputText);
        panelNorth.add(searchButton);

        add(panelNorth, BorderLayout.NORTH);

        searchButton.addActionListener(e -> controller.handleSearch(inputText.getText()));

    }

    private void buildCenter() {
        JPanel panelCenter = new JPanel();

        resultArea = new JTextArea(20,100);
        panelCenter.add(resultArea);

        add(panelCenter, BorderLayout.CENTER);

    }

    public void updateView(List<String> newValuesToShow) {
        resultArea.setText(
                newValuesToShow.stream()
                .collect(Collectors.joining("\n"))
        );
    }

}
