/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import java.util.stream.Collectors;
import model.Model;
import view.SwingView;

/**
 *
 * @author andra
 */
public class Controller {

    private Model m;
    private SwingView v;

    public Controller(Model m, SwingView v) {
        this.m = m;
        this.v = v;

        m.setController(this);
        v.setController(this);

    }

    public void handleSearch(String input) {
        updateModel(getFilteredList(input));
      
    }

    public List<String> getFilteredList(String str) {
        return m.getCountriesFromFile().stream()
                //.filter(c -> c.contains(str)).collect(Collectors.toList());
                .filter(c -> c.toLowerCase().contains(str.toLowerCase()))
                .collect(Collectors.toList());
                
    }

    public void updateModel(List<String> newCountries) {
        m.setFilteredCountries(newCountries);
    }
    
    public void updateView(List<String> newCountries){
        v.updateView(newCountries);
    }

}
