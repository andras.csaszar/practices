/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Controller;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author andra
 */
public class Model {

    private Controller controller;
    private String filePath = "input.txt";
    private List<String> countriesFromFile;
    private List<String> filteredCountries;

    public Model() {

        this.countriesFromFile = new ArrayList();
        this.filteredCountries = new ArrayList();
        //createFile(filePath);
       
        readCountriesFromFile(filePath);

        //debug
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

    private void readCountriesFromFile(String filePath) {
        File file = new File(filePath);

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            countriesFromFile = br.lines().collect(Collectors.toList());
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public List<String> getCountriesFromFile() {
        return countriesFromFile;
    }

    public void setFilteredCountries(List<String> filteredCountries) {
        this.filteredCountries = filteredCountries;
        controller.updateView(filteredCountries);
    }

    public List<String> getFilteredCountries() {
        return filteredCountries;
    }

    @Override
    public String toString() {
        return "Model{" + "countriesFromFile=" + countriesFromFile + ", filteredCountries=" + filteredCountries + '}';
    }

    private void createFile(String filePath) {

        File f = new File(filePath);

        PrintWriter pw;
        try {
            pw = new PrintWriter(new FileWriter(f));
            pw.write("hey");
        } catch (IOException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
