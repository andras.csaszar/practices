
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mail;

import controller.Controller;
import model.Model;
import view.SwingView;

/**
 *
 * @author andra
 */
public class SwingCountriesListApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Model m = new Model();
        SwingView v = new SwingView();
        
        Controller c = new Controller (m , v);
        v.enableView();
        
        
        
    }
    
}
