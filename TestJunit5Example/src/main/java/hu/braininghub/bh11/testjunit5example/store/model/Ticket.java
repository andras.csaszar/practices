package hu.braininghub.bh11.testjunit5example.store.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Objects;

public class Ticket {

    Ticket() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }



    enum Location {
        LONDON,
        BUDAPEST
    }

    private Airline owner;
    private String origin;
    private String destination;

    public Ticket(Airline owner, String origin, String destination) {
        this.owner = owner;
        this.origin = origin;
        this.destination = destination;
    }

    public Ticket(Airline owner) {
        this(owner, "BUD", "FRA");
    }

    public Airline getOwner() {
        return owner;
    }

    public void setOwner(Airline owner) {
        this.owner = owner;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getLocation() {
        return owner.isHungarianAirline() ? Location.BUDAPEST.name() : Location.LONDON.name();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ticket ticket = (Ticket) o;
        return Objects.equals(owner, ticket.owner) &&
                Objects.equals(origin, ticket.origin) &&
                Objects.equals(destination, ticket.destination);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, origin, destination);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("owner", owner)
                .append("origin", origin)
                .append("destination", destination)
                .toString();
    }
}
