package hu.braininghub.bh11.testjunit5example.store;

import hu.braininghub.bh11.testjunit5example.store.model.Ticket;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

public class TicketStore {

    private final List<Ticket> tickets = new ArrayList<>();

    public boolean add(Ticket ticket) {
        return tickets.add(ticket);
    }

    public long getNumberOfStoredHungarianAirlines() {
        return tickets.stream()
                .filter(ticket -> ticket.getOwner().isHungarianAirline())
                .count();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("tickets", tickets)
                .toString();
    }
}
