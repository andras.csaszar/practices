package hu.braininghub.bh11.testjunit5example.store.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Objects;

public class Airline {

    //nested Enum is implicit static
    enum HungarianAirlines {
        WIZZ_AIR,
        MALEV,
        BUDAPEST_AIR
    }

    enum Airlines {
        WIZZ_AIR,
        MALEV,
        BUDAPEST_AIR,
        BERLIN_AIR,
        WIEN_AIR
    }

    private final Airlines shortName;
    private final String longName;

    public Airline(Airlines shortName, String longName) {
        this.shortName = shortName;
        this.longName = longName;
    }

    public Airlines getShortName() {
        return shortName;
    }

    public String getLongName() {
        return longName;
    }

    public boolean isHungarianAirline() {
        try {
            HungarianAirlines.valueOf(shortName.name());

        } catch (IllegalArgumentException e) {
            return false;
        }

       return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Airline airline = (Airline) o;
        return Objects.equals(shortName, airline.shortName) &&
                Objects.equals(longName, airline.longName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(shortName, longName);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("shortName", shortName)
                .append("longName", longName)
                .toString();
    }
}
