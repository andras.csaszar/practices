/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.testjunit5example.store;

import hu.braininghub.bh11.testjunit5example.store.model.Ticket;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author andra
 */
@ExtendWith(MockitoExtension.class)
public class TicketStoreTest {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private Ticket ticket;
    private TicketStore underTest /*= new TicketStore()*/ ;

    @BeforeEach
    void init() {
        underTest = new TicketStore();
    }

    @DisplayName("Should get 0 when empty list is used")
    @Test
    void testGetNumberOfStoredHungarianAirlinesWithEmptyList() {

        //Given
        //When
        long result = underTest.getNumberOfStoredHungarianAirlines();
        //Then
        assertEquals(0L, result);

    }

    @Test
    void testGetNumberOfStoredHungarianAirlinesWith1NonHun() {

        //Given
        underTest.add(ticket);
        Mockito.when(ticket.getOwner().isHungarianAirline()).thenReturn(false);
        //When
        long result = underTest.getNumberOfStoredHungarianAirlines();
        //Then
        assertEquals(0L, result);

    }

    @Test
    void testGetNumberOfStoredHungarianAirlinesWith1Hun() {

        //Given
        underTest.add(ticket);
        underTest.add(ticket);
        Mockito.when(ticket.getOwner().isHungarianAirline()).thenReturn(true, false);
        //When
        long result = underTest.getNumberOfStoredHungarianAirlines();
        //Then
        assertEquals(1L, result);

    }

}
