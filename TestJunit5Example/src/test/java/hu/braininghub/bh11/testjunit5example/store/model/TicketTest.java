/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.testjunit5example.store.model;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author andra
 */
@ExtendWith(MockitoExtension.class)
public class TicketTest {

    @Mock
    private Airline airline;

    @InjectMocks
    private Ticket underTest;

    @Test
    void shouldGetLocationReturnBudapest() {
        //Given
        // Airline a = Mockito.mock(Airline.class);

        //  Ticket underTest = new Ticket(airline);
        when(airline.isHungarianAirline()).thenReturn(true);

        //When
        String location = underTest.getLocation();
        //Then
        assertEquals("BUDAPEST", location);

    }

    @Test
    void shouldGetLocationReturnLondon() {
        //Given
        // Airline a = Mockito.mock(Airline.class);

        //  Ticket underTest = new Ticket(airline);
        when(airline.isHungarianAirline()).thenReturn(false);

        //When
        String location = underTest.getLocation();
        //Then
        assertEquals("LONDON", location);

    }

   

}
