/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.testjunit5example.store.model;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author andra
 */
@ExtendWith(MockitoExtension.class)
public class Ticket2Test {

    @Mock
    private Airline airline;

    @InjectMocks
    private Ticket underTest;
    
    @BeforeEach
    void give(){
        underTest = new Ticket(airline);
    }

   @Test
    void testDefaultConstructor() {
        //Given
        //When
        //Then
        assertAll("default constructor", 
                () -> assertEquals("BUD", underTest.getOrigin()),
                () -> assertEquals("FRA", underTest.getDestination()));
              

    }

}
