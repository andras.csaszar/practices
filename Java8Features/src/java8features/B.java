/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java8features;

/**
 *
 * @author andra
 */
public interface B extends A {
    @Override
    default void method1(){
        System.out.println("do something in B");
    }
    static void staticMethod(){
        System.out.println("do something static in B");
    }
    
}
