/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java8features;

/**
 *
 * @author andra
 */
public interface A {
    
    void method1();
    static void staticMethod(){
        System.out.println("do something static in A");
    }
    
    //private default void method(){}
    //ez csak Java9-től van
    
    
    
}
