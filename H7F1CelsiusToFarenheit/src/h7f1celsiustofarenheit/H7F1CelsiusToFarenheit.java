/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h7f1celsiustofarenheit;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class H7F1CelsiusToFarenheit {

    static final Scanner SCANNER = new Scanner(System.in);

    static double getNumber() {
        double number=0d;
        System.out.println("Give me a number Celsios degree and I will tell you what is it in Farenheit");
        do {
            if (SCANNER.hasNextInt()) {
                number = SCANNER.nextInt();
                return number;
            } else {
                SCANNER.next();
            }
        } while (true);

    }

    static double convertCelsiusToFarenheit(double celsius) {
        return (celsius*1.8)+ 32.0;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        /*5
        
         [°C] = ([°F] - 32) : 1,8
         [°F] = [°C] · 1,8 + 32
         */
        
        System.out.println(convertCelsiusToFarenheit(getNumber()));
       
        
    }

}
