/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.event.ActionEvent;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import model.Log;
import model.Model;
import view.SwingView;

/**
 *
 * @author andra
 */
public class Controller {

    private Model m;
    private SwingView v;

    public Controller(Model m, SwingView v) {
        this.m = m;
        this.v = v;

        m.setController(this);
        v.setController(this);

    }

    public void handleSearch(String eventName) {

        updateModel(createLogEntry(eventName));
        
        
        updateView(m.getEvents());  //mvp
    }

    private Log createLogEntry(String eventName) {

        return new Log(eventName);
       
    }

    private boolean isLastHourInLogSame(LocalDateTime dateTime) {
        LocalDateTime date;
        if (!m.getEvents().isEmpty()) {
            date = m.getEvents().get(m.getEvents().size() - 1).getDateOfEvent();
        } else {
           date = null;
        }
  return date.getHour() == dateTime.get
    }

    private void updateModel(Log newLog) {
        m.addEvent(newLog);
    }

    public void updateView(List<String> newStrings) {
        v.setResults(newStrings);
    }

}
