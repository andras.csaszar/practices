/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Controller;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andra
 */
public class Model {

    private Controller controller;
    private String filePath = "events.txt";
    //private List<String> events;
    private List<Log> events;
    private int eventNumber;

    public Model() {
        this.events = new ArrayList<>();
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

    public List<Log> getEvents() {
        return events;
    }


    public void addEvent(Log event) {
        this.events.add(event);
        saveEventToFile();
    }

    private void saveEventToFile() {

        try (PrintWriter pw = new PrintWriter(new FileWriter(filePath));) {
            pw.write(events.toString());
        } catch (IOException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
