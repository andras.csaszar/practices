/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 *
 * @author andra
 */
public class Log {
    private static int eventNumberClass;
    private int eventNumber;
    private String event;
    private LocalDateTime dateOfEvent;

    public Log(String event) {
        this.eventNumberClass++; 
        this.eventNumber = this.eventNumberClass;
        this.event=event;
        dateOfEvent = LocalDateTime.now();
        
    }

    public String getEvent() {
        return event;
    }

    public LocalDateTime getDateOfEvent() {
        return dateOfEvent;
    }

    public int getEventNumber() {
        return eventNumber;
    }
    
    
}
