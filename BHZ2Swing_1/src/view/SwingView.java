/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import java.awt.BorderLayout;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author andra
 */
public class SwingView extends JFrame {

    private Controller controller;

    private JButton searchButton;
    private JTextArea resultArea;

    public void setController(Controller controller) {
        this.controller = controller;
    }

    public void enableView() {
        construct();
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

    }
//todo
    public void setResults(List<String> newStringsToShow) {
        resultArea.setText(newStringsToShow.stream()
                .collect(Collectors.joining("\n")));
        String as = resultArea.getText();
    }

    private void construct() {
        
        buildCenter();
        buildSouth();

    }


    private void buildCenter() {
        resultArea = new JTextArea(50, 100);

        JPanel centerPanel = new JPanel();
        centerPanel.add(resultArea);
        add(centerPanel, BorderLayout.CENTER);
    }
private void buildSouth() {
 
        searchButton = new JButton("Search");
        JPanel southPanel = new JPanel();

        southPanel.add(searchButton);

        add(southPanel, BorderLayout.SOUTH);

        searchButton.addActionListener(e -> controller.handleSearch(((JButton)(e.getSource())).getText()));
 
    }
}
