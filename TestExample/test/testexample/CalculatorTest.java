/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testexample;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author andra
 */
public class CalculatorTest {

    private int number1 = 10;
    private int number2 = 25;
    private Calculator underTest;
    
    @BeforeClass
    public static void beforeAllMethods(){
        System.out.println("BeforeAllMethods");
    }

    @Before
    public void runBeforeAnyMethod() {
        System.out.println("runBeforeAnyMethod");
        underTest = new Calculator(number1, number2);
    }

    @Test
    public void testAdd() {
        //Given

        //When
        int result = underTest.add();

        //Then
        Assert.assertEquals(number1 + number2, result);
        //első paraméter az expected
    }

    @Test
    public void testSubtract() {
        //Given

        //When
        int result = underTest.subtract();

        //Then
        Assert.assertEquals(number1 - number2, result);
        //első paraméter az expected
    }

    @Test
    public void testMultiply() {
        //Given

        //When
        int result = underTest.multiply();

        //Then
        Assert.assertEquals(number1 * number2, result);
        //első paraméter az expected
    }

}
