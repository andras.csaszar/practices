/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpatterns.app;

import designpatterns.creation.AbstractAnimal;
import designpatterns.creation.AnimalFactory;
import designpatterns.creation.Car;
import designpatterns.creation.Car2;
import designpatterns.creation.MySingleton;

/**
 *
 * @author andra
 */
public class App {

    public static void main(String[] args) {
        MySingleton ms = MySingleton.getInstance();
        MySingleton ms1 = MySingleton.getInstance();

        Car c = new Car.CarBuilder("trabi", "feher")
                .setAbs(true)
                .setMaxSpeed(221)
                .build();

        
        Car2 c2  = new Car2("trabi", "feher")
                .setAbs(true)
                .setMaxSpeed(222);
    
    
        AbstractAnimal ab1 = AnimalFactory.create("Cat");
        ab1.sayHello();
        AbstractAnimal ab2 = AnimalFactory.create("Dog");
        ab2.sayHello();
        AbstractAnimal ab3 = AnimalFactory.create("Snake");
        ab3.sayHello();
    
    }

}
