/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpatterns.creation;

/**
 *
 * @author andra
 */
public class AnimalFactory {
    
     public static AbstractAnimal create(String type) {
        if ("Cat".equalsIgnoreCase(type)) {
            return new Cat();
        } else if ("Dog".equalsIgnoreCase(type)) {
            return new Dog();
        } else if ("Snake".equalsIgnoreCase(type)) {
            return new Snake();
        }
        return null;
    }
    
}
