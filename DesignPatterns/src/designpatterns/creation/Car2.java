/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpatterns.creation;

/**
 *
 * @author andra
 */
public class Car2 {

    private String model;
    private String color;
    private boolean abs;
    private int maxSpeed;
    private boolean spoiler;

    public Car2(String model, String color) {
        this.model = model;
        this.color = color;
    }

    public Car2 setModel(String model) {
        this.model = model;
        return this;
    }

    public Car2 setColor(String color) {
        this.color = color;
        return this;
    }

    public Car2 setAbs(boolean abs) {
        this.abs = abs;
        return this;
    }

    public Car2 setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
        return this;
    }

    public Car2 setSpoiler(boolean spoiler) {
        this.spoiler = spoiler;
        return this;    
    }

    
    
}
