/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filefiltering;

import collector.FileCollector;
import controller.Controller;
import controller.FileController;
import filter.FilterAggregator;
import java.io.File;
import java.util.List;
import model.FileStore;
import model.Model;
import reader.ConsoleReader;
import view.ConsoleView;
import view.SwingView;
import view.View;

/**
 *
 * @author andra
 */
public class Application {

    private final FileCollector fileCollector = new FileCollector();
    private final ConsoleReader consoleReader = new ConsoleReader();
    private final FilterAggregator filterAggregator = new FilterAggregator();

    public void process() {
        String path = consoleReader.read();
        List<File> files = fileCollector.collect(new File(path));
        List<File> filteredFiles = filterAggregator.filter(files);

        printResult(filteredFiles);

    }

    private void printResult(List<File> files) {  // Consol-os view-nak ez az update-je
        files.forEach(System.out::println);

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

//        Application app = new Application();
//        app.process();
        
        //View v = new SwingView();
        View v = new ConsoleView();
        
        Model m = new FileStore();
        
        Controller c = new FileController(v, m);
        
        v.enableView();
        
        
    }

}
