/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Controller;
import controller.ModelController;
import java.io.File;
import java.util.List;

/**
 *
 * @author andra
 */
public interface Model {

    void setController(ModelController controller);

    List<File> getFiles();

    void setFiles(List<File> files);

}
