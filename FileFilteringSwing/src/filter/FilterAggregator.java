/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter;

import filter.handler.Filter;
import filter.handler.ParentFolderContainsAFilter;
import filter.handler.SizeLimited2MBFilter;
import filter.handler.SmallLetterContainedFilter;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author andra
 */
public class FilterAggregator {

    private final List<Filter> filters = new ArrayList<>();

    public FilterAggregator() {
        filters.add(new SizeLimited2MBFilter());
        filters.add(new SmallLetterContainedFilter());
        filters.add(new ParentFolderContainsAFilter());
    }

    public List<File> filter(List<File> files) {
        return files.stream()
                .filter(this::allFilterMatched)
                .collect(Collectors.toList());
    }

    private boolean allFilterMatched(File f) {

        for (Filter filter : collectRelevantFilters(f)) {
            if (!filter.filter(f)) {
                return false;
            }
        }
        return true;
    }

    private List<Filter> collectRelevantFilters(File file) {
        return filters.stream()
                .filter(f -> f.test(file))
                .collect(Collectors.toList());
    }

}
