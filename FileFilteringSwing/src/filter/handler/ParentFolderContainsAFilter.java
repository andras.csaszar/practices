/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter.handler;

import java.io.File;

/**
 *
 * @author andra
 */
public class ParentFolderContainsAFilter implements Filter {

    @Override
    public boolean filter(File file) {
        String parent = file.getParent();
        StringBuilder sb = new StringBuilder(parent);

        if (file.isDirectory()) {
            return false;
        }
        for (int i = 0; i < sb.length(); i++) {
            if (sb.charAt(i) == 'A') {
                return true;
            }

        }

        return false;
    }

    @Override
    public boolean test(File file) {
        return true;
    }

}
