/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.ViewController;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author andra
 */
public class ConsoleView implements View {

    private ViewController controller;
    private static final String STOP = "exit";
    private static String path = null;

    @Override
    public void setController(ViewController controller) {
        this.controller = controller;
    }

    @Override
    public void update(List<File> files) {
        System.out.println(
                "Filter result:\n"
                + files.stream()
                .map(File::getPath)
                .collect(Collectors.joining("\n"))
        );
    }

    @Override
    public void enableView() {
        while (!STOP.equalsIgnoreCase(path)) {
            controller.handleGoButton(readingFromConsole());
            enableView();
        }
    }

    private String readingFromConsole() {
        System.out.println("Enter the directory path to list files filtered!");
        try /*()*/ {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            while (true) {
                path = br.readLine();
                if (path == null || STOP.equalsIgnoreCase(path)) {
                    br.close();
                    return null;
                    //break;
                }
                return path;
            }

        } catch (IOException ex) {
            System.out.println("No such file or directory");
            ex.printStackTrace();
            return null;
        }

    }

}
