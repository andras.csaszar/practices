/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.ViewController;
import filefiltering.Application;
import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author andra
 */
public class SwingView extends JFrame implements View {

    private ViewController controller;
    private JTextArea jTextArea;
    private JButton jButton;
    private JTextField jTextField;

    @Override
    public void setController(ViewController controller) {
        this.controller = controller;
    }

    @Override
    public void update(List<File> files) {
        jTextArea.setText(
                files.stream()
                .map(File::getPath)
                .collect(Collectors.joining("\n")));

    }

    @Override
    public void enableView() {
        construct();

        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        serialiseOnClose();
        setVisible(true);
    }

    private void serialiseOnClose(){
    
        addWindowListener(new WindowAdapter() {
         public void windowClosing(WindowEvent windowEvent){
            controller.serialize();
         }        
      });
      //  addWindowListeren(e -> )
        
    }
    
    private void construct() {
        jTextArea = new JTextArea(100, 100);
        jButton = new JButton("GO");
        jTextField = new JTextField(20);

        JPanel p1 = new JPanel();
        p1.add(jTextField);
        p1.add(jButton);

        JPanel p2 = new JPanel();
        p2.add(jTextArea);

        add(p1, BorderLayout.NORTH);
        add(p2, BorderLayout.CENTER);

        jButton.addActionListener(e -> controller.handleGoButton(jTextField.getText()));

    }

}
