/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.ViewController;
import java.io.File;
import java.util.List;

/**
 *
 * @author andra
 */
public interface View {
    void setController(ViewController controller);
    void update(List<File> files);
    void enableView(); 
}
