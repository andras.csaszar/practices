/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import collector.FileCollector;
import filter.FilterAggregator;
import java.io.File;
import java.util.Collections;
import java.util.List;
import model.FileStore;
import model.Model;
import model.Saver;
import model.Serializer;
import reader.ConsoleReader;
import view.View;

/**
 *
 * @author andra
 */
public class FileController implements ModelController, ViewController {

    private final View view;
    private final Model model;

    private final FileCollector fileCollector = new FileCollector();
    private final FilterAggregator filterAggregator = new FilterAggregator();

    private final Saver saver = new Serializer();

    public FileController(View view, Model model) {
        this.view = view;
        this.model = model;

        model.setController(this);
        view.setController(this);
    }

    @Override
    public void notifyView() {
        List<File> files = model.getFiles(); //query
        view.update(Collections.unmodifiableList(files));
    }

    @Override
    public void handleGoButton(String path) {
        if (path != null) {
            List<File> files = fileCollector.collect(new File(path));
            List<File> filteredFiles = filterAggregator.filter(files);

            model.setFiles(filteredFiles);
        }

    }

    @Override
    public void serialize() {
        saver.serialise(model.getFiles());
    }

}
