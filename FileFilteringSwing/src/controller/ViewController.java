/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.FileStore;
import model.Model;

/**
 *
 * @author andra
 */
public interface ViewController extends Controller {

    void handleGoButton(String str); //View hívja meg
    void serialize();
}
