/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hetlinkrolfeladatrecursiveorai;

/**
 *
 * @author andra
 */
public class HetLinkrolFeladatRecursiveOrai {

     static String formatInteger(long i) {
        String str = ""+i;
        
        if(i<99) {
            str = "0" + str;
        }
        if(i<=9) {
            str = "0" + str;
        }
        
        return str;
    }
    static void recursiveWriteNumberInFormat(long num) {
        if (num >= 1000) {
            recursiveWriteNumberInFormat(num / 1000);
        }
        String str = num % 1000 + " ";
        if (num > 1000) {
            str = formatInteger(num % 1000) + " ";
        }
        System.out.print(str);
    }


    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }   
    
}
