/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tizenhatenum;

/**
 *
 * @author andra
 */
public enum SmartDays {

    MONDAY() {
                @Override
                public void printMood() {
                    System.out.println("Monday");
                }
            },
    TUESDAY() {
                @Override
                public void printMood() {
                    System.out.println("Tuesday");
                }
                
                @Override
                public String toString(){
                return "Monday";
                }
            },
    WEDNESDAY() {
                @Override
                public void printMood() {
                    System.out.println("Wednesday");
                }
            },
    THURSDAY() {
                @Override
                public void printMood() {
                    System.out.println("Thursday");
                }
            },
    FRIDAY() {
                @Override
                public void printMood() {
                    System.out.println("Friday");
                }
            },
    SATURDAY() {
                @Override
                public void printMood() {
                    System.out.println("Saturday");
                }
            },
    SUNDAY() {
                @Override
                public void printMood() {
                    System.out.println("Sunday");
                }
            };

    public abstract void printMood();

}
