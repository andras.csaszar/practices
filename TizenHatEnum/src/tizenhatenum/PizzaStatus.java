/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tizenhatenum;

/**
 *
 * @author andra
 */
public enum PizzaStatus {
     ORDERED(5) {
        @Override
        public boolean isOrdered() {
            return true;
        }
    },
    READY(2) {
        @Override
        public boolean isReady() {
            return true;
        }
    },
    DELIVERED(0) {
        @Override
        public boolean isDelivered() {
            return true;
        }
    };
    private int timeToDelivery;
    public boolean isOrdered() {
        return false;
    }
    public boolean isReady() {
        return false;
    }
    public boolean isDelivered() {
        return false;
    }
    public int getTimeToDelivery() {
        return timeToDelivery;
    }
    PizzaStatus(int timeToDelivery) {
        this.timeToDelivery = timeToDelivery;
    }
}
