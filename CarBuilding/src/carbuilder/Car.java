/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carbuilder;

/**
 *
 * @author andra
 */
public class Car {

    private String plate;
    private int id;
    private String model;
    private int maxSpeed;
    private int length;
    private int width;

    public static class CarBuilder {

        private String plate;
        private int id;
        private String model;
        private int maxSpeed;
        private int length;
        private int width;

        public CarBuilder(String plate, int id) {
            this.plate = plate;
            this.id = id;
        }

        public CarBuilder setPlate(String plate) {
            this.plate = plate;
            return this;
        }

        public CarBuilder setId(int id) {
            this.id = id;
            return this;
        }

        public CarBuilder setModel(String model) {
            this.model = model;
            return this;
        }

        public CarBuilder setMaxSpeed(int maxSpeed) {
            this.maxSpeed = maxSpeed;
            return this;
        }

        public CarBuilder setLength(int length) {
            this.length = length;
            return this;
        }

        public CarBuilder setWidth(int width) {
            this.width = width;
            return this;
        }

        public Car build() {
            return new Car(this);
        }

    }

    private Car(CarBuilder carBuilder) {
        this.plate = carBuilder.plate;
        this.id = carBuilder.id;
        this.length = carBuilder.length;
        this.maxSpeed = carBuilder.maxSpeed;
        this.model = carBuilder.model;
        this.width = carBuilder.width;

    }

    @Override
    public String toString() {
        return "Car{" + "plate=" + plate + ", id=" + id + ", model=" + model + ", maxSpeed=" + maxSpeed + ", length=" + length + ", width=" + width + '}';
    }



}
