/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carbuilder;

import carbuilder.Car.CarBuilder;

/**
 *
 * @author andra
 */
public class CarBuilding {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Car car1 = new CarBuilder("MMM-123", 4)
                .setModel("Merci")
                .setLength(3)
                .setWidth(2)
                .build();
        System.out.println(car1);

    }

}
