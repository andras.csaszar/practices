/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exception;

/**
 *
 * @author andra
 */
public class EntertainmentProductOverUsedException extends RuntimeException {

    public EntertainmentProductOverUsedException(String message) {
        super(message);
    }
}
