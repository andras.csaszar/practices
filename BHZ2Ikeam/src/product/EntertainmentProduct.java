/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

import exception.EntertainmentProductOverUsedException;
import java.time.LocalDateTime;
import property.SwitchingOn;

/**
 *
 * @author andra
 */
public class EntertainmentProduct extends AbstractProduct implements SwitchingOn {

    private int switchOnCounter = 0;

    public EntertainmentProduct(ProductType productType, String producer, BarCode barCode, int price) {
        super(productType, producer, barCode, price);
    }

    public int getSwitchOnCounter() {
        return switchOnCounter;
    }






    @Override
    public void switchOn() {
        if (isOverUsed()) {
            throw new EntertainmentProductOverUsedException("I am overused, cannot swith on");
        }
        switchOnCounter++;
        System.out.println("I am on, it is fun.");
    }

    private boolean isOverUsed() {
        return switchOnCounter > 5;
    }

}
