/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

/**
 *
 * @author andra
 */
public class ProductFactory {

    private static final int OPERATION = 0;
    private static final int BARCODE = 1;

    private static final int TYPE = 2;
    private static final int PRODUCER = 3;
    private static final int PRICE = 4;
    private static final int GROUP = 5;

    public static AbstractProduct create(String[] parameters) {

        int barCodeNumber = Integer.parseInt(parameters[BARCODE]);
        ProductType productType = ProductType.valueOf(parameters[TYPE]);
        String producer = parameters[PRODUCER];
        int price = Integer.parseInt(parameters[PRICE]);

        String productGroup = parameters[GROUP];

        if ("entertainment".equals(productGroup.toLowerCase())) {
            return new EntertainmentProduct(productType, producer, new BarCode(), price);
        } else if ("beauty".equals(productGroup.toLowerCase())) {
            return new BeautyProduct(price, productType, producer, new BarCode(), price);
        } else if ("kitchen".equals(productGroup.toLowerCase())) {
            return new KitchenProduct(productType, producer, new BarCode(), price);
        }
        return null;
    }

}
