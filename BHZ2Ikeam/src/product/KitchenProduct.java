/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

import java.time.LocalDateTime;
import property.LiftingUp;
import property.SwitchingOn;

/**
 *
 * @author andra
 */
public class KitchenProduct extends AbstractProduct implements LiftingUp, SwitchingOn {

    public KitchenProduct(ProductType productType, String producer, BarCode barCode, int price) {
        super(productType, producer, barCode, price);
    }






    @Override
    public void liftUp() {
        System.out.println("I am up in the air now..");
    }

    @Override
    public void switchOn() {
        System.out.println("I am switched on now..");
    }
    
}
