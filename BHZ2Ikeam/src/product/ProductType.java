/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

/**
 *
 * @author andra
 */
public enum ProductType {

    CHEAP(),
    AVERAGE(),
    EXPENSIVE(),
    LUXURY(),
    POPULAR();

}
