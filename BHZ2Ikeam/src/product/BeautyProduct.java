/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

import java.time.LocalDateTime;

/**
 *
 * @author andra
 */
public class BeautyProduct extends AbstractProduct {

    private int weightInKg;

    public BeautyProduct(int weightInKg, ProductType productType, String producer, BarCode barCode, int price) {
        super(productType, producer, barCode, price);
        this.weightInKg = weightInKg;
    }

 




    public void setWeightInKg(int weightInKg) {
        this.weightInKg = weightInKg;
    }


    
    
}
