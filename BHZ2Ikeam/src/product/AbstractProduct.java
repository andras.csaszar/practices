/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 *
 * @author andra
 */
public abstract class AbstractProduct {

    private final ProductType productType;
    private final String producer;
    private final BarCode barCode;
    private final int price;
    private final LocalDate dateOfRegistered;
    private final int barCodeNumber;

    public AbstractProduct(ProductType productType, String producer, BarCode barCode, int price) {
        this.productType = productType;
        this.producer = producer;
        this.barCode = barCode;
        this.price = price;
        this.dateOfRegistered = LocalDate.now();
        this.barCodeNumber = barCode.getNumber();
    }

    public ProductType getProductType() {
        return productType;
    }

    public String getProducer() {
        return producer;
    }

    public BarCode getBarCode() {
        return barCode;
    }

    public int getPrice() {
        return price;
    }

    public LocalDate getDateOfRegistered() {
        return dateOfRegistered;
    }



    public int getBarCodeNumber() {
        return barCodeNumber;
    }

    @Override
    public String toString() {
        return "AbstractProduct{" + "productType=" + productType + ", producer=" + producer + ", barCode=" + barCode + ", price=" + price + ", dateOfRegistered=" + dateOfRegistered + ", barCodeNumber=" + barCodeNumber + '}';
    }

}
