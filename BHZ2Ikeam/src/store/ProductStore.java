/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;

import java.util.ArrayList;
import java.util.List;
import product.AbstractProduct;

/**
 *
 * @author andra
 */
public class ProductStore {

    private final List<AbstractProduct> products = new ArrayList<>();

    public void add(AbstractProduct product) {
        products.add(product);
    }

    public void remove(AbstractProduct product){
        products.remove(this);
    }
    
    
    public List<AbstractProduct> getProducts() {
        return products;
    }
    
    
}