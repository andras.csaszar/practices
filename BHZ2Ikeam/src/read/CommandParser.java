/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package read;

import product.AbstractProduct;
import product.ProductFactory;
import report.Reporter;
import store.ProductStore;

/**
 *
 * @author andra
 */
public class CommandParser {

    private static final String DELMIMITER = " ";
    private static final int OPERATION = 0;

    private final ProductStore productStore = new ProductStore();
    private final Reporter reporter = new Reporter(productStore);

    public void process(String line) {
        String[] parameters = line.split(DELMIMITER);
        if ("REPORT".equalsIgnoreCase(parameters[OPERATION])) {
            report();
        } else if ("ADD".equalsIgnoreCase(parameters[OPERATION])) {

            AbstractProduct product = ProductFactory.create(parameters);

            productStore.add(product);
        } else if ("REMOVE".equalsIgnoreCase(parameters[OPERATION])) {
            productStore.remove(reporter.getProductByBarCode(parameters[1]));
        }
    }

    public void report() {
        reporter.generateReports();
    }
}
