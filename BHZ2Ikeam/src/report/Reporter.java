/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import java.util.Optional;
import product.AbstractProduct;
import product.EntertainmentProduct;
import store.ProductStore;

/**
 *
 * @author andra
 */
public class Reporter {

    private ProductStore productStore;

    public Reporter(ProductStore productStore) {
        this.productStore = productStore;
    }

    public void generateReports() {
        System.out.println("The entertainment products with more than 2 more switch-ons are as many as:" + countEntertainProductMin2SwitchOns());
        System.out.println("Number of stored items: "+ productStore.getProducts().size());
    }

    public AbstractProduct getProductByBarCode(String parameter) {
        Optional<AbstractProduct> prod
                = productStore.getProducts().stream()
                .filter(p -> p.getBarCodeNumber() == Integer.parseInt(parameter))
                .findAny();

        if (prod.isPresent()) {
            return (AbstractProduct) prod.get();

        }
        return null;
    }

    private long countEntertainProductMin2SwitchOns() {
        return productStore.getProducts().stream()
                .filter(p -> p instanceof EntertainmentProduct)
                .map(p -> (EntertainmentProduct) p)
                .filter(e -> e.getSwitchOnCounter() < 4)
                .count();

    }

}
