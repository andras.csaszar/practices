/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h7f3digitsumofnumberrecursive;

/**
 *
 * @author andra
 */
public class H7F3DigitSumOfNumberRecursive {

    
    
    static int sumOfDigits(int number) {
        if (number == 0) {
            return 0;
        }

        return sumOfDigits(number / 10) + number % 10;

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        System.out.println(sumOfDigits(12345));

    }

}
