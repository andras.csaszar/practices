/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hero;

import stone.StoneType;

/**
 *
 * @author andra
 */
public class HeroFactory {

    private static final String EARTH = "0";
    private static final String NON_EARTH = "1";

    private static final int EARTH_PARAMETER_INDEX = 3;
    private static final int STONE_PARAMETER_INDEX = 2;
    private static final int POWER_PARAMETER_INDEX = 1;
    private static final int NAME_PARAMETER_INDEX = 0;

    public static AbstractHero create(String[] parameters) {

        String name = parameters[NAME_PARAMETER_INDEX];
        double power = Double.parseDouble(parameters[POWER_PARAMETER_INDEX]);
        StoneType stone = StoneType.valueOf(parameters[STONE_PARAMETER_INDEX]);

        if (EARTH.equals(parameters[EARTH_PARAMETER_INDEX])) {

            return new BornOnEarth(name, power, stone);

        }

        return new NotBornOnEarth(name, power, stone);

    }

}
