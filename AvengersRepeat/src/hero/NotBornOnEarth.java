/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hero;

import ability.ShootingFire;
import ability.Swimming;
import stone.StoneType;

/**
 *
 * @author andra
 */
public class NotBornOnEarth extends AbstractHero implements Swimming, ShootingFire{

    public NotBornOnEarth(String name, double power, StoneType stone) {
        super(name, power, stone);
    }

    @Override
    public void swim() {
        System.out.println("I swim.");
    }

    @Override
    public void shootFire() {
        System.out.println("I shoot fire(ball)");
    }
    
}
