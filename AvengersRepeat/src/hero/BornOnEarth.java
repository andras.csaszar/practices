/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hero;

import ability.Flying;
import stone.StoneType;

/**
 *
 * @author andra
 */
public class BornOnEarth extends AbstractHero implements Flying {

    private Passport passport;

    public BornOnEarth(String name, double power, StoneType stone) {
        super(name, power, stone);
        this.passport = new Passport();

    }

    @Override
    public void fly() {
        System.out.println("I fly");
    }

}
