/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hero;

import stone.StoneType;

/**
 *
 * @author andra
 */
public abstract class AbstractHero {
    private String name;
    private double power;
    private StoneType stone;

    public AbstractHero(String name, double power, StoneType stone) {
        this.name = name;
        this.power = power;
        this.stone = stone;
    }

    public String getName() {
        return name;
    }

    public double getPower() {
        return power;
    }

    public StoneType getStone() {
        return stone;
    }
    
   
    
    
}
