/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package read;

import exceptions.InvalidHeroNameException;
import hero.AbstractHero;
import hero.HeroFactory;
import report.Reporter;
import report.Saver;
import store.Fleet;

/**
 *
 * @author andra
 */
public class CommandHandler {

    private static final String DELIMITER = ";";
    private static final int MIN_CHAR_OF_NAME = 2;
    private static final int NAME_PARAMETER_INDEX = 0;

    private final Fleet store;
    private final Reporter reporter;
    private final Saver saver;

    public CommandHandler() {
        this.store = new Fleet();
        this.reporter = new Reporter(store);
        this.saver = new Saver(store);

    }

    public void process(String line) throws InvalidHeroNameException {
        String[] parameters = line.split(DELIMITER);
        checkNameRestriction(parameters[NAME_PARAMETER_INDEX]);
        AbstractHero hero = HeroFactory.create(parameters);
        store.add(hero);
    }

    private void checkNameRestriction(String name) throws InvalidHeroNameException {
        if (name.length() < 2) {
            throw new InvalidHeroNameException();

        }
    }

    public void print() {
        System.out.println(store.toString());

    }
    
    public void report(){
        reporter.generateReports();
    }

}
