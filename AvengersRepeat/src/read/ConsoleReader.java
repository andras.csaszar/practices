/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package read;

import exceptions.InvalidHeroNameException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andra
 */
public class ConsoleReader {

    private final static String STOP = "exit";
    private CommandHandler parser = new CommandHandler();

    public void read() {

        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                String line = br.readLine();

                if (STOP.equals(line) || line == null) {
                    break;
                }
            }
            parser.process(br.readLine());

        } catch (InvalidHeroNameException ex) {
            Logger.getLogger(ConsoleReader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ConsoleReader.class.getName()).log(Level.SEVERE, null, ex);
        }

        parser.print();
        parser.report();
        parser.save();
        
    }

}
