/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;

import hero.AbstractHero;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author andra
 */
public class Ship {

    private List<AbstractHero> heroes;
    private static final int CAPACITY = 4;
    private static final char NAME_INDEX = 1;

    public Ship() {
        heroes = new ArrayList<>();
    }

    public boolean add(AbstractHero hero) {
        if (hasCapacity()) {
            return heroes.add(hero);
        }
        return false;
    }

    public boolean hasCapacity() {
        return heroes.size() < 4;
    }

    public Comparator<AbstractHero> getComparatorByStonePower() {
        return new Comparator<AbstractHero>() {

            @Override
            public int compare(AbstractHero h1, AbstractHero h2) {
                return h1.getStone().getPower() - h2.getStone().getPower();
            }

        };

    }

    public Comparator<AbstractHero> getComparatorByFirstCharInName() {

        return new Comparator<AbstractHero>() {

            @Override
            public int compare(AbstractHero h1, AbstractHero h2) {
                return h1.getName().charAt(NAME_INDEX) - h2.getName().charAt(NAME_INDEX);
            }

        };
    }

    public void sortByComparator(Comparator<AbstractHero> cmp) {
        heroes.sort(cmp);
    }

    public List<AbstractHero> getHeroes() {
        return heroes;
    }

    @Override
    public String toString() {
        return "Ship{" + "heroes=" + heroes + '}';
    }

}
