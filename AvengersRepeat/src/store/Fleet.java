/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;

import hero.AbstractHero;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author andra
 */
public class Fleet {

    private List<Ship> ships;

    public Fleet() {
        this.ships = new ArrayList<>();
    }

    public boolean add(AbstractHero hero) {
        return findShip().add(hero);

    }

    public Ship findShip() {
        Ship ship = findLastShip();

        if (!ship.hasCapacity()) {
            ships.add(new Ship());
            ship = findLastShip();
        } 
        return ship;
    }

    public Ship findLastShip() {

        if (ships.isEmpty()) {

            ships.add(new Ship());
        }
        return ships.get(ships.size() - 1);
    }

    public List<Ship> getShips() {
        return ships;
    }

    @Override
    public String toString() {
        return "Fleet{" + "ships=" + ships + '}';
    }

}
