/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import store.Fleet;

/**
 *
 * @author andra
 */
public class Saver {

    private static final String FILENAME = "Fleet.txt";
    private Fleet fleet;

    public Saver(Fleet fleet) {
        this.fleet = fleet;
    }

    public void serializeFleet() {
        try (FileOutputStream fos = new FileOutputStream(FILENAME);
                ObjectOutputStream oos = new ObjectOutputStream(fos);) {

            oos.writeObject(fleet);

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
