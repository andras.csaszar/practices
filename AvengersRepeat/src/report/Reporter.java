/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import hero.AbstractHero;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import stone.StoneType;
import store.Fleet;
import store.Ship;

/**
 *
 * @author andra
 */
public class Reporter {

    private Fleet fleet;

    public Reporter(Fleet fleet) {
        this.fleet = fleet;
    }

    public void generateReports() {
        printCountStonesByShip();
        countOfBornOnEarth();
        maxPowerHero();
        listPassports();
    }

    private Map<StoneType, Integer> countStonesInAShip(Ship ship) {
        return ship.getHeroes().stream()
                .map(AbstractHero::getStone)
                .collect(Collectors.toMap(
                                k -> k,
                                v -> 1,
                                (v1, v2) -> v1 + v2,
                                HashMap::new
                        ));

    }

    private Map<Ship, Map<StoneType, Integer>> countStonesByShips() {
        return fleet.getShips().stream()
                .collect(Collectors.toMap(
                                k -> k,
                                this::countStonesInAShip,
                                (v1, v2) -> v1,
                                HashMap::new
                        ));

    }

    private void printCountStonesByShips() {
        countStonesByShips().entrySet().stream()
                .forEach(i -> {
                    System.out.println("Ship: " + i);
                    i.getValue()
                    .forEach((j, k)
                            -> System.out.println("Stone: " + j + " Count: " + k));
                });
    }

}
