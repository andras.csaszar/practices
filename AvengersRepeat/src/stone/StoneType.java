/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stone;

/**
 *
 * @author andra
 */
public enum StoneType {

    SPACE("RED", 3),
    FORCE("YELLOW", 2),
    REALITY("BLUE", 6),
    MIND("WHITE", 8),
    TIME("GREEN", 5),
    SOUL("BLACK", 7);

    private String color;
    private int power;

    private StoneType(String color, int power) {
        this.color = color;
        this.power = power;
    }

    public String getColor() {
        return color;
    }

    public int getPower() {
        return power;
    }

}
