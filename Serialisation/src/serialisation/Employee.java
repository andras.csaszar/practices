/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serialisation;

import java.io.Serializable;

/**
 *
 * @author andra
 */
public class Employee implements Serializable{
    
    
    
   /*transient*/ private int age;
    /*static*/ private int salary;
    private String name;
    private Department department;

    public Employee(int age, int salary, String name, Department department) {
        this.age = age;
        this.salary = salary;
        this.name = name;
        this.department = department;
    }

    @Override
    public String toString() {
        return "Employee{" + "age=" + age + ", salary=" + salary + ", name=" + name + ", department=" + department + '}';
    }





  

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
    
    
    
}
