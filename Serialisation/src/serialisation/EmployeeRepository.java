/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serialisation;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andra
 */
public class EmployeeRepository {

    private static final String FILENAME = "employee.ser";
    private List<Employee> employees;

    public List<Employee> getEmployees() {
        return employees;
    }

    public Employee getEmployeeByName(String name) {
        return this.employees.stream()
                .filter(p -> name.equals(p.getName()))
                .findAny().orElseThrow(() -> new NullPointerException()); /*get();*/
                //optional-al is vissza lehet térni
    }

    public void addEmployee(Employee emp) {
        this.employees.add(emp);
    }

    public void removeEmployee(Employee emp) {
        this.employees.remove(emp);
    }

    public void updateEmployeeAge(Employee emp, int newAge) {
        //equals + hashCode-ot Employee-nak, hogy az indexOf működjön
        int empIndex = employees.indexOf(emp);
        this.employees.get(empIndex).setAge(newAge);
    }

    public void updateEmployeeSalary(Employee emp, int newSalary) {

        int empIndex = employees.indexOf(emp);
        this.employees.get(empIndex).setSalary(newSalary);

    }

    public void updateEmployeeDepartment(Employee emp, Department newDepartment) {

        int empIndex = employees.indexOf(emp);
        this.employees.get(empIndex).setDepartment(newDepartment);

    }

    public static void serializeEmployee(Employee emp) {

        try (FileOutputStream fs = new FileOutputStream(FILENAME);
                ObjectOutputStream ou = new ObjectOutputStream(fs)) {
            ou.writeObject(emp);

        } catch (FileNotFoundException ex) {
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public static Employee deSerializeEmployee() {
        Employee emp = null;
        try (FileInputStream fs = new FileInputStream(FILENAME);
                ObjectInputStream oi = new ObjectInputStream(fs)) {
            emp = (Employee) oi.readObject();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
        return emp;
    }

}
