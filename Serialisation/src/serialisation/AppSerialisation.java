/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serialisation;

/**
 *
 * @author andra
 */
public class AppSerialisation {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Employee emp = new Employee(27, 10000, "Emp1", new Department ("kis utca", "IT"));
        System.out.println("Employee before erialization");
        System.out.println(emp);
        EmployeeRepository.serializeEmployee(emp);

        Employee result = EmployeeRepository.deSerializeEmployee();
        System.out.println("Employee before erialization");
        System.out.println(result);

    }

}
