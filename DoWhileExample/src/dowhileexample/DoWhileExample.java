/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dowhileexample;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class DoWhileExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Példa 1.
        do {

            System.out.println("Legalább 1-szer lefut.");

        } while (false);

        // Példa 2. Addig kérünk be számokat amíg 0-t nem ad
        Scanner sc = new Scanner(System.in);
        int stopNumber = 0;
        int number;

        do {
            number = sc.nextInt();
            if (number != stopNumber) {
                System.out.println(number);
            }
        } while (number != stopNumber);

    }

}
