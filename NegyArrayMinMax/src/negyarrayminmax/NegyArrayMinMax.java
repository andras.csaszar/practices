/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negyarrayminmax;

/**
 *
 * @author andra
 */
public class NegyArrayMinMax {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        int[] numbers = {21, 124, 12, 1, 15, 687, 5, 0, 7, 1, 87};
        //A tömb legnagyobb eleme.

        int max = 0;

        for (int i = 0; i < numbers.length; i++) {

            if (numbers[i] > max) {
                max = numbers[i];
            }
        }
        System.out.println("Maximum: " + max);

        /// negatív számokra is érvényes
        int[] numbersN = {21, 124, 12, -1, 15, 687, 5, 0, -7, 1, 87};
   

        int maxN = numbersN[0];  // a tömb első elemét vesszük annak

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

    }

}
