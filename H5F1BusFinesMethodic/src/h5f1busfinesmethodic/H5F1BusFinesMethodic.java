/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h5f1busfinesmethodic;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class H5F1BusFinesMethodic {
    static void reAskInputUntilValid(Scanner scan, int numerOfInput, int [] array, ) {
       
        //todo


    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int numberOfInput = 0;
        double[] hourlyFines = new double[24];

        boolean allInputValid = true;
        boolean inputEnded = false;

        System.out.println("Adja meg az adatoka szóközzel elválasztva: óra perc típus összeg.\nHa végzett adja be a 0 0 x 0 számsort");
        do {
            allInputValid = true;
            int hour = -1;
            int minute = -1;
            String type = "a";
            int amount = -1;

            if (sc.hasNextInt()) {
                hour = sc.nextInt();
                if (hour < 0 || hour > 24) {
                    allInputValid = false;
                    //sc.next();
                }
            } else {
                allInputValid = false;
                sc.next();
            }
            if (sc.hasNextInt()) {
                minute = sc.nextInt();
                if (minute < 0 || minute > 60) {

                    allInputValid = false;
                    //sc.next();
                }
            } else {
                allInputValid = false;
                sc.next();
            }
            if (sc.hasNext("c") || sc.hasNext("h") || sc.hasNext("x")) {
                type = sc.next();
                // az x-et később csekkoljuk a környezetében (többi inputtól függ, hogy valid-e)
            } else {
                allInputValid = false;
                //sc.next();
            }

            if (sc.hasNextInt()) {
                amount = sc.nextInt();
                if (amount < 0) {
                    allInputValid = false;
                    //sc.next();
                }
            } else {
                allInputValid = false;
                sc.next();
            }
            //
            inputEnded = ("" + hour + " " + minute + " " + type + " " + amount).equals("0 0 x 0");

            if (!inputEnded && type.equals("x")) {
                allInputValid = false;
            }

            //System.out.println(allInputValid);
            //System.out.println(inputEnded);
            System.out.print(allInputValid ? "-" : "Helytelen adatok, vigye fel újra");
            System.out.print(inputEnded ? "Adatbevitel vége!\n" : "-\n");

            if (allInputValid) {
                hourlyFines[hour] += type.equals("c") ? 0.8 * amount : amount;
            }

        } while (allInputValid && !inputEnded);

        for (int i = 0; i < hourlyFines.length; i++) {

            System.out.printf("%d:00-%d:59, %.0f\n", i, i, hourlyFines[i]);

        }
        
    }
    
}
