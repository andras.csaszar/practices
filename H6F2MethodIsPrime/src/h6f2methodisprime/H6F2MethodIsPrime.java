/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h6f2methodisprime;

import java.util.Scanner;


/*
5. feladat:
Írj függvényt, amely a paramétereként átvett pozitív, egész számról eldönti, hogy az prím-e! Logikai
visszatérése legyen, amely akkor IGAZ, ha a paraméterként átvett szám prím, különben HAMIS.
Írj programot, amely a felhasználótól bekér egy számot, és a fenti függvény segítségével kiírja a
képernyőre az annyiadik prímszámot.
*/

/**
 *
 * @author andra
 */
public class H6F2MethodIsPrime {

    static final Scanner SCANNER = new Scanner(System.in);
    static int number;

    static void swallowNotInt() {
        SCANNER.next();
    }

    static int getPositiveNumber() {

        do {
            System.out.println("Give me a positive integer number and I will tell you all the primes up to that number");
            if (SCANNER.hasNextInt()) {
                number = SCANNER.nextInt();
                if (number > 0) {
                    return number;
                }

            } else {
                swallowNotInt();
            }
        } while (true);

    }

    static boolean isPrime(int number) {
        boolean isPrime = true;

        for (int i = 2; i <= Math.sqrt((double) number); i++) {//121-re pl. kell a négyzetgyöke
            if (number % i == 0) {
                isPrime = false;
            }
        }
        return isPrime && number != 1 && number!=0;
    }


    static int getPrimeNumberTillNumber(int number) {
        int temp = 0;
        int countPrime =0;
        
        while (countPrime < number) {
            if (isPrime(temp)) {
                countPrime++;
            }
            temp++;
        }
        return temp-1;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.printf("A(z) %d-ik prímszám, a: %d.\n ", getPositiveNumber(), getPrimeNumberTillNumber(number));
    }

}
