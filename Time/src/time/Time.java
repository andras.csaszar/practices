/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package time;

import java.time.LocalDate;
import static java.time.LocalDate.now;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

/**
 *
 * @author andra
 */
public class Time {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        LocalDate localDate = LocalDate.now();
        System.out.println(localDate);

        localDate = LocalDate.of(1848, 3, 15);
        System.out.println(localDate);

        localDate = LocalDate.parse("1985-01-19");
        System.out.println(localDate);

        localDate = LocalDate.parse("1985.01.19", DateTimeFormatter.ofPattern("yyyy.MM.dd"));
        System.out.println(localDate);

        System.out.println(localDate.format(DateTimeFormatter.ofPattern("yyy/MM/dd")));
        System.out.println(localDate.plusDays(128));
        System.out.println(localDate.plusWeeks(256));
        System.out.println(localDate.isLeapYear());//
        System.out.println(localDate.minus(10, ChronoUnit.MONTHS));

        LocalTime localTime = LocalTime.now();
        System.out.println(localTime);
        localTime = LocalTime.of(6, 30);
        System.out.println(localTime);
        localTime = LocalTime.parse("21:30");
        System.out.println(localTime);

        System.out.println(localTime.format(DateTimeFormatter.ofPattern("hh:mm")));

        System.out.println(localTime.format(DateTimeFormatter.ofPattern("HH:mm")));
    
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println(localDateTime);
        
        LocalDateTime endOfLesson = LocalDateTime.of(2019, 12, 8, 18, 0);
        System.out.println(localDateTime);
        
        for(String zone: ZoneId.getAvailableZoneIds()){
            System.out.println(zone);
        }
        LocalDateTime now = LocalDateTime.now();
        ZonedDateTime zdt = ZonedDateTime.of(now, ZoneId.of("America/Puerto_Rico"));
        System.out.println(zdt);
        
//        Period.between(localDate, localDate)
        
       System.out.println( ChronoUnit.DAYS.between(localDateTime, localDate));
    }

}
