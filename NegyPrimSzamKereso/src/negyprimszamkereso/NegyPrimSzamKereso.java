/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negyprimszamkereso;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class NegyPrimSzamKereso {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        System.out.println("Adjon meg egy számot!");
        int number = sc.nextInt();
        int countDivisor = 0;
        for (int i = 1; i < number + 1; i++) {

            if (number % i == 0) {
                countDivisor++;
            }

        }

        if (countDivisor < 2) {
            System.out.printf("A megadott szám: %d prím szám\n", number);
        } else {
            System.out.printf("A megadott szám: %d NEM prím szám\n", number);
        }

        /////////////////////// másik megoldás
        
        System.out.println("Másik módszerrel");
                
        boolean isPrime = true;

        for (int i = 2; i < Math.sqrt(number) + 1; i++) {

            if (number % i == 0) {
                isPrime = false;
            }
        }

        if (!isPrime || number == 1) {
            System.out.println("nem prím");
        } else {
            System.out.println("prim");
        }

    }

}
