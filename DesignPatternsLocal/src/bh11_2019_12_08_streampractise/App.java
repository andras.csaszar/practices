package bh11_2019_12_08_streampractise;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {

    static List<Country> countries = new ArrayList<>();

    public static void main(String[] args) {
        generateData();
        //printCountOfCountriesWhereNameContains("S");
        //printCountryByCode("hu");
        //printAllCities();
        //sortAllOfCountry();
        //printContinents();
        printAllCities();
        printAllCountries();
        System.out.println("Sorted by Continent descending");
        printSortedCountriesByContinentDesc();
        System.out.println("SortedCountriesByCountOfCities");
        printSortedCountriesByCountOfCities();
        System.out.println("printCountryWhichContainsPartOfCityName");
        printCountryWhichContainsPartOfCityName("Ap");

        printCountrySizes();
    }

    public static void generateData() {
        Country hungary = new Country("hu", "Hungary", "Europe", 93);
        hungary.addCity(new City("Budapest", 2000));
        hungary.addCity(new City("Szeged", 800));
        hungary.addCity(new City("Nyíregyháza", 200));
        hungary.addCity(new City("Tokaj", 30));

        Country spain = new Country("sp", "Span", "Europe", 20);
        spain.addCity(new City("Barcelona", 300));
        spain.addCity(new City("Madrid", 250));

        Country thailand = new Country("th", "Thailand", "Asia", 200);
        thailand.addCity(new City("Phuket", 50));
        thailand.addCity(new City("Bangkok", 3000));
        //András addedd
        thailand.addCity(new City("Mae Hong Son", 7066));
        thailand.addCity(new City("Laem Chabang", 61_801));
        thailand.addCity(new City("Pattaya", 1_000_000));

        countries.add(hungary);
        countries.add(hungary);
        countries.add(spain);
        countries.add(thailand);

    }

    //basic
    public static void printAllCountries() {
        countries.stream().forEach((x) -> System.out.println(x));
        //countries.stream().forEach(System.out::println);
    }

    //basic
    public static void printDistinctCountries() {
        countries.stream().distinct().forEach((x) -> System.out.println(x));
    }

    //basic
    public static void printCountriesWhereNameContains(String str) {
        countries.stream().filter((x) -> x.getName().contains(str)).forEach((country) -> System.out.println(country));
    }

    //basic
    public static void printCountOfCountriesWhereNameContains(String str) {
        System.out.println(countries.stream().filter((x) -> x.getName().contains(str)).count());
    }

    //basic
    public static void printCountryByCode(String code) {
        countries.stream().filter((x) -> x.getCode().equals(code)).forEach((country) -> System.out.println(country));
    }

    //basic
    public static void sortAllOfCountry() {
        //countries.stream().sorted((c1, c2)->c1.getSize()-c2.getSize()).forEach((x)->System.out.println(x));
        countries
                .stream()
                .sorted((c1, c2) -> c1.getName().compareTo(c2.getName()))
                .forEach((x) -> System.out.println(x));
    }

    //extra
    public static void printAllCities() {
        countries
                .stream()
                .forEach((country)
                        -> country.
                        getCities().
                        stream().
                        forEach((city) -> System.out.println(city)));

        //második megoldás, az első a preferált, csak megértés miatt
        List<List<City>> list = countries
                .stream()
                .map((country) -> country.getCities())
                .collect(Collectors.toList());

        List<City> merged = new ArrayList<>();
        for (List<City> subList : list) {
            merged.addAll(subList);
        }

        merged.stream().forEach((x) -> System.out.println(x));
    }

    //basic
    public static void printContinents() {
        //countries.stream().forEach((country)->System.out.println(country.getContinent()));
        countries
                .stream()
                .map((country) -> country.getContinent())
                .distinct()
                .forEach((c) -> System.out.println(c));
        //List<String> mapList = countries.stream().map((country)->country.getContinent()).collect(Collectors.toList());
    }

    //basic
    public static void printCountryNames() {
        countries.stream().forEach((country) -> System.out.println(country.getName()));
    }

    //basic
    public static void printCountryCodes() {
        countries.stream().forEach((country) -> System.out.println(country.getCode()));
    }

    //extra
    public static void printSummOffPopulation() {
        countries
                .stream()
                .mapToInt(
                        (country) -> country.getCities()
                        .stream()
                        .mapToInt(city -> city.getPopulation())
                        .sum())
                .sum();
        //countries.get(0).getCities().stream().mapToInt(city->city.getPopulation()).sum();
    }

    //extra
    public static void printSummOfEuropePopulation() {
        int population = countries
                .stream()
                .filter((c) -> c.getContinent().equals("Europe"))
                .mapToInt(
                        (country) -> country.getCities()
                        .stream()
                        .mapToInt(city -> city.getPopulation())
                        .sum())
                .sum();

    }

    //extra
    public static void printAllOfCountriesPopulationLessThanX(int x) {
        int populationAllLessThanX = countries.stream()
                .mapToInt(country -> country.getCities()
                        .stream()
                        .mapToInt(city -> city.getPopulation())
                        .sum())
                .filter(pop -> pop < x)
                .sum();
        System.out.println(populationAllLessThanX);
    }

    ///////////////------------------- 
    //HF
    //kontinens neve alapján legyen abc sorrendben csökkenő
    public static void printSortedCountriesByContinentDesc() {
        countries.stream()
                .sorted((c1, c2) -> c2.getContinent().toString().compareTo(c1.getContinent().toString()))
                .forEach(System.out::println);
    }

    //városok darabszáma alapján legyen sorrendben
    public static void printSortedCountriesByCountOfCities() {
        countries.stream()
                .distinct()
                .sorted((ctry1, ctry2) -> ctry1.getCities().size() - ctry2.getCities().size())
                //.mapToInt(ctry-> ctry.getCities().size())
                .forEach(System.out::println);

        /*       .
         .flatMap(cities-> cities.stream())
         .sorted(cities -> ci)
       
         */
    }

    //kap egy név részletét a városnak, írja ki azokat az országokat ahol ez megtalálható, ne legyen casesensitive
    public static void printCountryWhichContainsPartOfCityName(String partOfCityName) {
        countries.stream()
                .map(ctry -> ctry.getCities())
                .flatMap(cities -> cities.stream())
                .filter(c -> c.getName().toLowerCase().contains(partOfCityName.toLowerCase()))
                .forEach(System.out::println);

                //        .filter(ctry-> (ctry.getCities().stream().filter(a->a.getName().contains(partOfCityName))))
        ////                forEach(a->a.toString().contains(partOfCityName)))
        countries
                .stream()
                .filter((p) -> p.getCities()
                        .toString()
                        .toLowerCase()
                        .contains(partOfCityName.toLowerCase()))
                .forEach(System.out::println);

    }

    //írja ki az országokhoz tartozó méreteket
    public static void printCountrySizes() {
        countries.stream()
                .mapToInt(ctry -> ctry.getSize())
                .forEach(System.out::println);
    }
}
