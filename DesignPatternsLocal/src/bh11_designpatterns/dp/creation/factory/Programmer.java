package bh11_designpatterns.dp.creation.factory;

public class Programmer extends AbstractAnimal{
    @Override
    public void sayHello() {
        System.out.println("Hello World");
    }    
}
