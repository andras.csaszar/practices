package bh11_designpatterns.dp.creation.factory;

public abstract class AbstractAnimal {
    public abstract void sayHello();
}
