<%-- 
    Document   : deserts
    Created on : 2020.01.24., 17:35:45
    Author     : andra
--%>

<%@page import="java.util.List"%>
<%@page import="hu.braininghub.bh11.myfirstapi.Desert"%>
<%@page import="hu.braininghub.bh11.myfirstapi.Desert"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>See the deserts!</h1>
        <% List<Desert> deserts = (List<Desert>) request.getAttribute("deserts");%>


        [ 
        {
        "name":"xxx1",
        "size":"yyy1"
        },
        {
        "name":"xxx2",
        "size":"yyy2"
        }
        .
        .
        .
        ]

        <%  for (int i = 0; i < deserts.size(); i++) {%>

        <%if (i == 0) {%>
        <p> [ </p>
        <%}%>
        <p>{</p>
        <p>name:<%= deserts.get(i).getName()%>, </p>
        <p> size:<%= deserts.get(i).getSize()%></p>
        <p>} 

            <%if (i != deserts.size() - 1) {%>
            ,</p>

        <%} else {%>
        <p> ] </p>
        <%}%>

        <% }%>

        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Size</th>
                </tr>
            </thead>
            <tbody>
                <%  for (int i = 0; i < deserts.size(); i++) {%>
                <tr>
                    <td> <%= deserts.get(i).getName()%></td>
                    <td> <%= deserts.get(i).getSize()%></td>
                </tr>
                <% }%>
            </tbody>
        </table>  

    </body>
</html>
