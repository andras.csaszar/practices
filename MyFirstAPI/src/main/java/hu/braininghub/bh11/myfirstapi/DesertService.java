/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.myfirstapi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author andra
 */
public class DesertService {

    private static List<Desert> deserts = new ArrayList<>();
    private static final File FILE_PATH = new File("C:\\Users\\andra\\Documents\\GitProjects\\andrascodes\\MyFirstAPI\\Deserts.txt");
    private static final String ATTRIBUTE_SPLITTER = "\\|";

    public DesertService() {
        transformAndAddAllReadDataFromStringToDesert(readDeserts());
    }

    public List<Desert> getDeserts() {
        return deserts;
    }

    private List<String> readDeserts() {
        List<String> desertLines = null;

        try (BufferedReader br = new BufferedReader(new FileReader(FILE_PATH))) {

            desertLines = br.lines().collect(Collectors.toList());

        } catch (FileNotFoundException ex) {
            Logger.getLogger(DesertService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return desertLines;
    }

    private Desert transformStringToDesert(String line) {
        String oneDesert[] = line.split(ATTRIBUTE_SPLITTER);

        return new Desert(oneDesert[0], Integer.parseInt(oneDesert[1]));
    }

    private void transformAndAddAllReadDataFromStringToDesert(List<String> desertsAsString) {

        desertsAsString.stream().forEach((desertString) -> {
            deserts.add(transformStringToDesert(desertString));
        });
    }

}
