/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h5f3arraysearchbyinput;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class H5F3ArraySearchByInput {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);

        int numberOfInput = 20;
        System.out.printf("Adjon meg %d számot egymás után!\n", numberOfInput);
        int[] array = new int[numberOfInput];

        for (int i = 0; i < array.length; i++) {
            if (sc.hasNextInt()) {
                array[i] = sc.nextInt();
            } else {
                System.out.println("Nem egész szám!");
                i--;
                sc.next();
            }
        }
        boolean isInt;
        System.out.println("Adjon meg egy egész számot, amit megkeresünk az eddigiek közül!");
        int number = 0;
        do {
            isInt = true;
            if (sc.hasNextInt()) {
                number = sc.nextInt();
            } else {
                System.out.println("Nem egész szám!");
                isInt = false;
                sc.next();
            }

        } while (!isInt);

        int numbIndex = 0;//todo
        boolean found = false;

        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                numbIndex = i;
                found = true;
            } else {

            }
        }

        if (found) {
            System.out.printf("A keresett szám indexe: %d.\n", numbIndex);
        } else {
            System.out.println("Nincs ilyen szám!\n");
            ;
        }

    }

}
