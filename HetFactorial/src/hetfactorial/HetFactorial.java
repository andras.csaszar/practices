/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hetfactorial;

/**
 *
 * @author andra
 */
public class HetFactorial {

    static int calculateFactorial(int number) {

        int factor = 1;
        for (int i = 1; i <= number; i++) {
            factor *= i;

        }
        return factor;
        // a 0! az = 1 (matematika.. :))
    }

    static int recursiveFact(int number) {
        if (number == 0 || number == 1) {
            return 1;
        }
        
        return number*recursiveFact(number-1);
    }

    /*
    recursiveFact(5);
    5*recursiveFact(4);
        5*4*recursiveFact(3);
            5*4*3*recursiveFact(2);
            5*4*3*2*recursiveFact(1);
                5*4*3*2*1;
    */
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        int number = 5;

        System.out.println(calculateFactorial(number));
    }

}
