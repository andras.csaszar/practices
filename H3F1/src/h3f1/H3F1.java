/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h3f1;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class H3F1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // 1. Kérj be egy számot, írd ki a páros osztóit!

        System.out.printf("Adjon meg egy számot (max/min +/- %d), kiírom a páros osztóit!\n", Integer.MAX_VALUE);
        Scanner sc = new Scanner(System.in);
        boolean isInt = sc.hasNextInt();
        if (!isInt) {            
            System.out.println("Egész számot kértem!");
        } else {
            int number = sc.nextInt();
            int a = 0;
            boolean hasEvenDivider = false;
            for (int i = 1; i < number + 1; i++) {

                if (number % i == 0 && i % 2 == 0) {
                    System.out.printf("%d. páros osztó: %d\n", a, i);
                    a++;
                    hasEvenDivider = true;
                }
            }
            if (!hasEvenDivider) {
                System.out.println("Nincs páros osztója.");
            }
        }
    }

}
