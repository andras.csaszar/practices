/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h6f4methodarraytoreverse;

/*
 8. feladat
 Írj függvényt, ami egy tömböt átvesz paraméterként és hátulról indulva kiírja minden második elemét!
 Ügyelj arra, hogy nehogy túl/alulindexeld a tömböt!
 */
/**
 *
 * @author andra
 */
public class H6F4MethodArrayToReverse {

    static void printArrayReverse(int arr[]) {
        for (int i = arr.length - 2; i > -1; i = i - 2) {
            System.out.print(arr[i] + "\n");
        }

    }

    static void printArrayReverse(char arr[]) {
        for (int i = arr.length - 2; i > -1; i = i - 2) {
            System.out.print(arr[i] + "\n");
        }

    }

    static void printArrayReverse(String arr[]) {
        for (int i = arr.length - 2; i > -1; i = i - 2) {
            System.out.print(arr[i] + "\n");
        }

    }

    static void printArrayReverse(double arr[]) {
        for (int i = arr.length - 2; i > -1; i = i - 2) {
            System.out.print(arr[i] + "\n");
        }

    }

    static void printArrayReverse(float arr[]) {
        for (int i = arr.length - 2; i > -1; i = i - 2) {
            System.out.print(arr[i] + "\n");
        }

    }

    static void printArrayReverse(byte arr[]) {
        for (int i = arr.length - 2; i > -1; i = i - 2) {
            System.out.print(arr[i] + "\n");
        }

    }

    static void printArrayReverse(short arr[]) {
        for (int i = arr.length - 2; i > -1; i = i - 2) {
            System.out.print(arr[i] + "\n");
        }

    }

    static void printArrayReverse(long arr[]) {
        for (int i = arr.length - 2; i > -1; i = i - 2) {
            System.out.print(arr[i] + "\n");
        }

    }

    static void printArrayReverse(boolean arr[]) {
        for (int i = arr.length - 2; i > -1; i = i - 2) {
            System.out.print(arr[i] + "\n");
        }

    }

    static int getRandomInt(int from, int to) {
        
        return (int) (Math.random()*(to-from)+from);
    
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        int array[] = new int[15];
        System.out.print("A tömb:\n");
        for (int i = 0; i < array.length; i++) {
            array[i] = getRandomInt(1,100);
            System.out.printf("%d\n", array[i]);
        }

        char arrChar[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g'};
        //--
        System.out.print("Függvénnyel visszafele, minden második:\n");
        printArrayReverse(array);

    }

}
