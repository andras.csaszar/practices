/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package read;

import exceptions.InvalidNameAvengersException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author andra
 */
public class ConsoleReader {

    private static final String STOP = "EXIT";
    private LineParser parser = new LineParser();

    public void read() {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {

            while (true) {
                String line = br.readLine();
                if (line == null || STOP.equals(line)) {
                    break;
                }
                parser.process(line);
            }

        } catch (IOException ex) {
            System.out.println(ex);
        } catch (InvalidNameAvengersException ex) {
            System.out.println(ex.getMessage());
        }
        parser.print();
    }
}
