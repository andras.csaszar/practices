/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hero;

import ability.Firing;
import ability.Swimming;
import stone.StoneType;

/**
 *
 * @author andra
 */
public class NotBornOnEarth extends AbstractHero implements Swimming, Firing {

    public NotBornOnEarth(String name, int power, StoneType stone) {
        super(name, power, stone);
    }

    @Override
    public void swim() {
        System.out.println("I can swim");
    }

    @Override
    public void fire() {
        System.out.println("I can throw fireballs");
    }

}
