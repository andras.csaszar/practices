/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tizennegylists;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author andra
 */
public class AppList {

    public static void main(String[] args) {
        List<Integer> primeNumbers = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            if (isPrime(i)) {

                primeNumbers.add(i);
            }
        }
        System.out.println(primeNumbers);
        
        for(int n : primeNumbers){
            System.out.println(n);
        }
        
        //VAGY
        
        for (int i = 0; i < primeNumbers.size(); i++) {
            System.out.println(primeNumbers.get(i));
        }
    }

    public static boolean isPrime(int n) {
        if (n <= 1) {
            return false;
        }

        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

}
