package hu.braininghub.fare.service;

import hu.braininghub.fare.dto.FareDto;
import hu.braininghub.fare.repository.FareRepository;
import hu.braininghub.fare.repository.entity.Fare;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FareService {

    @Autowired
    private FareRepository fareRepository;

    public List<FareDto> findByOriginAndDestination(String origin, String destination) {

        List<Fare> fares = fareRepository.filterFares(origin, destination);
        List<FareDto> fareDtos = new ArrayList<>();
        fares.forEach(fare -> {
            FareDto dto = new FareDto();
            BeanUtils.copyProperties(fare, dto);

            fareDtos.add(dto);
        });

        return fareDtos;
    }
}
