package hu.braininghub.fare.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class FareFilterResponse implements Serializable {

    @JsonProperty("_fares")
    private List<FareDto> fareDtos;
    public List<FareDto> getFareDtos() {
        return fareDtos;
    }
    public void setFareDtos(List<FareDto> fareDtos) {
        this.fareDtos = fareDtos;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FareFilterResponse that = (FareFilterResponse) o;
        return Objects.equals(fareDtos, that.fareDtos);
    }
    @Override
    public int hashCode() {
        return Objects.hash(fareDtos);
    }
    @Override
    public String toString() {
        return "FareFilterResponse{" +
                "fareDtos=" + fareDtos +
                '}';
    }

}
