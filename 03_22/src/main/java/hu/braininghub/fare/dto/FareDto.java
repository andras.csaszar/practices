package hu.braininghub.fare.dto;

import hu.braininghub.fare.repository.entity.Fare;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

public class FareDto implements Serializable {


        private BigDecimal id;

        private String carrier;

        private String origin;

        private String destination;

        private String fareClassCode;

        private Integer bookingClass;

        private LocalDate validFrom;

        private LocalDate validTo;

        public BigDecimal getId() {
            return id;
        }
        public void setId(BigDecimal id) {
            this.id = id;
        }
        public String getCarrier() {
            return carrier;
        }
        public void setCarrier(String carrier) {
            this.carrier = carrier;
        }
        public String getOrigin() {
            return origin;
        }
        public void setOrigin(String origin) {
            this.origin = origin;
        }
        public String getDestination() {
            return destination;
        }
        public void setDestination(String destination) {
            this.destination = destination;
        }
        public String getFareClassCode() {
            return fareClassCode;
        }
        public void setFareClassCode(String fareClassCode) {
            this.fareClassCode = fareClassCode;
        }
        public Integer getBookingClass() {
            return bookingClass;
        }
        public void setBookingClass(Integer bookingClass) {
            this.bookingClass = bookingClass;
        }
        public LocalDate getValidFrom() {
            return validFrom;
        }
        public void setValidFrom(LocalDate validFrom) {
            this.validFrom = validFrom;
        }
        public LocalDate getValidTo() {
            return validTo;
        }
        public void setValidTo(LocalDate validTo) {
            this.validTo = validTo;
        }
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            FareDto fareDto = (FareDto) o;
            return id.compareTo(fareDto.id) == 0;
        }
        @Override
        public int hashCode() {
            return Objects.hash(id);
        }
        @Override
        public String toString() {
            return "FareDto{" +
                    "id=" + id +
                    ", carrier='" + carrier + '\'' +
                    ", origin='" + origin + '\'' +
                    ", destination='" + destination + '\'' +
                    ", fareClassCode='" + fareClassCode + '\'' +
                    ", bookingClass=" + bookingClass +
                    ", validFrom=" + validFrom +
                    ", validTo=" + validTo +
                    '}';
        }
    }

