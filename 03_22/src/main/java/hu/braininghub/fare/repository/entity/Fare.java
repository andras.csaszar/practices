package hu.braininghub.fare.repository.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

    @Entity
    @Table(name = "fare")
    public class Fare implements Serializable {

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        @Column(name="fare_id")
        private BigDecimal id;
        @Column(name = "carrier", length = 2)
        private String carrier;
        @Column(name = "origin", length = 3)
        private String origin;
        @Column(name = "destination", length = 3)
        private String destination;
        @Column(name = "fare_class_code", length = 8)
        private String fareClassCode;
        @Column(name = "booking_class")
        private Integer bookingClass;
        @Column(name = "valid_from")
        private LocalDate validFrom;
        @Column(name = "valid_to")
        private LocalDate validTo;
        public BigDecimal getId() {
            return id;
        }
        public void setId(BigDecimal id) {
            this.id = id;
        }
        public String getCarrier() {
            return carrier;
        }
        public void setCarrier(String carrier) {
            this.carrier = carrier;
        }
        public String getOrigin() {
            return origin;
        }
        public void setOrigin(String origin) {
            this.origin = origin;
        }
        public String getDestination() {
            return destination;
        }
        public void setDestination(String destination) {
            this.destination = destination;
        }
        public String getFareClassCode() {
            return fareClassCode;
        }
        public void setFareClassCode(String fareClassCode) {
            this.fareClassCode = fareClassCode;
        }
        public Integer getBookingClass() {
            return bookingClass;
        }
        public void setBookingClass(Integer bookingClass) {
            this.bookingClass = bookingClass;
        }
        public LocalDate getValidFrom() {
            return validFrom;
        }
        public void setValidFrom(LocalDate validFrom) {
            this.validFrom = validFrom;
        }
        public LocalDate getValidTo() {
            return validTo;
        }
        public void setValidTo(LocalDate validTo) {
            this.validTo = validTo;
        }

        public Fare() {
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Fare fare = (Fare) o;
            return id.compareTo(fare.id) == 0;
        }
        @Override
        public int hashCode() {
            return Objects.hash(id);
        }
        @Override
        public String toString() {
            return "Fare{" +
                    "id=" + id +
                    ", carrier='" + carrier + '\'' +
                    ", origin='" + origin + '\'' +
                    ", destination='" + destination + '\'' +
                    ", fareClassCode='" + fareClassCode + '\'' +
                    ", bookingClass=" + bookingClass +
                    ", validFrom=" + validFrom +
                    ", validTo=" + validTo +
                    '}';
        }
    }


