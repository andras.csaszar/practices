package hu.braininghub.fare.repository;

import hu.braininghub.fare.repository.entity.Fare;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface FareRepository extends JpaRepository<Fare, BigDecimal> {
    @Query("Select f from Fare f WHERE f.origin = :origin AND f.destination = :destination")
    List<Fare> filterFares(@Param("origin") String origin, @Param("destination") String destination);

    List<Fare> findByOriginAndDestination(String origin, String destination);
}
