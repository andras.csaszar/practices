package hu.braininghub.fare.rest;

import hu.braininghub.fare.dto.FareDto;
import hu.braininghub.fare.dto.FareFilterRequest;
import hu.braininghub.fare.dto.FareFilterResponse;
import hu.braininghub.fare.service.FareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sun.misc.Request;

import java.util.List;

@RestController
@RequestMapping(value = "/fare")
public class FareRestController {

    @Value("${fare.service.name}")
    private String serviceName;

    @Autowired
    private FareService fareService;

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String hello() {
        return "Hello World from " + serviceName + "!";

    }

    @RequestMapping(value = "/filter", method = RequestMethod.POST)
    public ResponseEntity filter(@RequestBody FareFilterRequest request) {
        List<FareDto> fares = fareService.findByOriginAndDestination(request.getOrigin(), request.getDestination());
        FareFilterResponse response = new FareFilterResponse();
        response.setFareDtos(fares);

        return ResponseEntity.ok(response);
    }
}
