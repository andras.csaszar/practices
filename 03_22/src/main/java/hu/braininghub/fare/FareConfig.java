package hu.braininghub.fare;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "hu.braininghub.fare.repository")
@EntityScan(basePackages = "hu.braininghub.fare.repository.entity")
//@ComponentScan(basePackages = "hu.braininghub.fare")   //ha több base package-ed van, akkor itt fel tudod sorolni
//azt { package1, package2, ...} felsorolással lehet
//@PropertySource("classpath:application.properties")
public class FareConfig {
}
