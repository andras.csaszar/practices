package hu.braininghub.fare;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Scanner;

@SpringBootApplication
public class Application {

   /* @Bean(destroyMethod = "close", initMethod = "")
    public Scanner scanner(){
        return new Scanner(System.in);

    }*/
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
