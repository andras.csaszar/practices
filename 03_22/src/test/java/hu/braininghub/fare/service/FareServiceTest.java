package hu.braininghub.fare.service;

import hu.braininghub.fare.dto.FareDto;
import hu.braininghub.fare.repository.FareRepository;
import hu.braininghub.fare.repository.entity.Fare;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = FareServiceTest.Config.class)
public class FareServiceTest {

    @TestConfiguration
    @ComponentScan(basePackageClasses = FareService.class)
    static class Config {
        @Bean
        public FareRepository fareRepository() {
            return Mockito.mock(FareRepository.class);
        }

    }

    private static final String ORIGIN = "BUD";
    private static final String DESTINATION = "FRA";

    @Autowired
    private FareRepository fareRepository;

    @Autowired
    private FareService underTest;

    @Test
    void test() {
        assertNotNull(fareRepository);
        assertNotNull(underTest);
    }

    @Test
    void testFilter() {
        //Given
        List<Fare> fares = Arrays.asList(createFare("A", "B"), createFare("C", "D"));
        when(fareRepository.filterFares(anyString(), anyString())).thenReturn(fares);
        //doReturn(fares).when(fareRepository).filterFares(anyString(), anyString());
        List<FareDto> expectedResult = Arrays.asList(createFareDto("A", "B"), createFareDto("C", "D"));


        //When
        List<FareDto> result = underTest.findByOriginAndDestination(ORIGIN, DESTINATION);

        //Then
        assertEquals(expectedResult, result);

    }

    private Fare createFare(String origin, String destination) {
        Fare fare = new Fare();
        fare.setOrigin(origin);
        fare.setDestination(destination);
        fare.setId(BigDecimal.ZERO);
        return fare;

    }

    private FareDto createFareDto(String origin, String destination) {
        FareDto fareDto = new FareDto();
        fareDto.setOrigin(origin);
        fareDto.setDestination(destination);
        fareDto.setId(BigDecimal.ZERO);
        return fareDto;

    }

}
