/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h6f3methodhigherabs;

import java.util.Scanner;
/*
6. feladat.
Írj függvényt, amely paraméterként átvesz két egész számot és visszaadja azt, amelyiknek az abszolút
értéke nagyobb!
*/

/**
 *
 * @author andra
 */
public class H6F3MethodHigherAbs {

    final static Scanner SCANNER = new Scanner(System.in);
    static int number1;
    static int number2;

    static int getNumberOfHigherAbs(int num1, int num2) {

        return Math.max(Math.abs(num1), Math.abs(num2)) == Math.max(num1, num2)? Math.max(Math.abs(num1), Math.abs(num2)):Math.max(Math.abs(num1), Math.abs(num2))*(-1);
   
        /*if the maximum of the absolute values is the same as the maximum of the numbers then gives the higher number,
          otherwise gives the number of higher absolute value multiplied by -1.*/
    }

    static void swallowNotInt() {
        SCANNER.next();
    }

    static int getIntNumber() {
        do {
            System.out.println("Give me an integer number!");
            if (SCANNER.hasNextInt()) {
                number1 = SCANNER.nextInt();

                return number1;

            } else {
                swallowNotInt();
            }
        } while (true);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        System.out.println(getNumberOfHigherAbs(getIntNumber(), getIntNumber()));
    
    }

}
