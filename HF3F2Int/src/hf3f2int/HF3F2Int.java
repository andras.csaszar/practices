/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hf3f2int;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class HF3F2Int {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        //2. Sorsolj ki egy egész számot az [1000;10000] intervallumból. Írd ki az első és utolsó számjegyét!
        int number = (int) (Math.random() * 9000 + 1000);
        /*String numberString = Integer.toString(number);
         char firstNum= numberString.charAt(0);
         char lastNum= numberString.charAt(numberString.length()-1);
         */

        int firstNum = -1;    //nem lehetséges eredmény, mint kezdőérték - így látjuk ha nem jó a program
        int lastNum = -1;     //nem lehetséges eredmény, mint kezdőérték - így látjuk ha nem jó a program
        //első számjegy meghatározása:

            for (int i = 10; i < 10001; i *= 10) {
                if (number / i > 0) {
                    firstNum = number / i;
                    
                }

            }
        //utolsó számjegy meghatározása:
        lastNum = number %10;
        System.out.printf("A szám: %d.\nElső számjegy: %d.\nMásodik számjegy: %d.\n", number, firstNum, lastNum);

    }

}
