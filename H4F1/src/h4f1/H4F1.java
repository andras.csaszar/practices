/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h4f1;

/**
 *
 * @author andra
 */
public class H4F1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*
         1. tomb, 50 elem, [-100, 100] értékkészlet, írjuk ki a következőket: 
         - maximum hely és érték,
         - minimum hely és érték, 
         - átlag 
         - összeg

         */
        int[] array = new int[50];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 201 - 100);
        }
        int maxValue = array[0];
        int maxIndex = 0;
        int minValue = array[0];
        int minIndex = 0;
        int sum=0;
        double average=0.0;
        
        for (int i = 0; i < array.length; i++) {
            if (array[i] > maxValue) {
                maxValue = array[i];
                maxIndex = i;
            }
            if (array[i] < minValue) {
                minValue = array[i];
                minIndex = i;
            }
            
            sum+=array[i];
            
            
        }
        average = sum / (1.0*array.length);
        
        System.out.printf("A maximum érték: %d.\n A maximum indexe: %d.\n"
                + "A minmum érték: %d.\n A minimum index: %d.\n"
                + "Az összeg: %d.\n Az álag: %f.\n", maxValue, maxIndex, minValue, minIndex, sum, average);
    }

}
