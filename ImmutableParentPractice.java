//--Immutable--
//-minden változó final
//-nincs setter
//-osztály final: nincs öröklés az osztályból
//-konstruktornál, ha referenciatípus változó van benne, le kell másolni új objektumok létrehozásával az osztályunk számára (26-os sor aztán 27-es sor).
//-gettereknél ha referenciatípust adnánk vissaz (pl. ArrayList, akkor azt le kell másolni, vagy Collections.unmodifiableList-tel kell visszaadni

public final class ImmutableParent impelemnts Comparable<ImmutableParent>{
	
	private final String name;
	private final int age;
	private final List<Child> children;
	
		public ImmutableParent (String name, int age, List<Child> children){
			this.name = name;
			this.age = age;
			this.children = new ArrayList<>();
			copyChildElements(children);
			
		}
		
		private void copyChildElements(List<Child> children){
			for (Child c : children){
				Child newChild = new Child (c.getAge(), c.getName());
				this.children.add(newChild);				
			}
		}
		
		public List<Child> getChildren(){
			return Collections.unmodifiableList(children);
		}
		
		public String getName(){
			return name;
		}
		public int getAge(){
			return age;
		}
	
}