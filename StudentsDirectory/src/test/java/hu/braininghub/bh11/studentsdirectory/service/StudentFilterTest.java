/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.studentsdirectory.service;

import hu.braininghub.bh11.studentsdirectory.repository.dto.StudentDto;
import hu.braininghub.bh11.studentsdirectory.repository.entity.Student;
import hu.braininghub.bh11.studentsdirectory.repository.entity.StudentDao;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.beanutils.BeanUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author andra
 */
@ExtendWith(MockitoExtension.class)
public class StudentFilterTest {

    @Mock
    private StudentDao dao;

    private StudentFilterService underTest;

    @BeforeEach
    void init() {
        underTest = new StudentFilterService();
        underTest.setDao(dao);
    }

    @Test
    public void testGetStudentsByNameBasedOnFirstNameWithTwoItems() throws IllegalAccessException, InvocationTargetException {
        //Given
        Student s = mock(Student.class);
        Student s2 = mock(Student.class);
        List<StudentDto> resultDtoList = new ArrayList<>();
        s.setFirstName("aaa");
        //When
        when(dao.findByName(s.getFirstName())).thenReturn(Arrays.asList(s, s2));
        List<Student> foundStudents = (List<Student>) dao.findByName(s.getFirstName());

        foundStudents.forEach(
                (fst) -> {
                    try {
                        StudentDto dto = new StudentDto();
                        BeanUtils.copyProperties(dto, fst);
                        resultDtoList.add(dto);
                    } catch (IllegalAccessException ex) {
                        Logger.getLogger(StudentFilterTest.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InvocationTargetException ex) {
                        Logger.getLogger(StudentFilterTest.class.getName()).log(Level.SEVERE, null, ex);
                    }

                });

        Iterable<StudentDto> studentList = new ArrayList<>();
        studentList = underTest.getStudentsByName(s.getFirstName());

        //Then
        Assertions.assertEquals(resultDtoList, studentList);

    }
}
