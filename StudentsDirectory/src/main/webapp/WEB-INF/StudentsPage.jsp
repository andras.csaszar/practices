<%-- 
    Document   : StudentsPage
    Created on : 2020. ápr. 19., 12:23:01
    Author     : andra
--%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <h1>Hello World!</h1>
        <c:set var="stud" value="${list}"/>
        <table>
            <tr>
                <th>
                    FirstName
                </th>
            </tr>
            <c:forEach var="s" items="${list}">
                <tr>
                    <td>
                        <c:out value="${s.firstname}"/> 
                    </td>

                </tr>

            </c:forEach>


        </table>
    </body>
</html>
