<%-- 
    Document   : index.jsp
    Created on : 2020. ápr. 19., 12:48:23
    Author     : andra
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <!--  <meta http-equiv="refresh" content="0; URL=/letterController" /> -->
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>

        <form class="ng-pristine ng-valid searchbox_form" id="filter_form"
              method="post" action="studentfilter" name="filter_form">
            <label for="name">First Name starts with:</label>
            <select id="name" name="fname"><br><br>
                <c:forEach var="l" items="${letters}">
                    <option value="${l.letter}"><c:out value="${l.letter}"/></option>
                </c:forEach>
            </select>

            <input type="submit" value="Submit">
        </form>
    </body>
</html>
