/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.studentsdirectory.servlet;

import hu.braininghub.bh11.studentsdirectory.repository.dto.StudentDto;
import hu.braininghub.bh11.studentsdirectory.service.StudentFilterService;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author andra
 */
@WebServlet(name = "FilterStudentServlet", urlPatterns = {"/studentfilter"})
public class FilterStudentServlet extends HttpServlet {

    @Inject
    StudentFilterService service;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<StudentDto> list = (List<StudentDto>) service.getStudentsByName((String) request.getParameter("name"));
        request.setAttribute("list", list);
        request.getRequestDispatcher("WEB-INF/StudentsPage.jsp").forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
