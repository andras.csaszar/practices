/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.studentsdirectory.repository.entity;


/**
 *
 * @author andra
 */

    public interface CrudRepository<Entity, ID> {

    Iterable<Entity> findByName(String name);

}

    
