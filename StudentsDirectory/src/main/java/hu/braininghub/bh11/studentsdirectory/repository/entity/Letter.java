/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.studentsdirectory.repository.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Table;

/**
 *
 * @author andra
 */
@Entity
@Table(name = "letter")
public enum Letter {
    A, B, C, D;

    @Enumerated
    @Column(name = "letter")
    private String letter;

}
