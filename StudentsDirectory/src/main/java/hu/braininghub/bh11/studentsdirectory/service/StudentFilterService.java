/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.studentsdirectory.service;

import hu.braininghub.bh11.studentsdirectory.repository.dto.StudentDto;
import hu.braininghub.bh11.studentsdirectory.repository.entity.Student;
import hu.braininghub.bh11.studentsdirectory.repository.entity.StudentDao;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.inject.Inject;
import lombok.Data;
import org.apache.commons.beanutils.BeanUtils;

/**
 *
 * @author andra
 */
@Data
@Singleton
@LocalBean
public class StudentFilterService {

    @Inject
    StudentDao dao;

    public Iterable<StudentDto> getStudentsByName(String name) {

        List<StudentDto> result = new ArrayList<>();
        for (Student st : dao.findByName(name)) {
            StudentDto dto = new StudentDto();
            try {

                BeanUtils.copyProperties(dto, st);
                result.add(dto);

            }  catch (InvocationTargetException ex) {
                Logger.getLogger(StudentFilterService.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(StudentFilterService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;

    }

}
