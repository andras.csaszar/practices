/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.studentsdirectory.repository.entity;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andra
 */
@Singleton
@LocalBean
public class StudentDao implements CrudRepository<Student, String> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Iterable<Student> findByName(String name) {

        return em.createQuery("SELECT s FROM Student s WHERE s.firstName LIKE :pattern")
                .setParameter("pattern", name + "%")
                .getResultList();
    }

}
