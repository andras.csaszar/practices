/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.consolemenusqlswing;

/**
 *
 * @author andra
 */
public interface View {
 void enableView();
 void setResults(String result);
 void setController(Controller controller);
}
