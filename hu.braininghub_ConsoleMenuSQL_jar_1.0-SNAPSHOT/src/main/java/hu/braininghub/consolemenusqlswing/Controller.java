/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.consolemenusqlswing;

/**
 *
 * @author andra
 */
public class Controller {

    private HrDao dao;
    private View v;

    public Controller(HrDao m, View v) {
        this.dao = m;
        this.v = v;

        m.setController(this);
        v.setController(this);

    }

    private void updateView(String result) {
        v.setResults(result);
    }

    public void handleButtons(String line) {
        String result = new String();
        switch (line) {
            case "1":
                result = dao.findEmployeeWthMaxSalary();
                break;
            case "2":
                result = dao.findMaxEmployeeDepartment();
                break;
            case "3":
                result = dao.findEmployeesEarnOverAverage();
                break;
            case "4":
                result = dao.findEmployeesHiredAfter19910101();
                break;
            case "5":
                result = dao.findJobTitlesWithClerk();
        }
        
        updateView(result);
        

    }
}
