/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.consolemenusqlswing;

import java.awt.BorderLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author andra
 */
public class SwingView extends JFrame implements View {

    private Controller controller;
    final List<JButton> buttons = new ArrayList<>();
    private JTextArea resultArea;

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void enableView() {
        construct();
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

    }
//todo

    @Override
    public void setResults(String newStringsToShow) {
        resultArea.setText(newStringsToShow);
    }

    private void construct() {

        buildCenter();
        buildSouth();

    }

    private void buildSouth() {
        resultArea = new JTextArea(10, 100);
        
        JPanel southPanel = new JPanel();
        JScrollPane scrollPane = new JScrollPane(southPanel);
        southPanel.add(resultArea);
        add(scrollPane, BorderLayout.SOUTH);
    }

    private void buildCenter() {
        final int countOfButton = 5;
        
        JPanel centerPanel = new JPanel();
        
        for (int i = 1; i < 6; i++) {
            JButton btn = new JButton(Integer.toString(i));
            buttons.add(btn);
            centerPanel.add(btn);
            btn.addActionListener(e -> controller.handleButtons(((JButton) e.getSource()).getText()));
        }
        
        add(centerPanel, BorderLayout.CENTER);

    }

}
