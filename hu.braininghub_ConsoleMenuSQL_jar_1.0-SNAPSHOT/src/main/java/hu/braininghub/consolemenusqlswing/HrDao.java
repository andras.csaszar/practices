package hu.braininghub.consolemenusqlswing;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author andra
 */
public class HrDao {

    private static final String URL = "jdbc:mysql://localhost:3306/hr?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String USER = "root";
    private static String PW = "Vilagbeke2020";
    private static StringBuilder sb = new StringBuilder();

    private Controller controller;
//  public HrDao(){
//        try {
//            Class.forName("com.mysql.jdbc.Driver");
//        } catch (ClassNotFoundException ex) {
//            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
//        }
//  
//  }

    public void setController(Controller controller) {
        this.controller = controller;

    }

    public String findEmployeeWthMaxSalary() {
        sb = sb.delete(0, sb.length());
        String sql = "SELECT last_name, first_name, max(salary) FROM employees";

        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                Statement stm = connection.createStatement();) {
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                sb.append(rs.getString("first_name") + " " + rs.getString("last_name") + "\n");

                //System.out.println(rs.getString("first_name") + " " + rs.getString("last_name"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return sb.toString();
        }
    }

    public String findMaxEmployeeDepartment() {
        sb = sb.delete(0, sb.length());
        String sql = "select * from departments where department_id = (\n"
                + "select department_id from employees group by department_id order by count(*) desc limit 1)";

        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                Statement stm = connection.createStatement();) {
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                sb.append(rs.getString("department_name") + "\n");

            }
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return sb.toString();
        }

    }

    public String findEmployeesEarnOverAverage() {
       sb = sb.delete(0, sb.length());
        String sql = "select first_name, last_name from employees where salary > (\n"
                + "select avg(salary) from employees as avgSalary)";

        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                Statement stm = connection.createStatement();) {
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                sb.append(rs.getString("first_name") + " " + rs.getString("last_name") + "\n");
            }
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return sb.toString();
        }

    }

    public String findEmployeesHiredAfter19910101() {
        sb = sb.delete(0, sb.length());
        String sql = "select first_name, last_name, hire_date from employees where hire_date > '1991.01.01'";

        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                Statement stm = connection.createStatement();) {
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                sb.append(rs.getString("first_name") + " " + rs.getString("last_name") + " " + rs.getString("hire_date") + "\n");
            }
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return sb.toString();
        }

    }

    public String findJobTitlesWithClerk() {
        sb = sb.delete(0, sb.length());
        String sql = "select distinct job_title from jobs where job_title LIKE \"%clerk\" order by job_title";

        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                Statement stm = connection.createStatement();) {
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                sb.append(rs.getString("job_title") + "\n");
            }
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return sb.toString();
        }

    }

}
