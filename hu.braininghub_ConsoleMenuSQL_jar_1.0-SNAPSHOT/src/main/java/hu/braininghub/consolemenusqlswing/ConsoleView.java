package hu.braininghub.consolemenusqlswing;

import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author andra
 */
public class ConsoleView implements View {

    private static final String STOP = "EXIT";
    private Controller controller;

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void enableView() {
        read();
    }

    public String read() {
        HrDao dao = new HrDao();
        String line;
        try (Scanner sc = new Scanner(System.in)) {

            do {
                line = sc.nextLine();
                controller.handleButtons(line);
            } while (!STOP.equalsIgnoreCase(line));
        }
        return line;
    }

    @Override
    public void setResults(String stringsToShow){
        System.out.println(stringsToShow);
    }
    
}
