/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h5f2arrayprint;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class H5F2ArrayPrint {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Scanner sc = new Scanner(System.in);
        int[] array = new int[100];
        int n = 1;
        boolean isValidInput = true;
        int countValidInput = 0;
        System.out.println("Adjon meg egész számokat (max. 100 db-ot, ha befejezte, üssön nullát");
        do {
            if (sc.hasNextInt()) {
                n = sc.nextInt();
                array[countValidInput] = n;
                if (n != 0) {
                    countValidInput++;
                }
            } else {
                isValidInput = false;
                sc.next();
                System.out.println("Nem egész számot adott meg!");
            }
        } while (n != 0);
// beviteli sorrendben
        System.out.println("Beviteli sorrendben:");
        for (int i = 0; i < countValidInput; i++) {
            System.out.println(array[i]);
        }
        int maxItem = array[0];
        int maxIndex = 0;
        int minItem = array[0];
        int minIndex = 0;
// max,min
        for (int i = 0; i < countValidInput; i++) {

            if (array[i] > maxItem) {
                maxItem = array[i];
                maxIndex = i;
            }
            if (array[i] < minItem) {
                minItem = array[i];
                minIndex = i;
            }

        }
        System.out.printf("Legnagyobb elem: %d, az indexe: %d.\n", maxItem, maxIndex);
        System.out.printf("Legkisebb elem: %d, az indexe: %d.\n", minItem, minIndex);
// fordított sorrendben
        System.out.println("Fordított sorrendben:");
        for (int i = countValidInput-1; i > -1; i--) {

            System.out.println(array[i]);

        }

    }

}
