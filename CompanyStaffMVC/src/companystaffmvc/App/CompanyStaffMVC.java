/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package companystaffmvc.App;


import companystaffmvc.controller.CompanyStaffController;
import companystaffmvc.controller.Controller;
import companystaffmvc.model.Boss;
import companystaffmvc.model.CompanyStaffModel;
import companystaffmvc.model.Employee;
import companystaffmvc.model.Model;
import companystaffmvc.model.Staff;
import companystaffmvc.model.StaffReporter;
import companystaffmvc.view.CompanyStaffConsoleView;
import companystaffmvc.view.CompanyStaffSwingView;
import companystaffmvc.view.View;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 *
 * @author andra
 */
public class CompanyStaffMVC {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Model m = new CompanyStaffModel();
        
        
        View v = new CompanyStaffSwingView();
        //View v = new CompanyStaffConsoleView();
        Controller c = new CompanyStaffController(v,m);
        
        v.start();
        
        //debugging
//        SortedSet<Staff> allSet = new TreeSet<>();
//        List<Staff> allList = new ArrayList<>();
//        
//        Boss boss = new Boss();
//        boss.setName("hu");
//        allSet.add(boss);
//        allList.add(boss);
//        
//        
//        Employee emp = new Employee(boss);
//        emp.setName("break");
//        allSet.add(emp);
//        allList.add(emp);
//        
////        System.out.println(allSet);
////        System.out.println(allList);
////        
//        System.out.println(boss.getEmployees());
    }
    
}
