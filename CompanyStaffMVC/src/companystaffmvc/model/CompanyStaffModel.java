/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package companystaffmvc.model;

import companystaffmvc.controller.CompanyStaffController;
import companystaffmvc.controller.Controller;
import java.util.ArrayList;
import companystaffmvc.model.Staff;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 *
 * @author andra
 */
public class CompanyStaffModel implements Model, Serializable {

    private Controller controller;
    private SortedSet<Staff> allStaff = new TreeSet<>();

    
    
    public boolean addToAllStaff(Staff staff) {
        return allStaff.add(staff);
        
    }

    public SortedSet<Staff> getAllStaff() {
        return allStaff;
    }

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.controller);
        hash = 29 * hash + Objects.hashCode(this.allStaff);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CompanyStaffModel other = (CompanyStaffModel) obj;
        if (!Objects.equals(this.controller, other.controller)) {
            return false;
        }
        if (!Objects.equals(this.allStaff, other.allStaff)) {
            return false;
        }
        return true;
    }

}
