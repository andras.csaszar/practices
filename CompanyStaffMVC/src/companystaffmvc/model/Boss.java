/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package companystaffmvc.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 *
 * @author andra
 */
public class Boss extends Staff implements Serializable {

    private SortedSet<Employee> employees;

    public Boss() {
        employees = new TreeSet<>();
    }

    public void setEmployees(SortedSet<Employee> employees) {
        copyEmployeeElements(employees);
    }

    private void copyEmployeeElements(SortedSet<Employee> employees) {

        for (Employee emp : employees) {
            Employee newEmp = new Employee(emp.getBoss());
            newEmp.setAge(emp.getAge());
            newEmp.setName(emp.getName());
            this.employees.add(newEmp);
        }
    }

    public boolean addEmployee(Employee employee) {
        return this.employees.add(employee);
    }

    public SortedSet<Employee> getEmployees() {
       // return Collections.unmodifiableSortedSet(employees);
        return employees;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.employees);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Boss other = (Boss) obj;
        if (!Objects.equals(this.employees, other.employees)) {
            return false;
        }
        return true;
    }



    @Override
    public String toString() {
        return "|Boss{"+"name: "+getName() + ", employees=" + employees + '}'+"|";
    }

//    @Override
//    public int compareTo(Staff o) {
//        if(o instanceof Boss){
//        return (this.getEmployees().size() - ((Boss) o).getEmployees().size());
//        } else{return 1;}
//
//    }

}
