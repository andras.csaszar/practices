/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package companystaffmvc.model;

import companystaffmvc.controller.Controller;
import companystaffmvc.model.Boss;
import companystaffmvc.model.Employee;
import companystaffmvc.model.Staff;
import java.util.Collections;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 *
 * @author andra
 */
public class StaffReporter {

    private Controller controller;
    private SortedSet<Staff> allStaff = new TreeSet<>();

    public StaffReporter(SortedSet<Staff> allStaff) {
        Staff newStaff = null;
        
        for (Staff s : allStaff) {
            if (s instanceof Employee) {
                newStaff = new Employee(((Employee) s).getBoss());
                
            }
            if (s instanceof Boss) {
                newStaff = new Boss();
                newStaff.setAge(s.getAge());
                newStaff.setName(s.getName());
                
                for (Employee emp : ((Boss) s).getEmployees()) {
                    Employee newEmp = new Employee(emp.getBoss());
                    ((Boss)newStaff).addEmployee(newEmp);
                }
            }
            this.allStaff.add(newStaff);
        }
//
//        this.allStaff = allStaff;

    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

    public String listAllStaff() {

        StringBuilder sb = new StringBuilder();

        allStaff.stream()
                .forEach((x) -> sb.append(x));
        return sb.toString();
    }

    public SortedSet<Staff> getBossesOnly(SortedSet<Staff> staff) {
        return staff.stream()
                .filter(s -> s instanceof Boss)
                .collect(Collectors.toCollection(TreeSet::new));
    }

    public Boss getLastBoss(SortedSet<Staff> staff) {
        if (staff == null) {
            Boss boss = new Boss();
            staff.add(boss);
        }
        return (Boss) ((TreeSet) getBossesOnly(staff)).last();
    }

    public Boss getFirstBoss(SortedSet<Staff> staff) {
        return (Boss) ((TreeSet) getBossesOnly(staff)).first();
    }

    public SortedSet<Staff> getEmployeesOnly(SortedSet<Staff> staff) {
        return staff.stream()
                .filter(s -> s instanceof Employee)
                .collect(Collectors.toCollection(TreeSet::new));

    }

    public SortedSet<Staff> getAllStaff() {
        return Collections.unmodifiableSortedSet(allStaff);
    }
}
