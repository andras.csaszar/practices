/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package companystaffmvc.model;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author andra
 */
public class Employee extends Staff implements Serializable {

    private Boss boss;

    public Employee(Boss boss) {
       this.boss = boss;
        setBoss(boss);
    }

    public boolean setBoss(Boss boss) {
        if (this.boss != boss) {
            if (this.boss != null) {
                this.boss.getEmployees().remove(this);
            }
        }
        boss.addEmployee(this);

        return boss.getEmployees().contains(this);
    }

    public Boss getBoss() {
        return boss;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.boss);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Employee other = (Employee) obj;
        if (!Objects.equals(this.boss, other.boss)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Employee{" + "name: " + getName() + " age: " + getAge() + ", boss=" + boss.getName() + '}';
    }
}
