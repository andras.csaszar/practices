/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package companystaffmvc.model;

import java.util.Objects;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 *
 * @author andra
 */
public abstract class Staff implements Comparable<Staff> {

//    private SortedSet<Staff> allStaff = new TreeSet<>();
    protected String name;
    protected int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
//
//    public SortedSet<Staff> getAllStaff() {
//        return allStaff;
//    }
//
//    public void addToAllStaff(Staff staff) {
//        this.allStaff.add(staff);
//    }

    @Override
    public int compareTo(Staff o) {
        if (o.getName() == null && this.getName() == null) {
            return 0;
        }
        if (o.getName() == null) {
            return 1;
        }
        if (this.getName() == null) {
            return -1;
        }
        return this.getName().compareTo(o.getName());

    }

    @Override
    public String toString() {
        return "Staff{" + "name=" + name + ", age=" + age + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.name);
        hash = 83 * hash + this.age;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Staff other = (Staff) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (this.age != other.age) {
            return false;
        }
        return true;
    }

}
