/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package companystaffmvc.view;

import companystaffmvc.controller.CompanyStaffController;
import companystaffmvc.controller.Controller;

/**
 *
 * @author andra
 */
public interface View {

    void setController(Controller csc);
    void start();
    void setText(String text);

    
}
