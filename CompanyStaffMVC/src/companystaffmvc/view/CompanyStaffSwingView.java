/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package companystaffmvc.view;

import companystaffmvc.controller.CompanyStaffController;
import companystaffmvc.controller.Controller;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author andra
 */
public class CompanyStaffSwingView extends JFrame implements View {

    private Controller controller;

    private final JButton jbEmp = new JButton("Emp");
    private final JButton jbBoss = new JButton("Boss");
    private final JButton jbList = new JButton("List");
    private final JButton jbSave = new JButton("Save");
    private final JTextField jtInputText = new JTextField("Your input", 50);
    private final JTextField jtOutputText = new JTextField("Result", 100);

    private JPanel buildNorth() {
        JPanel jpNorth = new JPanel();
        jpNorth.setLayout(new GridLayout(1, 1));

        if (controller instanceof CompanyStaffController) {
            String name = jtInputText.getText();
            jbBoss.addActionListener(l
                    -> ((CompanyStaffController) controller).handleAddBoss(jtInputText.getText()));
            jbEmp.addActionListener(l
                    -> ((CompanyStaffController) controller).handleAddEmp(jtInputText.getText()));

        } else {
            System.out.println("Controller mismatch.");
        }

        jpNorth.add(jbEmp);
        jpNorth.add(jbBoss);
        return jpNorth;
    }

    private JPanel buildCenter() {
        JPanel jpCenter = new JPanel();
        jpCenter.setLayout(new FlowLayout());
        jbSave.addActionListener(l
                -> ((CompanyStaffController) controller).handleSave()
        );
        jbList.addActionListener(l
                -> ((CompanyStaffController) controller).handleList()
        );

        jpCenter.add(jbSave);
        jpCenter.add(jbList);
        return jpCenter;

    }

    private JPanel buildSouth() {
        JPanel jpSouth = new JPanel();
        jpSouth.setLayout(new GridLayout(1, 1));
        jpSouth.add(jtInputText);
        jpSouth.add(jtOutputText);

        jtOutputText.setEditable(false);

        return jpSouth;
    }

    public void setOutputText(String expression) {
        jtOutputText.setText(expression);
    }

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    private void showWindow() {

        add(buildNorth(), BorderLayout.NORTH);
        add(buildCenter(), BorderLayout.CENTER);
        add(buildSouth(), BorderLayout.SOUTH);

        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

    }

    @Override
    public void start() {
        showWindow();
    }

    @Override
    public void setText(String text) {
        jtOutputText.setText(text);
    }

}
