/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package companystaffmvc.view;

/**
 *
 * @author andra
 */
public enum Commands {
    
    BOSS('B'){},
    EMPLOYEE('E'){},
    SAVE('S'){},
    LIST('L'){};
    
    private char character;

    private Commands(char character) {
        this.character = character;
    }

    public char getCharacter() {
        return character;
    }
    
}
