/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package companystaffmvc.view;

import companystaffmvc.controller.CompanyStaffController;
import companystaffmvc.controller.Controller;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andra
 */
public class CompanyStaffConsoleView implements View {
    // private static final BufferedReader READER = new BufferedReader(new InputStreamReader(System.in));

    private Controller controller;
    private static final String STOP = "exit";
    private static final String INSTRUCTION ="Write B/E space and name to add Boss/Employee, S/L to Save or List";

    private void startReading() {
        String command = null;

        try (BufferedReader READER = new BufferedReader(new InputStreamReader(System.in));) {
            do{ {
                System.out.println(INSTRUCTION);
                while (!STOP.equals(command)) {
                    command = READER.readLine().toUpperCase();
                    handleCommand(command);
                }

            }}while (READER.ready());
        } catch (IOException ex) {
            Logger.getLogger(CompanyStaffConsoleView.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            
        }

    }

    private void handleCommand(String command) {
        
        
        char c = command.charAt(0);
        String name = command.substring(1, command.length()).trim();
        
        switch (c) {
            
            case 'B': System.out.println(((CompanyStaffController)controller).handleAddBoss(name)); break;
            case 'E': System.out.println(((CompanyStaffController)controller).handleAddEmp(name)); break;
            case 'S': ((CompanyStaffController)controller).handleSave(); break;
            case 'L': ((CompanyStaffController)controller).handleList(); break;
                
        }
    }

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void start() {

        startReading();

    }

    @Override
    public void setText(String text) {
        System.out.println(text);
    }

}
