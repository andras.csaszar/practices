/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package companystaffmvc.controller;

import companystaffmvc.model.Boss;
import companystaffmvc.model.CompanyStaffModel;
import companystaffmvc.model.Employee;
import companystaffmvc.model.Model;
import companystaffmvc.model.Staff;
import companystaffmvc.model.StaffReporter;
import companystaffmvc.view.View;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Collections;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andra
 */
public class CompanyStaffController implements Controller {

    private View view;
    private CompanyStaffModel model;
    private final String FILENAME = "CompanyStaffMVC.txt";
    private  SortedSet<Staff> fullStaff = new TreeSet<>();

    @Override

    public void doOperation() {
    }

    public SortedSet<Staff> getFullStaff() {
        return fullStaff;
    }

    public CompanyStaffController(View view, Model model) {
        this.view = view;
        this.model = (CompanyStaffModel) model;

        view.setController(this);
        model.setController(this);

    }

    public boolean addStaff(Staff staff) {
        return this.fullStaff.add(staff);

    }

    public SortedSet<Staff> generateStaffReport() {
        return Collections.unmodifiableSortedSet(getFullStaff());
    }

    public boolean handleAddBoss(String name) {
        Boss boss = new Boss();
        boss.setName(name);
        return addStaff(boss);
//        return model.getAllStaff().contains(boss);
    }

    public boolean isAnyBossInStaff(SortedSet<Staff> staff) {
        return (staff.stream().anyMatch(s -> s instanceof Boss));
    }

    public boolean handleAddEmp(String name) {

        Boss boss = null;
        if (fullStaff != null || !isAnyBossInStaff(fullStaff)) {
            handleAddBoss(name + "_boss");
        }

            boss = new StaffReporter(fullStaff).getLastBoss(fullStaff);
        
        Employee emp = new Employee(boss);
        emp.setName(name);
      return  addStaff(emp);

    }
//        return allStaff.stream().map(s -> s.getName()).anyMatch(n -> n.equals(name));

    public String handleSave() {

        String message = null;
        try (FileOutputStream fos = new FileOutputStream(FILENAME);
                ObjectOutputStream ou = new ObjectOutputStream(fos);) {

            ou.writeObject(generateStaffReport());
            message = "Staff list saved.";

        } catch (FileNotFoundException ex) {
            Logger.getLogger(CompanyStaffController.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CompanyStaffController.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (message == null) {
                message = "Not saved";
            }
        }
        notifyView(message);
        return message;

    }

    public void notifyView(String text) {
        view.setText(text);
    }

    public String handleList() {
        StaffReporter sr = new StaffReporter(generateStaffReport());

        String str = sr.listAllStaff();

        notifyView(sr.listAllStaff());

        return sr.listAllStaff();
    }

}
