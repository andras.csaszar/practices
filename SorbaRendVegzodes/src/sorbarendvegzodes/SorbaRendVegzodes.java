/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sorbarendvegzodes;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class SorbaRendVegzodes {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n1 = -1;
        boolean isValidInput = false;

        do {
            System.out.println("Adjon meg egy egész számot!");

            if (sc.hasNextInt()) {
                n1 = sc.nextInt();
                isValidInput = true;
            } else {
                sc.next();
            }
        } while (!isValidInput);

        int n2 = -1;
        isValidInput = false;
        do {
            System.out.println("Adjon meg egy egész számot!");

            if (sc.hasNextInt()) {
                n2 = sc.nextInt();
                isValidInput = true;
            } else {
                sc.next();
            }
        } while (!isValidInput);
        System.out.println("---------- 7-re végződő számok ------------");
        int from = n1 > n2 ? n2 : n1;
        int to = n1 > n2 ? n1 : n2;
        
        from = Math.min(n1,n2);
        to = Math.max(n1,n2);
        
        int i = from;
        while (i <= to) {
            if (i % 10 == 7) {
                System.out.println(i);
            }
            i++;
        }

    }

}
