/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kilencprogfel22;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class KilencProgFel22 {

    static boolean isPrime(int number) {

        if (number == 1 || number == 0) {
            return false;
        } else {

            for (int i = 2; i < Math.sqrt(number); i++) {
                if (number % i == 0) {
                    return false;
                }

            }
            return true;
        }

    }

    static int[] getPrimesTillNumber(int number) {
        int size = 0;

        for (int i = 2; i <= number; i++) {
            if (isPrime(i)) {
                size++;
            }
        }

        int[] arrayOfPrimes = new int[size];

        int countStepper = 0;

        for (int i = 0; i <= number; i++) {
            if (isPrime(i)) {
                arrayOfPrimes[countStepper++] = i;
            }
        }
        return arrayOfPrimes;
    }

    static int[] getFrequencyOfPrimes(int number, int[] arr) {
        int[] result = new int[arr.length];

//        int subCount = 0;
        for (int i = 0; i < arr.length; i++) {

            if (number % arr[i] == 0) {
                number = number / arr[i];
                result[i]++;
                i--;
            }

        }
        return result;
    }

    static int getNumber() {

        Scanner sc = new Scanner(System.in);

        int result = -1;

        while (result < 0) {
            if (sc.hasNextInt()) {

                result = sc.nextInt();
            } else {
                sc.next();
            }
        }
        return result;
    }

    static void printArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != 0) {
                System.out.println(i+" "+arr[i]);
            }
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        int numb = getNumber();
        int[] primes = getPrimesTillNumber(numb);
        int[] freqOfPrimes = getFrequencyOfPrimes(numb, primes);

        printArray(primes);
        System.out.println();
        printArray(freqOfPrimes);

    }

}
