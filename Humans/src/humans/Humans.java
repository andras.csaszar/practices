/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package humans;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author andra
 */
public class Humans {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        MyPair<String, Worker> p1 = new MyPair<>("Alma", new Worker("10", "W"));

        MyPair<String, Worker> p2 = new MyPair<>("Alma", new Worker("13", "D"));

        System.out.println(p1.getClass());
        System.out.println(p2.getClass());

        List<Number> l1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));
        List l2 = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));
        List<Integer> l3 = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));
        List<Object> l4 = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));

        m1(l1);
        m2(l4);
        m3(l2);
    
    }

    public static void m1(List l) {
        // l1, l2, l3, l4  --eezeket tudja bevenni
    }

    public static void m2(List<Object> l)
    {
        // l2, l4    --ezeket tudja bevenni
    }
    
    public static void m3(List<Number> l){
        // l1, l2
    }
    
    public static void m4(List<?> l){
        // l1, l2, l3, l4
    }
    
    public static void m5(List<? extends Number> l){
        //l1, l2, l3
       //l.get(x);
        
    }
    public static void add(List<? super Number> l, Integer a){
       l.add(a);
        
    }
    
    
}
