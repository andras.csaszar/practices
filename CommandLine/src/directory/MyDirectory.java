/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package directory;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.util.logging.Level;
import java.util.logging.Logger;
import parser.Commands;

import parser.Commands.*;

/**
 *
 * @author andra
 */
public class MyDirectory {

    private File file = new File(System.getProperty("user.dir"));

    public void pwd() {
        try {
            System.out.println(file.getCanonicalPath());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void ls() {
        File[] files = file.listFiles();

        for (File f : files) {
            System.out.print(f.getName());
            System.out.println(f.isFile() ? " F" + f.length() : " D");

        }
    }

    public void cd(String command) {
        if (Commands.PARENT_DIRECTORY.equals(command)) {
            cdParent();
        } else {
            cdDirectory(command);
        }
    }

    public void mv(String from, String to) {
    
        File originalNamedFile = new File (file, from);
        File newlyNamedFile = new File (file, to);
        originalNamedFile.renameTo(newlyNamedFile);
    
    }

    private void cdParent() {
        file = file.getParentFile();
    }

    private void cdDirectory(String directory) {
        File to = new File(file, directory);

        if (to.exists() && to.isDirectory()) {
            file = to;
        }

    }

    public void touchFile(String fileName) {
        File newFile = new File(file, fileName);
        try {
            if(newFile.exists()&&newFile.isFile()){
            throw new FileAlreadyExistsException("File already exists");
            }
            newFile.createNewFile();
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

}
