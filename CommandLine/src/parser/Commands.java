/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

/**
 *
 * @author andra
 */
public abstract class Commands {
    
    public static final String CHANGE_DIRECTORY = "cd";
    public static final String LIST = "ls";
    public static final String PARENT_DIRECTORY = "..";
    public static final String RENAME = "mv";
    public static final String PWD = "pwd";
    public static final String TOUCH = "touch";
    
    private Commands(){}
    
}
