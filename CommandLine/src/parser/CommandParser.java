/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

import directory.MyDirectory;
import static parser.Commands.*;

/**
 *
 * @author andra
 */
public class CommandParser {

    private static final String DELIMITER = " ";
    private MyDirectory directory = new MyDirectory();

    public void parse(String line) {
        String[] commands = line.split(DELIMITER);

        if (PWD.equals(commands[0])) {
            directory.pwd();
        } else if (commands.length == 2 && CHANGE_DIRECTORY.equals(commands[0])) {
            directory.cd(commands[1]);
        } else if (LIST.equals(commands[0])) {
            directory.ls();
        } else if (commands.length == 3 && RENAME.equals(commands[0])){
            directory.mv(commands[1], commands[2]);
        } else if (commands.length == 2 && TOUCH.equals(commands[0])){
            directory.touchFile(commands[1]);
                }

    }
}
