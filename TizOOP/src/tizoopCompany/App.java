/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tizoopCompany;

/**
 *
 * @author andra
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        
        Company company = new Company();
        
//        Employee[] employees = new Employee[2];

      // Company.raiseIndicator = 1.2;

        Employee employee = new Employee("Bela", "Balazs", "Kiss", 300_000.0);
        company.addEmployee(employee);
        
//        System.out.println(employee.name.getFullName());
//        System.out.println(employee.salary + " Ft");

//        employee.increaseSalary();
//        System.out.println(employee.salary + " Ft");

        Employee employee2 = new Employee("Adam", "Nagy", 400_000.0);
        company.addEmployee(employee2);
//        System.out.println(employee2.name.getFullName());
//        System.out.println(employee2.salary + " Ft");
//        employee2.increaseSalary();
//        System.out.println(employee2.salary + " Ft");

        Name name = new Name("Eszter", "Móricz");
        Employee employee3 = new Employee(name, 500_000.0);
        company.addEmployee(employee3);
//        System.out.println(employee3.name.getFullName());
        
//        //vagy 
        
        Employee employee4 = new Employee(new Name ("Lajos","Lajvér"),150_000.0);
        company.addEmployee(employee4);
//        System.out.println(employee4.name.getFullName());
        
//        Employee employee5 = new Employee();
//        System.out.println(employee5.getFullName());
        
        
        company.printEmployeesWithSalaries();
        company.increaseSalaries();
        company.printEmployeesWithSalaries();
      
        /*
        for (int i = 0; i < employees.length; i++) {
            employees[i].increaseSalary();
            System.out.println(employees[i].name.getFullName() + " " + employees[i].salary + " Ft");
        }

        for (Employee emp : employees) {
            emp.increaseSalary();
            System.out.println(emp.name.getFullName() + " " + emp.salary + " Ft");
        }
        */

        
        
        Employee empOne = new Manager(new Name("Andras","Cs"), 2000, "Merci");
        Employee progOne = new Programmer(new Name("Andras","Cs"), 20000);
        
        
        company.addEmployee(empOne);
        company.addEmployee(progOne);
        
        company.printEmployeesWithSalaries();
        
    }

}
