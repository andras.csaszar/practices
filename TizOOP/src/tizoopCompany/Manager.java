/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tizoopCompany;

/**
 *
 * @author andra
 */
public class Manager extends Employee {
    //mivel az Employee-nak nincs üres constructor-a, ezért kell írni itt egyet, amelyik meghívja az employee-t

    String car;
    
    public Manager(Name name, double salary, String car) {
        super(name, salary);
        this.car = car;
    }   
    
    @Override
        public void setSalary(double salary) {

        if (salary < 0 || salary > 100_000_000) {
            this.salary =0;
            System.out.println("Wrong salary, salary is set to 0");

        } else {
            this.salary = salary;
        }

    }

    @Override
    public String toString() {
        return "This is a Manager with this properties: Manager{" + "name=" + name + ", salary=" + salary + " Car="+car+'}';
    }
    
}
