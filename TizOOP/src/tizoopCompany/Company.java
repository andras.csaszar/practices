/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tizoopCompany;

/**
 *
 * @author andra
 */
public class Company {

    private Employee[] employees;
    private int numberOfEmployees;
    public static double raiseIndicator = 1.1;

    public Company() {
        employees = new Employee[10];
    }

    private void increaseMaximumNumberOFEmployees() {
        Employee[] employees2 = new Employee[numberOfEmployees * 2];

        for (int i = 0; i < employees.length; i++) {
            employees2[i] = employees[i];
        }
        employees = employees2;
    }
    public void addEmployee(Employee emp) {
        // increaseMaximumNumberOfEmployees

        if (numberOfEmployees == employees.length) {
            increaseMaximumNumberOFEmployees();
        }
        employees[numberOfEmployees] = emp;
        numberOfEmployees++;

    }

    public void increaseSalaries() {

        for (int i = 0; i < numberOfEmployees; i++) {
            employees[i].increaseSalary(raiseIndicator);
        }
    }


    public void printEmployeesWithSalaries() {
        for (int i = 0; i < numberOfEmployees; i++) {
            System.out.println(employees[i].getName().getFullName() + "'s salary: " + employees[i].getSalary());
        }

    }
}
