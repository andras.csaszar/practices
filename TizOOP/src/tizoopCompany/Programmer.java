/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tizoopCompany;

/**
 *
 * @author andra
 */
public class Programmer extends Employee {

    private boolean homeOffice;
    
    public Programmer(Name name, double salary) {
        super(name, salary);
        this.homeOffice = true;
    }

    public boolean isHomeOffice() {
        return homeOffice;
    }
    


    @Override
    public double getSalary() {
        return Double.MAX_VALUE; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
          return "This is a programmer with this properties: Manager{" + "name=" + name + ", salary=" + salary + ", homeOffice=" + homeOffice+ '}';
    }
    
    
}
