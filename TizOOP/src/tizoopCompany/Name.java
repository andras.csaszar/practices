/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tizoopCompany;

/**
 *
 * @author andra
 */
public class Name {

    private String firstName;
    private String lastName;
    private String middleName;

    public Name(String firstName, String lastName) {
        this.firstName = firstName;
//        this.setFirstName(firstName);
        this.lastName = lastName;
    }

    public Name(String firstName, String middleName, String lastName) {
        this(firstName, lastName);
        this.middleName = middleName;
    }

    public String getFullName() {

        if (this.middleName == null) {
            return firstName + " " + lastName;
        }
        return firstName + " " + middleName + " " + lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;

    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
}
