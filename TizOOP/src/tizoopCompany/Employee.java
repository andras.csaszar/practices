/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tizoopCompany;

/**
 *
 * @author andra
 */
public class Employee {

    /*private*/ protected Name name;
    /*private*/ protected double salary;
    // static double raiseIndicator = 1.1;

//    Employee(){}
    public Employee(String firstName, String lastName, double salary) {

        name = new Name(firstName, lastName);
        this.setSalary(salary);
    }

    public Employee(String firstName, String middleName, String lastName, double salary) {

        this(firstName, lastName, salary);
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.salary = salary;
        name.setMiddleName(middleName);

    }

    public Employee(Name name, double salary) {
        this.name = name;
        this.salary = salary;
    }

    public String getFullName() {
        return name.getFullName();
    }

    public void increaseSalary(double raiseIndicator) {
        salary = salary * raiseIndicator;

    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {

        if (salary < 0 || salary > 10_000_000) {
            this.salary = 0;
            System.out.println("Wrong salary, salary is set to 0");

        } else {
            this.salary = salary;
        }

    }

    @Override
    public String toString() {
        return "Employee{" + "name=" + name + ", salary=" + salary + '}';
    }

}
