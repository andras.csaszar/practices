/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kilencprogfel20;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class KilencProgFel20 {

    /**
     * @param args the command line arguments
     */
    static char[] getLowerCases(char[] character) {

        int countLowerCases = 0;

        for (int i = 0; i < character.length; i++) {

            if (isLower(character[i])) {
                countLowerCases++;
            }

        }

        char[] result = new char[countLowerCases];

        int stepper = 0;
        for (int i = 0; i < character.length; i++) {

            if (isLower(character[i])) {
                result[stepper++] = character[i];

            }

        }
        return result;  
    }

    static boolean isLower(char charct) {
        return charct >= 97 && charct < 123;

    }

    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        int length = 15;
        char[] characters = new char[length];
        for (int i = 0; i < characters.length; i++) {
            if (sc.hasNext()) {
                characters[i] = sc.next().charAt(0);
            }
        }

        char[] test = getLowerCases(characters);

        for (int i = 0; i < test.length; i++) {
            System.out.println(test[i]);
        }

    }

}
