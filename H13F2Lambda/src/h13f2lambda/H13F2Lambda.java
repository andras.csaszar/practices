/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h13f2lambda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author andra
 */
public class H13F2Lambda {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        String[] strings = new String[10];
        strings[0] = "mikulas";
        strings[1] = "karacsony";
        strings[2] = "hoember";
        strings[3] = "alma";
        strings[4] = "korte";
        strings[5] = "mogyoro";
        strings[6] = "virgacs";
        strings[7] = "ajandek";
        strings[8] = "ho";
        strings[9] = "szan";

        List<String> stringList = new ArrayList<>();

        for (String str : strings) {
            stringList.add(str);
        }

        System.out.println(stringList);
        /*
         Collections.sort(stringList, new Comparator<String>() {
         public int compare(String s1, String s2) {
         return s1.length() - s2.length();
         }
         });*/

        Collections.sort(stringList, (String x, String y) -> x.length() - y.length());

        System.out.println(stringList);

        Collections.sort(stringList, (String s1, String s2) -> s2.length() - s1.length());
        System.out.println(stringList);

        Collections.sort(stringList, (String s1, String s2) -> s1.charAt(0) - s2.charAt(0));
        System.out.println(stringList);

        Collections.sort(stringList, (String s1, String s2) -> (s2.contains("e") ? 1 : 0)-(s1.contains("e") ? 1 : 0) 
        );
        System.out.println(stringList);

    }

}
