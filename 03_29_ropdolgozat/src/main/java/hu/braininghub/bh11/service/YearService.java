/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import lombok.Data;

/**
 *
 * @author andra
 */
@Data
@Singleton
@LocalBean
public class YearService {

    @PostConstruct
    private void newService(){
        System.out.println("This service has just been constructed.");
    }
    
    @PreDestroy
    private void gettingDone(){
        System.out.println("This service is done, is being destroyed.");
    }
    
    
    public String getLastTwoDigits(String number) {
        return number.substring(number.length() - 2);
    }

}
