/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.servlet;

import hu.braininghub.bh11.service.YearService;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author andra
 */
@WebServlet(name = "BirthDateServlet", urlPatterns = {"/BirthDateServlet"})
public class BirthDateServlet extends HttpServlet {

    @Inject
    YearService yearService;

    @Override
    public void init() {
        System.out.println("Servlet initialized.");
    }

    @Override
    public void destroy() {
        System.out.println("Servlet being destroyed.");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String birthYear = request.getParameter("birthYear");
        String cutBirthYear = yearService.getLastTwoDigits(birthYear);
        request.setAttribute("response", cutBirthYear);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
