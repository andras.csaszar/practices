/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negynumbersinrndorder;

/**
 *
 * @author andra
 */
public class NegyNumbersInRndOrder {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
//ötöslottó generátor
        int[] numbers = new int[10];

        for (int i = 0; i < 10; i++) {
            numbers[i] = (int) (Math.random() * 10 + 1);
        }

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
        //Ám a fentivel sajnos előfordulhat, hogy többször is van ugyanaz a szám
        System.out.println("---- biztosítva, hogy mindegyik különböző ----");
        int[] numbersDistinct = new int[10];
        int countOfNumbers = 0;
        int counterOfDuplicates = 0;
        while (countOfNumbers < 10) {
        boolean containsItem = false;
            int rndNumber = (int) (Math.random() * 10) + 1;

            for (int i = 0; i < countOfNumbers; i++) {
                if (numbersDistinct[i] == rndNumber) {
                    containsItem = true;
                    counterOfDuplicates++;
                }
            }
            if (!containsItem) {
                numbersDistinct[countOfNumbers] = rndNumber;
                countOfNumbers++;
            }
        }

        for (int i = 0; i < numbersDistinct.length; i++) {
            System.out.println(numbersDistinct[i]);
        }
        System.out.println("Duplikációk: " + counterOfDuplicates);
        
        
    }
    
}
