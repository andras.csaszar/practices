/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package szamosztoi;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class SzamOsztoi {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int n = -1;

        do {
            System.out.println("Adjon meg egy számot! Megmondom az osztóit");
            if (sc.hasNextInt()) {

                n = sc.nextInt();
            } else {
                sc.next();
            }

        } while (n < 0);
        //pozitív osztók számának kiírása
        int i = 1;
        while (i <= n) {
            if (n % i == 0) {
                System.out.println("Osztó:" + i);
                /* System.out.println(n + "/" + i +"= "+ (n/i*1.0));*/
                //System.out.printf("%d / %d = %d", n, i, (n/i*1.0));
            }
            i++;
        }
         
        int countOfDivider=0;
         int j = 1;
        while (j <= n) {
            if (n % j == 0) {
                countOfDivider++;
            }
            j++;
        }
        if(countOfDivider == 2){System.out.println("Prím");}
        
    }

}
