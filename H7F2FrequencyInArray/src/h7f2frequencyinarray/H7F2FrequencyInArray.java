/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h7f2frequencyinarray;

/**
 *
 * @author andra
 */
public class H7F2FrequencyInArray {

    static int getRandomNumber(int to) {

        return (int) (Math.random() * to);

    }

    static int[] fillInArray(int[] array) {

        for (int i = 0; i < array.length; i++) {
            array[i] = getRandomNumber(100);
        }
        return array;
    }

    static int getMaxValueOfArray(int array[]) {

        int max = array[0];

        for (int i = 0; i < array.length; i++) {
            if (max < array[i]) {
                max = array[i];
            }

        }

        return max;
        

    }
     static int getMaxValueIndexOfArray(int array[]) {

        int max = array[0];
        int maxIndex =0;
        for (int i = 0; i < array.length; i++) {
            if (max < array[i]) {
                max = array[i];
                maxIndex = i;
            }

        }

        return maxIndex;
        

    }

    static int getMostFrequentIntInArray(int[] array, int auxTableSize) {

        int [] auxTable = new int [auxTableSize+1];
        
        for (int i = 0; i < array.length; i++) {
            auxTable[(array[i])]++;
        }
        
        return getMaxValueIndexOfArray(auxTable);
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //// TODO code application logic here

        /*
         leggyakoribb szám: 0-100 közötti véletlen számokkal töltsünk fel egy 300 elemű tömböt,
         majd keressük meg h melyik szám került bele a leggyakrabban
         */
        int size = 300;
        int[] array = new int[size];
        int mostFrequent=0;
        
        fillInArray(array);
        
        mostFrequent = getMostFrequentIntInArray(array, getMaxValueOfArray(array));
               
             System.out.println(mostFrequent);  

    }

}
