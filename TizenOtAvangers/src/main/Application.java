/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import hero.AbstractHero;
import hero.BornOnEarth;
import hero.IdentityCard;
import hero.NotBornOnEarth;
import stone.AbstractStone;
import stone.Soul;
import stone.Time;

public class Application {

    static {
        System.out.println("HHHHHHEEEEELLLLLLOOOOO");
    }

    public static void main(String[] args) {

        // AbstractStone soul = new Soul();
        Soul soul = new Soul();
        Time time = new Time();
        AbstractHero[] heroes = new AbstractHero[5];

        heroes[0] = new BornOnEarth("A1", 10, soul, new IdentityCard());
        heroes[1] = new BornOnEarth("A2", 20, soul, new IdentityCard());
        heroes[2] = new BornOnEarth("A3", 10, time, new IdentityCard());
        heroes[3] = new NotBornOnEarth("A4", 55, soul);
        heroes[4] = new NotBornOnEarth("A5", 83, time);

        for (int i = 0; i < heroes.length; i++) {
            System.out.println(heroes[i]);
        }



    }

}
