/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

import java.util.Arrays;
import java.util.Optional;

/**
 *
 * @author andra
 */
public enum HappinessFactor {

    SAD(0),
    NEUTRAL(1),
    HAPPY(2);

    private final int order;

    private HappinessFactor(int order) {
        this.order = order;
    }

    public static HappinessFactor nextLevel(HappinessFactor factor) {

        Optional<HappinessFactor> nextLevel = Arrays.stream(HappinessFactor.values())
                .sorted((x, y) -> y.order - x.order)
                .filter(f -> f.order < factor.order + 1)
                .findAny();
        return nextLevel.orElse(HAPPY);

    }
}
