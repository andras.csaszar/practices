/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

import animal.AbstractAnimal;

/**
 *
 * @author andra
 */
public interface Visitor {

    void act(AbstractAnimal animal);

    void feed(AbstractAnimal animal);
}
