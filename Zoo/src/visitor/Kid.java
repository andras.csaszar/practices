/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

import animal.AbstractAnimal;
import animal.HappinessFactor;
import zoo.exceptions.ZooKidCannotFeedAnimalsException;

/**
 *
 * @author andra
 */
public class Kid implements Visitor {
    
    @Override
    public void act(AbstractAnimal animal) {
        animal.setVisitors(animal.getVisitors() + 2);
        animal.setHunger(animal.getHunger() - 1);
        animal.setHappiness(HappinessFactor.nextLevel(animal.getHappiness()));
    }
    
    
    @Override
    public void feed(AbstractAnimal animal)  {
        throw new ZooKidCannotFeedAnimalsException("Kids cannot feed animals in this zoo."); }
    
}
