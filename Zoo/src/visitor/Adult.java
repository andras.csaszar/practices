/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

import animal.AbstractAnimal;
import zoo.shop.Food;

/**
 *
 * @author andra
 */
public class Adult implements Visitor{

    @Override
    public void act(AbstractAnimal animal) {
        animal.setVisitors(animal.getVisitors()+1);

    }

    @Override
    public void feed(AbstractAnimal animal) {
        animal.eat(new Food());
    }
    
}
