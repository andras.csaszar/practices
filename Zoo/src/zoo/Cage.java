/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zoo;

import animal.AbstractAnimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import visitor.Visitor;
import visitor.Worker;

/**
 *
 * @author andra
 */
public class Cage {

    private static final int CROWDED_PERCENTAGE = 80;
    private final int capacity = (int) (Math.random() * 500 + 500); // sorsolást privát metódusba, az 500-at változóba kiszervezni
    private Worker worker;
    private final List<AbstractAnimal> animals = new ArrayList<>();

    public boolean add(AbstractAnimal animal) {
        if (animal.getEntryCage() == null) {
            if (isEnoughSpace(animal.getRequiredPlace())) {
                animal.setEntryCage(LocalDateTime.now());
                return animals.add(animal);
            }
        }
        return false;
    }

    public boolean remove(AbstractAnimal animal) {
        if (animals.contains(animal)) {
            animal.setEntryCage(null);
            return animals.remove(animal);
        }
        return false;
    }

    public void visit(Visitor visitor) {
        animals.stream()
                .forEach(animal -> animal.visit(visitor));
    }

    public void feed(Visitor visitor) {
        animals.stream()
                .forEach(animal -> animal.eat(visitor));
    }

    public void enter(Worker worker) {
        visit(worker);
        System.out.println("Worker is in the cage: " + worker);
    }
    /*private*/

    public boolean isEnoughSpace(int newValue) {
        int sum = animals.stream()
                .map(AbstractAnimal::getRequiredPlace)
                .reduce(0, (a, b) -> a + b);

        return sum + newValue < capacity * CROWDED_PERCENTAGE / 100D;
    }

    public Worker getWorker() {
        return worker;
    }

    public void setWorker(Worker worker) {
        this.worker = worker;
    }

    public int getCapacity() {
        return capacity;
    }

    public List<AbstractAnimal> getAnimals() {
        return animals;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + this.capacity;
        hash = 17 * hash + Objects.hashCode(this.worker);
        hash = 17 * hash + Objects.hashCode(this.animals);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cage other = (Cage) obj;
        if (this.capacity != other.capacity) {
            return false;
        }
        if (!Objects.equals(this.worker, other.worker)) {
            return false;
        }
        if (!Objects.equals(this.animals, other.animals)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Cage{" + "capacity=" + capacity + ", worker=" + worker + ", animals=" + animals + '}';
    }

}
