/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zoo.exceptions;

/**
 *
 * @author andra
 */
public class ZooExceptions extends RuntimeException {

    public ZooExceptions(String message) {
        super(message);
    }
    
}
