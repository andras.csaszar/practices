/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zoo;

import animal.AbstractAnimal;
import java.util.ArrayList;
import java.util.List;
import visitor.Visitor;

/**
 *
 * @author andra
 */
public class Zoo {

    private final List<Cage> cages;
    private final List<Visitor> visitors;
    private final Report report;

    public Zoo() {
        cages = new ArrayList<>();
        visitors = new ArrayList<>();
        report = new Report(this);
    }

    public List<Cage> getCages() {
        return cages;
    }

    public List<Visitor> getVisitors() {
        return visitors;
    }

    public Report getReport() {
        return report;
    }

    public boolean addCage(Cage cage) {
        return cages.add(cage);
    }

    public boolean removeCage(Cage cage) {
        return cages.remove(cage);
    }

    public boolean addVisitor(Visitor visitor) {
        return visitors.add(visitor);
    }

    public boolean removeVisitor(Visitor visitor) {
       return visitors.remove(this);
    }

    public boolean addAnimal(AbstractAnimal animal) {
        return report.findFirstCageWithSpace(animal).add(animal);

    }

    public boolean removeAnimal(AbstractAnimal animal) {
        return report.findCageOfAnimal(animal).remove(animal);
    }

}
