/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zoo;

import animal.AbstractAnimal;
import animal.HappinessFactor;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author andra
 */
public class Report {

    private Zoo zoo;

    public Report(Zoo zoo) {
        this.zoo = zoo;
    }

    public Zoo getZoo() {
        return zoo;
    }

    public Cage findFirstCageWithSpace(AbstractAnimal animal) {
        return zoo.getCages().stream()
                .filter(c -> c.isEnoughSpace(animal.getRequiredPlace())).findFirst().orElseGet(Cage::new);

    }

    public Cage findCageOfAnimal(AbstractAnimal animal) {
        return zoo.getCages().stream().filter(c -> c.getAnimals().contains(animal)).findFirst().orElseGet(null);

    }

    public boolean isAnimalInCageAlready(AbstractAnimal animal) {
        return animal.getEntryCage() != null;
    }

    public List<AbstractAnimal> getUnhappyAndHungryAnimals() {
        return zoo.getCages().stream().flatMap(c -> c.getAnimals().stream())
                .filter(a -> HappinessFactor.SAD.equals(a.getHappiness()) && a.getHunger() < 5)
                .collect(Collectors.toList());

    }
    //gyereklátogatók számának kimutatása nem lehetséges, nincs eltárolva sehol
    public void getNumberOfKidVisitorsPerCage() {
        
     Map<Cage, Integer> map =           zoo.getCages().stream().collect(Collectors.toMap(
                                k -> k,
                                v -> v.getAnimals().stream().mapToInt(a -> a.getVisitors()).sum(),
                                (v1, v2) -> v1 + v2,
                                HashMap::new
                        )
                );
     
        System.out.println(map);
    }
}
