/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Controller;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author andra
 */
public class Model {

    private Controller controller;

    private String filePath = "countries.txt";
    private List<String> originalCountries;
    private List<String> foundCountries;

    public Model() {
        this.originalCountries = new ArrayList<>();
        this.foundCountries = new ArrayList<>();
        readFromFile();
        setInitialFoundCountries();
    }
    public void setController(Controller controller) {
        this.controller = controller;
    }
    
    private void readFromFile() {
        File file = new File(filePath);
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            
            originalCountries = br.lines().collect(Collectors.toList());
            
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            System.out.println("File Not Found!");
        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println("IO Exception");
        }
    }
  



    public void setInitialFoundCountries(){
        foundCountries = originalCountries;
    }
    public void setFoundCountries(List<String> foundCountries) {
        this.foundCountries = foundCountries;
        controller.updateView(foundCountries);  //mvc
    }

    public List<String> getOriginalCountries() {
        return originalCountries;
    }

    public List<String> getFoundCountries() {
        return foundCountries;
    }

    @Override
    public String toString() {
        return "Model{" + "originalCountries=" + originalCountries + ", foundCountries=" + foundCountries + '}';
    }

}
