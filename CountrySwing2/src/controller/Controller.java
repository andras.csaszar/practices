/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import java.util.stream.Collectors;
import model.Model;
import view.SwingView;

/**
 *
 * @author andra
 */
public class Controller {

    private Model m;
    private SwingView v;

    public Controller(Model m, SwingView v) {
        this.m = m;
        this.v = v;

        m.setController(this);
        v.setController(this);

    }

    public void handleSearch(String input) {
        updateModel(filterStrings(input, m.getOriginalCountries()));
        //updateView(m.getFoundCountries());  //mvp
    }

    private List<String> filterStrings(String toContain, List<String> countries) {

        return countries.stream()
                .filter(c -> (c.toLowerCase()).contains((toContain.toLowerCase())))
                .collect(Collectors.toList());

    }

    private void updateModel(List<String> newStrings) {
        m.setFoundCountries(newStrings);
    }

    public void updateView(List<String> newStrings) {
        v.setResults(newStrings);
    }

}
