/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import controller.Controller;
import model.Model;
import view.SwingView;


/**
 *
 * @author andra
 */
public class CountrySwing2App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SwingView v = new SwingView();
        Model m = new Model();
        Controller c = new Controller(m, v);
        v.enableView();
    }
    
}
