/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import java.awt.BorderLayout;

import java.util.List;
import java.util.stream.Collectors;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author andra
 */
public class SwingView extends JFrame {

    private Controller controller;

    private JTextField inputText;
    private JButton searchButton;
    private JTextArea resultArea;

    public void enableView() {
        construct();
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

    }

    public void setResults(List<String> newStringsToShow) {
        resultArea.setText(newStringsToShow.stream()
                .collect(Collectors.joining("\n")));
        String as = resultArea.getText();
    }

    private void construct() {
        buildNorth();
        buildCenter();
        //buildSouth();

    }

    private void buildNorth() {

        inputText = new JTextField(50);
        searchButton = new JButton("Search");

        JPanel northPanel = new JPanel();
        northPanel.add(inputText);
        northPanel.add(searchButton);

        add(northPanel, BorderLayout.NORTH);

        searchButton.addActionListener(e -> controller.handleSearch(inputText.getText()));

    }

    private void buildCenter() {
        resultArea = new JTextArea(50, 100);

        JPanel centerPanel = new JPanel();
        centerPanel.add(resultArea);
        add(centerPanel, BorderLayout.CENTER);
    }

    private void buildSouth() {
        resultArea = new JTextArea(50, 100);

        JPanel southPanel = new JPanel();
        // southPanel.add();
        add(southPanel, BorderLayout.SOUTH);
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

}
