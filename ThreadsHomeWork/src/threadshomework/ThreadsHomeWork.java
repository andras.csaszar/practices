/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threadshomework;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andra
 */
public class ThreadsHomeWork {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        NamePrint t1 = new NamePrint();
        NamePrint t2 = new NamePrint();
        NamePrint t3 = new NamePrint();
        NamePrint t4 = new NamePrint();
        NamePrint t5 = new NamePrint();
        
        t1.start();
        try {
            t1.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(ThreadsHomeWork.class.getName()).log(Level.SEVERE, null, ex);
        }
        t2.start();
        try {
            t2.join(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(ThreadsHomeWork.class.getName()).log(Level.SEVERE, null, ex);
        }
        t3.start();
        try {
            t3.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(ThreadsHomeWork.class.getName()).log(Level.SEVERE, null, ex);
        }
        t4.start();
        t5.start();
             
        
        
    }
    
}
