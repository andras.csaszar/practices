/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfiewpractice;

import java.util.Comparator;

/**
 *
 * @author andra
 */
public class MovieComparatorName implements Comparator <Movie>{

    @Override
    public int compare(Movie m1, Movie m2) {
        
        return m1.getName().compareTo(m2.getName());
        
    }
    
}
