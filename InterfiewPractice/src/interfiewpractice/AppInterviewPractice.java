/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfiewpractice;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andra
 */
public class AppInterviewPractice {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws MyCheckedExceptionIOException {
        Map< String, Integer> hm = new HashMap< String, Integer>();
        hm.put("a", new Integer(100));
        hm.put("b", new Integer(200));
        hm.put("c", new Integer(300));
        hm.put("d", new Integer(400));

        // Returns Set view      
        Set< Map.Entry< String, Integer>> st = hm.entrySet();

        for (Map.Entry< String, Integer> me : st) {
            System.out.print(me.getKey() + ":");
            System.out.println(me.getValue());
        }

        Map<String, Integer> hM = new HashMap<>();
        hM.put("A", new Integer(10));
        hM.put("B", new Integer(20));
        hM.put("C", new Integer(30));
        hM.put("D", new Integer(40));
        hM.put("E", new Integer(50));

        System.out.println(hM.entrySet());

        Set<Map.Entry<String, Integer>> setView = hM.entrySet();
        System.out.println("--------- own test --------");
        for (Map.Entry<String, Integer> me : setView) {
            System.out.println(me.toString());
        }

//        throw new UnsupportedOperationException();
        Movie m1 = new Movie("Street", 2018, 5.4d);
        Movie m2 = new Movie("Burn", 2017, 5.7d);
        Movie m3 = new Movie("Fight", 2016, 6.4d);
        Movie m4 = new Movie("Let", 2016, 7.4d);
        Movie m5 = new Movie("Shot", 2015, 8.0d);
        Movie m6 = new Movie("Steel", 2017, 9.1d);
        Movie m7 = new Movie("Grow", 2013, 1.4d);
        Movie m8 = new Movie("Sink", 2011, 5.3d);

        List<Movie> list = new ArrayList<>();

        list.add(m1);
        list.add(m2);
        list.add(m3);
        list.add(m4);
        list.add(m5);
        list.add(m6);
        list.add(m7);
        list.add(m8);

        for (Movie m : list) {

            System.out.println(m);
        }

        System.out.println("Do camparator sorting by Collections.sort\n\n");
        MovieComparatorRating mc = new MovieComparatorRating();
        Collections.sort(list, mc);

        for (Movie m : list) {
            System.out.println(m);
        }

        if (m2.compareTo(m8) < 0) {
            System.out.println("m2 is less");
        }
        if (m2.compareTo(m8) > 0) {
            System.out.println("m8 is less");
        }
        if (m2.compareTo(m8) == 0) {
            System.out.println(" equal ");
        }

        Iterator<Movie> itr = list.iterator();

        while (itr.hasNext()) {

            System.out.println(itr.next());

        }

        for (WeekDays wd : WeekDays.values()) {
            System.out.print("next day: ");
            System.out.print(wd.toString() + "\t" + wd.ordinal() + "\n");
        }

        String s = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        System.out.println("Adjon meg egy számot");

        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            if (br.ready()) {
                System.out.println("ready to read");
            } else {
                System.out.println("not ready");
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        }

        try {
            BufferedReader br2 = new BufferedReader(new FileReader("file"));
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }

        Car car = new Car.CarBuilder(250, true)
                .setColor("Green")
                .setMode("Mercedes")
                .build();

    }
}
