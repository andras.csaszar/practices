/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfiewpractice;

/**
 *
 * @author andra
 */
public class Movie implements Comparable<Movie>{
    
    private String name;
    private int year;
    private double rating;

    public Movie(String name, int year, double rating) {
        this.name = name;
        this.year = year;
        this.rating = rating;
    }
    
    @Override
    public int compareTo(Movie m) {
            return this.year - m.year ;
    }

    public int getYear() {
        return year;
    }

    public double getRating() {
        return rating;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Movie{" + "name=" + name + ", year=" + year + ", rating=" + rating + '}';
    }

    
}
