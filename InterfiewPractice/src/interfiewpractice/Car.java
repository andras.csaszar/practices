/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfiewpractice;

/**
 *
 * @author andra
 */
public class Car {

    private int maxSpeed;
    private boolean abs;
    private String color;
    private String model;

    public static class CarBuilder {

        private int maxSpeed;
        private boolean abs;
        private String color;
        private String model;

        public CarBuilder(int maxSpeed, boolean abs) {
            this.maxSpeed = maxSpeed;
            this.abs = abs;

        }

        public CarBuilder setAbs(boolean abs) {
            this.abs = abs;
            return this;
        }

        public CarBuilder setMaxSpeed(int maxSpeed) {
            this.maxSpeed = maxSpeed;
            return this;

        }

        public CarBuilder setColor(String color) {
            this.color = color;
            return this;
        }

        public CarBuilder setMode(String model) {
            this.model = model;
            return this;
        }

        public Car build() {
            return new Car(this);
        }
    }

    public Car(CarBuilder cb) {
        this.abs = cb.abs;
        this.color = cb.color;
        this.maxSpeed = cb.maxSpeed;
        this.model = cb.model;

    }

}
