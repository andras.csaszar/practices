package bh11_2019_12_08_streampractise;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {

    static List<Country> countries = new ArrayList<>();

    public static void main(String[] args) {
        generateData();
    }

    public static void generateData() {
        Country hungary = new Country("hu", "Hungary", "Europe", 93);
        hungary.addCity(new City("Budapest", 2000));
        hungary.addCity(new City("Szeged", 800));
        hungary.addCity(new City("Nyíregyháza", 200));
        hungary.addCity(new City("Tokaj", 30));

        Country spain = new Country("sp", "Span", "Europe", 110);
        spain.addCity(new City("Barcelona", 300));
        spain.addCity(new City("Madrid", 250));

        Country thailand = new Country("th", "Thailand", "Asia", 200);
        thailand.addCity(new City("Phuket", 50));
        thailand.addCity(new City("Bangkok", 3000));

        countries.add(hungary);
        countries.add(hungary);
        countries.add(spain);
        countries.add(thailand);

        printAllCountries();
        System.out.println("----------");
        printDistinctCountries();
        System.out.println("----------");
        printCountriesWhereNameContains("ar");
        System.out.println("----------");
        printCountOfCountriesWhereNameContains("ar");
        System.out.println("----------");
        printCountryByCode("th");
        System.out.println("---SortAllOfCountry-------");
        sortAllOfCountry();
        System.out.println("----------");
        printAllCities();
        System.out.println("----------");
        printContinents();
        System.out.println("----------");
        printCountryNames();
        System.out.println("----------");
        printCountryCodes();

        System.out.println("----------");
        printAllOfCountriesPopulationLessThanX(80000);
        System.out.println("EZ a flatmap-es");
        printSummOfEuropePopulationFlatMap();
    }

    //basic
    public static void printAllCountries() {

        countries.stream()
                .forEach(System.out::println);

    }

    //basic
    public static void printDistinctCountries() {

        countries.stream()
                .distinct()
                .forEach(System.out::println);

    }

    //basic
    public static void printCountriesWhereNameContains(String str) {

        countries.stream()
                .filter(p -> p.getName().contains(str))
                .forEach(ctry -> System.out.println(ctry));

    }

    //basic
    public static void printCountOfCountriesWhereNameContains(String str) {
        System.out.println(countries.stream()
                .filter(p -> p.getName().contains(str))
                .count());
//               .forEach(ctry -> System.out.println(ctry));
    }

    //basic
    public static void printCountryByCode(String code) {
        countries.stream()
                .filter(p -> p.getCode().equals(code))
                .forEach(p -> System.out.println(p));
    }

    //basic
    public static void sortAllOfCountry() {
        // Collections.sort((s1, s2) -> s1.charAt(0) - s2.charAt(0));
        countries.stream()
                .sorted((c1, c2) -> c1.getName().compareTo(c2.getName()))
                .forEach(p -> System.out.println(p));
    }

    //extra
    public static void printAllCities() {
        countries.stream()
                .forEach(c -> c.getCities().stream()
                        .forEach(city -> System.out.println(city))
                );

    }

    public static void printAllCities2() {
        List<List<City>> list = countries
                .stream()
                .map(country -> country.getCities())
                .collect(Collectors.toList());

        /*
        List<City> merged = new ArrayList<>();
        for (list<City> subList : list) {
            merged
        }*/

    }

    //basic
    public static void printContinents() {
        countries.stream()
                .map(country -> country.getContinent())
                .distinct()
                .forEach(c -> System.out.println(c));

    }

    //basic
    public static void printCountryNames() {
        countries.stream()
                .map(country -> country.getName())
                .distinct()
                .forEach(c -> System.out.println(c));
    }

    //basic
    public static void printCountryCodes() {
        countries.stream()
                .map(country -> country.getCode())
                .distinct()
                .forEach(c -> System.out.println(c));
    }

    //extra
    public static void printSummOffPopulation() {

        int populationAll
                = countries.stream()
                .mapToInt(country -> country.getCities()
                        .stream()
                        .mapToInt(city -> city.getPopulation())
                        .sum())
                .sum();

        System.out.println(populationAll);
//        countries.get(0).getCities().stream().mapToInt(city -> city.getPopulation()).sum();

    }
    //Attilával órai:
        public static void printSummOfFEuropePopulationFlatMap() {

        countries.stream()
                .filter(c -> "Europe".equals(c.getContinent()))
                .map(c->c.getCities())
                .flatMap(cities->cities.stream())
                .mapToInt(city -> city.getPopulation())
                .sum();
    }

    //extra
    public static void printSummOfEuropePopulation() {

        int populationEurope
                = countries.stream()
                .filter(p -> p.getContinent().equals("Europe"))
                .mapToInt(country -> country.getCities()
                        .stream()
                        .mapToInt(city -> city.getPopulation())
                        .sum())
                .sum();
        System.out.println(populationEurope);
    }
//Attilával órai:
    public static void printSummOfEuropePopulationFlatMap() {
        
        countries.stream()
                .filter(c -> "Europe".equals(c.getContinent()))
                .map(c->c.getCities())
                .flatMap(cities->cities.stream())
                .forEach(System.out::println);
    }

    //extra
    public static void printAllOfCountriesPopulationLessThanX(int x) {
        int populationAllLessThanX
                = countries.stream()
                .mapToInt(country -> country.getCities()
                        .stream()
                        .mapToInt(city -> city.getPopulation())
                        .sum())
                .filter(pop -> pop < x)
                .sum();

        System.out.println(populationAllLessThanX);
    }
}
