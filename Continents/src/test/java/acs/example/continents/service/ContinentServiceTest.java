package acs.example.continents.service;

import acs.example.continents.dto.ContinentDto;
import acs.example.continents.dto.CountryDto;
import acs.example.continents.repository.ContinentRepository;
import acs.example.continents.repository.CountryRepository;
import acs.example.continents.repository.entity.Continent;
import acs.example.continents.repository.entity.Country;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = ContinentServiceTest.Config.class )
class ContinentServiceTest {

    @TestConfiguration
    @ComponentScan(basePackageClasses = ContinentService.class)
    static class Config {
        @Bean
        public CountryRepository countryRepository() {
            return mock(CountryRepository.class);
        }
        @Bean
        public ContinentRepository continentRepository() {
            return mock(ContinentRepository.class);
        }
    }
    private static final String CONTINENT_NAME = "A";
    private static final Integer CONT_ID = 1;
    private static ContinentDto continentDto = new ContinentDto();

    @Autowired
    private ContinentRepository continentRepository;
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private ContinentService underTest;

    @Test
    void test(){

        assertNotNull(continentRepository);
        assertNotNull(countryRepository);
        assertNotNull(underTest);
    }

    @Test
    void testFilter(){
        //Given
        Continent continentA = createContinent("A");

        Country countryA = createCountry(continentA, "A");
        Country countryB = createCountry(continentA, "B");

        List<Country> countries = Arrays.asList(countryA, countryB);
        when(continentRepository.findById(anyInt())).thenReturn(Optional.of(continentA));
        when(countryRepository.filterCountryByContinent(any(Continent.class))).thenReturn(countries);

        CountryDto countryDtoA = createCountryDto(
                countryA.getContinent().getContinentId(),
                countryA.getContinent().getContinentName(),
                countryA.getCountryId(),
                countryA.getCountryName()
        );
        CountryDto countryDtoB = createCountryDto(
                countryB.getContinent().getContinentId(),
                countryB.getContinent().getContinentName(),
                countryB.getCountryId(),
                countryB.getCountryName()
        );
        List<CountryDto> expectedResult = Arrays.asList(countryDtoA,countryDtoB);

        //When
        continentDto.setContinentId(CONT_ID);
        continentDto.setContinentName("B");
        List<CountryDto> result = underTest.findByContinent(continentDto);
        //Then
        assertEquals(expectedResult,result);
    }
    private Continent createContinent(String name){
        Continent cnt = new Continent();
        cnt.setContinentId(CONT_ID);
        cnt.setContinentName(name);
        cnt.setCountries(new ArrayList<>());
        return cnt;

    }
    private Country createCountry(Continent continent, String countryName){
        Country c = new Country();
        c.setContinent(continent);
        //c.setCountryId();
        c.setCountryName(countryName);
        return c;
    }

    private CountryDto createCountryDto(Integer contId, String continentName, Integer id, String name ){
        CountryDto dto = new CountryDto();
        dto.setContinentId(contId);
        dto.setContinentName(continentName);
        dto.setCountryId(id);
        dto.setCountryName(name);
        return dto;
    }

}

