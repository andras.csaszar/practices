package acs.example.continents;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "acs.example.continents.repository")
@ComponentScan("acs.example.continents.repository")
@EntityScan(basePackages = "acs.example.continents.repository.entity")
public class ContinentConfig {
}
