package acs.example.continents.rest;

import acs.example.continents.dto.CountryDto;
import acs.example.continents.dto.CountryFilterRequest;
import acs.example.continents.dto.CountryFilterResponse;
import acs.example.continents.service.ContinentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/continent")
public class ContinentRestController {

    @Value("${continents.service.name}")
    String serviceName;

    @Autowired
    ContinentService continentService;

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String hello() {
        return "Hello World from " + serviceName + "!";

    }

    @RequestMapping(value = "/filter", method = RequestMethod.POST)
    public ResponseEntity filter(@RequestBody CountryFilterRequest request){
        List<CountryDto> countryDtos = (continentService.findByContinentName(request.getContinentName()));
        CountryFilterResponse response =new CountryFilterResponse();
        response.setCountryDtos(countryDtos);
        return ResponseEntity.ok(response);
    }

}
