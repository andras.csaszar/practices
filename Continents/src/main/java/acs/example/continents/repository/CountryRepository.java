package acs.example.continents.repository;

import acs.example.continents.repository.entity.Country;
import acs.example.continents.repository.entity.Continent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CountryRepository extends JpaRepository<Continent, Integer> {
    @Query("SELECT c from Country c WHERE c.continent = :continent")
    List<Country> filterCountryByContinent(@Param("continent") Continent continent);

}
