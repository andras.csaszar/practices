package acs.example.continents.repository.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name ="continent")
public class Continent implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="continent_id")
    private Integer continentId;
    @Column(name="continent_name")
    private String continentName;
    @OneToMany(mappedBy = "continent", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<Country> countries;

    public Continent() {
    }

    public Integer getContinentId() {
        return continentId;
    }

    public void setContinentId(Integer continentId) {
        this.continentId = continentId;
    }

    public String getContinentName() {
        return continentName;
    }

    public void setContinentName(String continentName) {
        this.continentName = continentName;
    }

    public List<Country> getCountries() {
        return countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Continent continent = (Continent) o;
        return continentId.equals(continent.continentId) &&
                continentName.equals(continent.continentName) &&
                countries.equals(continent.countries);
    }

    @Override
    public int hashCode() {
        return Objects.hash(continentId, continentName, countries);
    }

    @Override
    public String toString() {
        return "Continent{" +
                "continentId=" + continentId +
                ", continentName='" + continentName + '\'' +
                ", countries=" + countries +
                '}';
    }
}
