package acs.example.continents.repository;

import acs.example.continents.repository.entity.Continent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ContinentRepository extends JpaRepository<Continent, Integer> {
    @Query("SELECT con FROM Continent con WHERE con.continentName = :continentName")
    Continent findByName(@Param("continentName") String continentName);

    @Query("SELECT con FROM Continent con WHERE con.id = :continentId")
    Optional<Continent> findById(@Param("continentId") Integer continentId);


}
