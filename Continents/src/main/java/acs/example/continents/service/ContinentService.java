package acs.example.continents.service;

import acs.example.continents.dto.ContinentDto;
import acs.example.continents.dto.CountryDto;
import acs.example.continents.repository.ContinentRepository;
import acs.example.continents.repository.CountryRepository;
import acs.example.continents.repository.entity.Country;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class ContinentService {
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private ContinentRepository continentRepository;


    public List<CountryDto> findByContinentName(String continentName){
        List<Country> countries =countryRepository.filterCountryByContinent(
                continentRepository.findByName(continentName)
        );

        List<CountryDto> countryDtos = new ArrayList<>();

        for(Country c: countries){
            CountryDto ctrDto = new CountryDto();
            BeanUtils.copyProperties(c,ctrDto);
            /*
            ContinentDto continentDto = new ContinentDto();
            BeanUtils.copyProperties(c.getContinent(),continentDto);
            ctrDto.setContinentDto(continentDto);
            */
            ctrDto.setContinentId(c.getContinent().getContinentId());
            ctrDto.setContinentName(c.getContinent().getContinentName());

            countryDtos.add(ctrDto);
        }
        return countryDtos;
    }


    public List<CountryDto> findByContinent(ContinentDto continentDto){
        List<Country> countries =countryRepository.filterCountryByContinent(
                continentRepository.findById(continentDto.getContinentId()).orElseThrow(NoSuchElementException::new)
        );
        List<CountryDto> countryDtos = new ArrayList<>();

        for(Country c: countries){
            CountryDto ctrDto = new CountryDto();
            BeanUtils.copyProperties(c,ctrDto);
            countryDtos.add(ctrDto);
        }
        return countryDtos;
    }
}
