package acs.example.continents.dto;


import acs.example.continents.repository.entity.Country;

import java.util.List;
import java.util.Objects;

public class ContinentDto {

    private Integer continentId;

    private String continentName;

    private List<Country> countries;

    public Integer getContinentId() {
        return continentId;
    }

    public void setContinentId(Integer continentId) {
        this.continentId = continentId;
    }

    public String getContinentName() {
        return continentName;
    }

    public void setContinentName(String continentName) {
        this.continentName = continentName;
    }

    public List<Country> getCountries() {
        return countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContinentDto continent = (ContinentDto) o;
        return continentId.equals(continent.continentId) &&
                continentName.equals(continent.continentName) &&
                countries.equals(continent.countries);
    }

    @Override
    public int hashCode() {
        return Objects.hash(continentId, continentName, countries);
    }

    @Override
    public String toString() {
        return "Continent{" +
                "continentId=" + continentId +
                ", continentName='" + continentName + '\'' +
                ", countries=" + countries +
                '}';
    }
}
