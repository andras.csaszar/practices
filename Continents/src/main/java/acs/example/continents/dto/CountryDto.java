package acs.example.continents.dto;

import java.util.Objects;

public class CountryDto {

    private Integer countryId;

    private String countryName;

 //   private ContinentDto continentDto;
    private String continentName;
    private Integer continentId;

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getContinentName() {
        return continentName;
    }

    public void setContinentName(String continentName) {
        this.continentName = continentName;
    }

    public Integer getContinentId() {
        return continentId;
    }

    public void setContinentId(Integer continentId) {
        this.continentId = continentId;
    }
/* public ContinentDto getContinentDto() {
        return continentDto;
    }*/

   /* public void setContinentDto(ContinentDto continentDto) {
        this.continentDto = continentDto;
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryDto that = (CountryDto) o;
        return countryId.equals(that.countryId) &&
                countryName.equals(that.countryName) &&
                continentName.equals(that.continentName) &&
                continentId.equals(that.continentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(countryId, countryName, continentName, continentId);
    }

    @Override
    public String toString() {
        return "CountryDto{" +
                "countryId=" + countryId +
                ", countryName='" + countryName + '\'' +
                ", continentName='" + continentName + '\'' +
                ", continentId=" + continentId +
                '}';
    }
}
