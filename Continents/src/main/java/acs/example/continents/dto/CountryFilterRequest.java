package acs.example.continents.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Objects;

public class CountryFilterRequest implements Serializable {
    @JsonProperty("continent")
    private String continentName;

    public String getContinentName() {
        return continentName;
    }

    public void setContinentName(String continentName) {
        this.continentName = continentName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryFilterRequest that = (CountryFilterRequest) o;
        return Objects.equals(continentName, that.continentName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(continentName);
    }
}
