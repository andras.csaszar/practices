package acs.example.continents.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class CountryFilterResponse implements Serializable {

    @JsonProperty(value = "countries")
    private List<CountryDto> countryDtos;

    public List<CountryDto> getCountryDtos() {
        return countryDtos;
    }

    public void setCountryDtos(List<CountryDto> countryDtos) {
        this.countryDtos = countryDtos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryFilterResponse that = (CountryFilterResponse) o;
        return Objects.equals(countryDtos, that.countryDtos);
    }

    @Override
    public int hashCode() {
        return Objects.hash(countryDtos);
    }

    @Override
    public String toString() {
        return "CountryFilterResponse{" +
                "countryDtos=" + countryDtos +
                '}';
    }
}
