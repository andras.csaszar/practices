/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import static com.sun.javafx.fxml.expression.Expression.add;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import swing1.Controller;

/**
 *
 * @author andra
 */
public class SwingView extends JFrame implements View {
private Controller controller;

    private final JButton jb = new JButton("Convert!");
    private final JTextField from = new JTextField(25);
    private final JTextField to = new JTextField(25);
    private final JTextField character = new JTextField(1);
    private final JComboBox <Character>jc = new JComboBox<>(new Character[]{'A', 'B', 'C', 'Z'});

    public SwingView() {
        buildWindow();
    }

    private JPanel buildNorth() {
        JLabel fromLabel = new JLabel("FROM:");
        JPanel north = new JPanel();
        north.add(fromLabel);
        north.add(from);
        north.add(jc);
        return north;
    }

    private JPanel buildSouth() {
        JLabel toLabel = new JLabel("TO:");
        JPanel south = new JPanel();
        south.add(toLabel);
        south.add(to);
        return south;
    }

    private JPanel buildCenter() {
        JPanel center = new JPanel();
        center.add(jb);
        return center;
    }

    private void buildWindow() {
        jb.addActionListener(e -> new Controller().copyText(from, to, (char) jc.getSelectedItem()));

        add(buildNorth(), BorderLayout.NORTH);
        add(buildCenter(), BorderLayout.CENTER);
        add(buildSouth(), BorderLayout.SOUTH);
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    @Override
    public void setController(Controller c) {
        controller = c;
    }
}
