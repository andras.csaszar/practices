/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.util.Scanner;
import javax.swing.text.View;
import swing1.Controller;

/**
 *
 * @author andra
 */
public class ConsoleView implements View {

    private static final String STOP = "exit";

    public ConsoleView() {
        startConsoleReader();
    }

    private void startConsoleReader() {

        try (Scanner scanner = new Scanner(System.in)) {
            String inputText;
            do {
                System.out.println("Input text:");
                inputText = scanner.nextLine();

                if (STOP.equalsIgnoreCase(inputText)) {
                    break;
                }

                System.out.println("Offset character: ");
                char c = scanner.nextLine().charAt(0);

                System.out.println("Result: " + new Controller().shiftTextByCharacter(inputText, c));

            } while (true);
        }
    }
}
