/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swing1;

import view.SwingView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import model.EncryptedText;
import model.Model;
import view.ConsoleView;
import view.View;

/**
 *
 * @author andra
 */
public class Swing1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        /* külön osztályban megcsináltuk
        
        JFrame jm = new JFrame();
        JButton jb = new JButton("Click");
        jb.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Alma");
            }
        });
        jb.addActionListener(l -> System.out.println("LAMBDA"));

        jm.add(jb);
        jm.setSize(300, 300);
        jm.setResizable(false);
        jm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jm.setVisible(true);
        */
        
       // SwingView f = new SwingView();
        View v = new SwingView();
        Model m = new EncryptedText();
        
        Controller c = new Controller(v, m);
        
    }

}
