/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swing1;

import java.awt.List;
import java.util.ArrayList;
import javax.swing.JTextField;
import javax.swing.text.View;
import model.Model;

/**
 *
 * @author andra
 */
public class Controller {

    private final View view;
    private final Model model;

    public Controller(View view, Model model) {
        this.view = view;
        this.model = model;
        
        view.setController(this);
        model.setController(this);
    }
    
    
    
    public void copyText(JTextField from, JTextField to, char c) {
        int offset = c - 'A';
        StringBuilder str = new StringBuilder();

        for (char character : from.getText().toCharArray()) {
            char next = (char) (character + offset);

            if (next > 'Z') {
                str.append((char) ('A' + (next - 'Z') - 1));
            } else {
                str.append(next);
            }
        }

    }

    public String shiftTextByCharacter(String text, char inputCharacter) {

        int offset = inputCharacter - 'A';
        StringBuilder str = new StringBuilder();

        for (char character :text.toCharArray()) {
            char next = (char) (character + offset);

            if (next > 'Z') {
                str.append((char) ('A' + (next - 'Z') - 1));
            } else {
                str.append(next);
            }
        }

    }

    /* int move = (int) jtInput.getText().charAt(0);

     StringBuilder convertedText = new StringBuilder(jtFrom.getText());
            

     Character[] convertedTextStr = new String[convertedText.length()];
            

     for (char c : convertedText) {
     c += move;
     }
     StringBuilder resultOfConvText = convertedText.;
     for (int i = 0; i < resultOfConvText.length; i++) {
     resultOfConvText[i] = convertedText[i];
     }

     jtTo.setText(resultOfConvText);
     */
}
