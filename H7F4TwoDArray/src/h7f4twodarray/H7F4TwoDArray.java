/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h7f4twodarray;

/**
 *
 * @author andra
 */
public class H7F4TwoDArray {

    /**
     * @param args the command line arguments
     */
    static int getRandomNumber(int to) {

        return (int) (Math.random() * to);

    }

    static int getMinValueOfArray(int array[][]) {

        int min = array[0][0];

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < 10; j++) {
                if (min > array[i][j]) {
                    min = array[i][j];
                }
            }

        }

        return min;
    }

    static int[][] fillInArray(int[][] array) {

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                array[i][j] = getRandomNumber(100);

            }
        }
        return array;
    }

    static int[] getMinValueIndexOfArray(int array[][]) {

        int min = array[0][0];
        int minIndexI = -1;
        int minIndexJ = -1;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < 10; j++) {
                if (min > array[i][j]) {
                    min = array[i][j];
                    minIndexI = i;
                    minIndexJ = j;
                }
            }

        }
        int[] result = {minIndexI, minIndexJ, min};
        
        return result;
    
        

    }
    public static void main(String[] args) {
        // TODO code application logic here

        /*
         - írjunk metódust, ami egy n*m-es kétdimenziós tömböt feltölt véletlenszerű elemekkel
         valamilyen határok között, majd írjunk egy másik metódust
         ami egy n*m-es kétdimenziós tömb legkisebb elemével és annak helyével tér vissza.
         A visszatérés egy tömb legyen, az alábbi formában [sor, oszlop, érték]

         */
    }

}
