/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h3f7if;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class H3F7If {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // 7. Kérj be egy számot a felhasználótól.
        // Ha 7-tel osztható, írd ki, hogy "kismalac". Ezt a feladatot oldd meg if és switch felhasználásával is.
        Scanner sc = new Scanner(System.in);
        int number = -1;
        boolean isInputValid;

        do {
            isInputValid = true;
            System.out.println("Adjon meg egy számot, ha 7-tel osztható, kiírom hogy kismalac.");

            if (sc.hasNextInt()) {
                number = sc.nextInt();
            } else {
                isInputValid = false;
                sc.next();
            }

        } while (!isInputValid);

        if (number%7==0) {
            System.out.println("kismalac");
        } else {
            System.out.printf("%d nem oszthatü 7-tel.\n", number);
        }

    }

}
