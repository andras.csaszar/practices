/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genericshomeworkpractice;

import java.util.ArrayList;

/**
 *
 * @author andra
 */

abstract class Animal {}

class Cat extends Animal {}

class Dog extends Animal {}

public class GenericsHomeWorkPractice {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList<?> house11 = new ArrayList<Cat>();
        ArrayList<?> house12 = new ArrayList<Animal>();
        ArrayList<?> house13 = new ArrayList<Dog>();
        ArrayList<?> house14 = new ArrayList<Object>();
        ArrayList<Object> house21 = new ArrayList<Cat>();
        ArrayList<Object> house22 = new ArrayList<Object>();
        ArrayList<Object> house23 = new ArrayList<?>();
        ArrayList<? super Animal> house31 = new ArrayList<Cat>();
        ArrayList<? super Animal> house32 = new ArrayList<Object>();
        ArrayList<? super Animal> house33 = new ArrayList<Animal>();
        ArrayList<? extends Animal> house41 = new ArrayList<Animal>();
        ArrayList<? extends Animal> house42 = new ArrayList<Object>();
        ArrayList<? extends Animal> house43 = new ArrayList<Cat>();
    }
    
}
