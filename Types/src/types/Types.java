/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package types;

/**
 *
 * @author andra
 */
public class Types {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    
        //long a = 100000000000; // alapból int-nek veszi, ezért hiba
        long a = 1000000000000L;
        //byte b = 128; //veszteség nélkül nem tudja tárolni, de cast-olással tudjuk kényszeríteni
        byte b = (byte) 128;
        float f =(float) 5.666;
        float f1 = 5.666f;
        
    }
    
}
