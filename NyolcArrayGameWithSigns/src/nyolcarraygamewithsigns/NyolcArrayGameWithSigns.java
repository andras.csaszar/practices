/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Feladat
        /*
 10 x 10 -es tömb
 5 csillag legyen véletlen helyen (egy helyen lehet több csillag is, de az 1-nek számít)
 3 db kérdőjel ? egy helyen csak egy ? lehet
 fel kell szedni az elemeket j,b,l,f moozgássa
 addig nem lehet felszedni kérdőjeleket, amíg van fent * a pályán
 */
package nyolcarraygamewithsigns;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class NyolcArrayGameWithSigns {

    static final int SIZE = 10;
    static final int NUMBER_OF_QUESTION_MARKS = 3;
    static final int NUMBER_OF_ASTERIX = 5;

    static final char QUESTION_MARK = '?';
    static final char ASTERIX = '*';
    static final char UP = 'u';
    static final char DOWN = 'd';
    static final char LEFT = 'l';
    static final char RIGHT = 'r';
    static final char EMPTY = '\0';
    static final char PLAYER = '\u263A';

    static int playerX = 0;
    static int playerY = 0;
    static char[][] field = new char[SIZE][SIZE];

    static int getPosition() {
        return (int) (Math.random() * SIZE);
    }

    static boolean isCharacterSetable(int x, int y, char character, boolean isRepeatable) {

        return field[x][y] == EMPTY || (isRepeatable && field[x][y] == character);

    }

    static boolean shouldSkipCycle(int x, int y, char character, boolean isRepeatable) {
        return (isRepeatable && field[x][y] != EMPTY && field[x][y] != character) || (!isRepeatable && field[x][y] != EMPTY);
    }

    static void putElements(int count, char character, boolean isRepeatable) {
        for (int i = 0; i < count; i++) {
            int x = getPosition();
            int y = getPosition();

            if (isCharacterSetable(x, y, character, isRepeatable)) {
                field[x][y] = character;
            } else if (shouldSkipCycle(x, y, character, isRepeatable)) {
                i--;


            
            
//            if (isRepeatable) {
//                if (field[x][y] == EMPTY || field[x][y] == character) {
//                    field[x][y] = character;
//                } else {
//                    i--;
//                }
//            } else {
//                if (field[x][y] == EMPTY) {
//                    field[x][y] = character;
//                } else {
//                    i--;
//                }
//            }

        }
    }

    static void setPlayer() {
        field[playerX][playerY] = PLAYER;
    }

    static boolean isCharacter(int x, int y, char character) {
        return field[x][y] == character;
    }

    static boolean anyCharacterOnTheField(char character) {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (isCharacter(i, j, character)) {
                    return true;
                }
            }
        }
        return false;
    }

    static void play() {
        Scanner scanner = new Scanner(System.in);
        while (anyCharacterOnTheField(QUESTION_MARK)) {
        print();
            char c = scanner.next().charAt(0);

            switch (c) {
                case DOWN:
                    handleDown();
                    break;
                case UP:
                    handleUp();
                    break;
                case LEFT:
                    handleLeft();
                    break;
                case RIGHT:
                    handleRight();
                    break;

            }
        }
        scanner.close();

    }

    static void performStep() {
        if (field[playerX][playerY] == ASTERIX || field[playerX][playerY] == EMPTY || field[playerX][playerY] == PLAYER) {
            field[playerX][playerY] = PLAYER;
        } else if (field[playerX][playerY] == QUESTION_MARK && !anyCharacterOnTheField(ASTERIX)) {
            field[playerX][playerY] = PLAYER;
        }
    }

    static void clearPosition(int x, int y) {
        if (field[x][y] == PLAYER) {
            field[x][y] = EMPTY;
        }

    }

    static void handleUp() {
        if (playerX != 0) {
            clearPosition(playerX, playerY);
            playerX--;
            performStep();

        }
    }

    static void handleDown() {
        if (playerX != field.length - 1) {
            clearPosition(playerX, playerY);
            playerX++;
            performStep();

        }
    }

    static void handleLeft() {
        if (playerY != 0) {
            clearPosition(playerX, playerY);
            playerY--;
            performStep();

        }
    }

    static void handleRight() {
        if (playerY != field[0].length - 1) {
            clearPosition(playerX, playerY);
            playerY++;

        }
    }

    static void print() {

        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[0].length; j++) {
                if (i == playerX && j == playerY) {
                    System.out.println(PLAYER);
                } else if (field[i][j] == ASTERIX || field[i][j] == QUESTION_MARK) {
                    System.out.println(field[i][j]);
                } else {
                    System.out.print(" ");
                }

            }
            System.out.println();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        setPlayer();
        putElements(NUMBER_OF_QUESTION_MARKS, QUESTION_MARK, false);
        putElements(NUMBER_OF_ASTERIX, ASTERIX, true);
        play();

    }

}
