/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h3f3;
import java.util.Scanner;
/**
 *
 * @author andra
 */
public class H3F3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
/*  3. Kérj be egy pozitív egész számot (addig kérjünk be adatot,
            amíg pozitív számot nem kapunk). Írd ki 0-tól az adott számig a számog négyzetét.
        Pl.: bemenet: 4 - kimenet: 0 1 4 9 16
*/
        Scanner sc = new Scanner (System.in);
                    int number = -1;
        do {
            System.out.println("Adjon meg egy számot! Kiírom 0-tól a számig a számok négyzetét");
            if (sc.hasNextInt()) {

                number = sc.nextInt();
            } else {
                sc.next();
            }

        } while (number < 0);

        for (int i = 0; i<number+1;i++){
            System.out.println((int)(Math.pow((i*1.0), 2)));
        
        }
    
    
    }
    
}
