<%-- 
    Document   : departments
    Created on : 2020.01.22., 20:48:48
    Author     : andra
--%>

<%@page import="hu.braininghub.bh11.Department"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% List<Department> departments = (List<Department>) request.getAttribute("departments");%>


        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Abbrev</th>
                    <th>Budget</th>
                </tr>
            </thead>
            <tbody>
                <%  for (int i = 0; i < departments.size(); i++) {%>
                <tr>
                    <td> <%= departments.get(i).getName()%></td>
                    <td> <%= departments.get(i).getAbbrev()%></td>
                    <td> <%= departments.get(i).getBudget()%></td>
                </tr>
                <% }%>
            </tbody>
        </table>  

        <h2>Add new department</h2>
        <form method="post" action="AddDepartmentServlet">
            <p> Name: <input type="text" name="name"/> </p>
            <p> Address <input type="text" name="abbrev"/></p>
            <p> Budget: <input type="number" name="budget"/></p>
            <input type="submit" value="Add department"/>
        </form>

        <h2>Delete department</h2>
        <form method="post" action="DeleteDepartmentServlet">
            <p> Name: <input type="text" name="name"/> </p>
            <input type="submit" value="Delete department"/>
        </form>
    </body>
</html>
