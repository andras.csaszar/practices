/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.text.html.StyleSheet;

/**
 *
 * @author andra
 */
public class EmployeeService {

    private static List<Employee> employees = new ArrayList<>();

    public List<Employee> getEmployees() {
        return employees;
    }

    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

    public  void deleteEmployeeByName(String name) {
        int index = getIndexByName(name);
        if (index != -1) {
            employees.remove(index);
        }
    }

    private int getIndexByName(String name) {
        for (int i = 0; i < employees.size(); i++) {
            if (name.equals(employees.get(i).getName())) {
                return i;
            }
        }
        return - 1;
    }
   
    
    public List<Employee> getEmployeesByDepartment(String department){
    return
            employees.stream()
            .filter(e-> department.equalsIgnoreCase(e.getDepartment()))
            .collect(Collectors.toList());
               
    }
    
}
