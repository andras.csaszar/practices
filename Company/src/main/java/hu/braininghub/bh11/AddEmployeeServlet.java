/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author andra
 */
public class AddEmployeeServlet extends HttpServlet {

    EmployeeService employeeService = new EmployeeService();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.employeeService.addEmployee(new Employee(request.getParameter("name"),
                request.getParameter("address"),
                Integer.valueOf(request.getParameter("salary")),
                request.getParameter("department")
        ));
        request.setAttribute("employees", employeeService.getEmployees());
        request.getRequestDispatcher("employees.jsp").forward(request, response);
    }

}
