/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author andra
 */
public class DepartmentService {

    private static List<Department> departments = new ArrayList<>();

    public List<Department> getDepartments() {
        return departments;
    }

    public void addDepartment(Department department) {
        departments.add(department);
    }

    public void deleteDepartmentByName(String name) {
        int index = getIndexByName(name);
        if (index != -1) {
            departments.remove(index);
        }

    }

    private int getIndexByName(String name) {
        for (int i = 0; i < departments.size(); i++) {
            if (name.equals(departments.get(i).getName())) {
                return i;
            }
        }
        return -1;
    }

}
