/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author andra
 */
public class AddDepartmentServlet extends HttpServlet {

    DepartmentService departmentService = new DepartmentService();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.departmentService.addDepartment(new Department(request.getParameter("name"),
                request.getParameter("abbrev"),
                Integer.valueOf(request.getParameter("budget"))
                ));
        request.setAttribute("departments", departmentService.getDepartments());
        request.getRequestDispatcher("departments.jsp").forward(request, response);
        
    }

}
