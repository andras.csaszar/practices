/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andra
 */
public class Jdbc2 {

    private static final String URL = "jdbc:mysql://localhost:3306/hr?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String USER = "root";
    private static String PW = "Vilagbeke2020";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String sql = "SELECT * FROM countries LIMIT 10";

        try (
                Connection conn = DriverManager.getConnection(URL, USER, PW);
                Statement stm = conn.createStatement();
                ResultSet rs = stm.executeQuery(sql);) {

            while (rs.next()) {
                System.out.println(rs.getString("country_name"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Jdbc2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
