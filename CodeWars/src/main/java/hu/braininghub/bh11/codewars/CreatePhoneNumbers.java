/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.codewars;

import java.util.Arrays;

/**
 *
 * @author andra
 */
public class CreatePhoneNumbers {

    public String createPhoneNumber(int[] numbers) {
        
        StringBuilder result = new StringBuilder();
        for (int i : numbers) {
            result.append(i);
        }
        result.insert(0, "(")
                .insert(4, ")")
                .insert(5, " ")
                .insert(9, "-");
        return result.toString();
    }

}
