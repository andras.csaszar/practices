/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.codewars;

/**
 *
 * @author andra
 */
public class DuplicateEncoder {

    public String encode(String word) {

        String word2 = word.toLowerCase();
        char[] chars = word2.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            final char cr = chars[i];
            int occur = (int) word2.chars().filter((t) -> cr == t).count();
            chars[i] = (occur > 1) ? (')') : ('(');

        }

        StringBuilder result = new StringBuilder();
        result.append(chars);
        return result.toString();

    }
}
