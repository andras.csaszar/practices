/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.codewars;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author andra
 */
public class VowelsCount {

    public int countVowels(String str) {
        int vowelsCount = 0;
        List<String> vowels = new ArrayList<>(Arrays.asList("a", "e", "i", "o", "u"));
        List<String> text = new ArrayList<>(Arrays.asList(str.split("{1}")));

        vowelsCount = (int) (text.stream().filter((s) -> vowels.contains(s)).count());

        return vowelsCount;
    }

    /*public long findNb(long m) {
        int n = 1;
        if (Math.pow(n, 3) < m) {
            findNb(++n,3);
        }
    }*/

}
