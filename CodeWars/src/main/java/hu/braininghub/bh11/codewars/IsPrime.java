/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.codewars;

/**
 *
 * @author andra
 */
public class IsPrime {

    public boolean isPrime(int num) {
        boolean result = true;

        if (num < 2) {
            result = false;
        } else {

            for (int i = 2; i <= Math.sqrt(num); i++) {
                if (i != num && num % i == 0) {
                    result = false;
                }
            }
        }
        return result;
    }
}
