/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.codewars;

/**
 *
 * @author andra
 */
public class ConvertStringToCamelCase {

    public String toCamelCase(String s) {
        StringBuilder sb = new StringBuilder();

        boolean wasWordDividerBefore = false;
        for (int i = 0; i < s.length(); i++) {
            Character c = s.charAt(i);
            String chr = c.toString();
            if (("-").equals(chr) || ("_").equals(chr)) {
                wasWordDividerBefore = true;
                continue;
            } else if (wasWordDividerBefore) {
                sb.append(chr.toUpperCase());
                wasWordDividerBefore = false;
            } else {
                sb.append(chr);
            }
        }

        return sb.toString();
    }

}
