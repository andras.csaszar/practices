package hu.braininghub.consolemenusql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author andra
 */
public class HrDao {

    private static final String URL = "jdbc:mysql://localhost:3306/hr?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String USER = "root";
    private static String PW = "Vilagbeke2020";

//  public HrDao(){
//        try {
//            Class.forName("com.mysql.jdbc.Driver");
//        } catch (ClassNotFoundException ex) {
//            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
//        }
//  
//  }
    public void findEmployeeWthMaxSalary() {

        String sql = "SELECT last_name, first_name, max(salary) FROM employees";

        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                Statement stm = connection.createStatement();) {
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getString("first_name") + " " + rs.getString("last_name"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void findMaxEmployeeDepartment() {

        String sql = "select * from departments where department_id = (\n"
                + "select department_id from employees group by department_id order by count(*) desc limit 1)";

        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                Statement stm = connection.createStatement();) {
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getString("department_name"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void findEmployeesEarnOverAverage() {

        String sql = "select first_name, last_name from employees where salary > (\n"
                + "select avg(salary) from employees as avgSalary)";

        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                Statement stm = connection.createStatement();) {
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getString("first_name") + " " + rs.getString("last_name"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void findEmployeesHiredAfter19910101() {

        String sql = "select first_name, last_name, hire_date from employees where hire_date > '1991.01.01'";

        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                Statement stm = connection.createStatement();) {
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getString("first_name") + " " + rs.getString("last_name") + " " + rs.getString("hire_date"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void findJobTitlesWithClerk() {

        String sql = "select distinct job_title from jobs where job_title LIKE \"%clerk\" order by job_title";

        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                Statement stm = connection.createStatement();) {
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getString("job_title"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void findEmployeeByIDUnsafe(String id) {

        String sql = "SELECT * "
                + "FROM employees e "
                + "WHERE e.employee_id =" + "'" + id + "'";

        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                Statement stm = connection.createStatement();) {
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getString("first_name") + " " + rs.getString("last_name"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void findEmployeeByFirstName(String name) {

        String sql = "SELECT * "
                + "FROM employees e "
                + "WHERE e.first_name =?";

        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                PreparedStatement pstm = connection.prepareStatement(sql);) {
            
            pstm.setString(1, name);
            
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getString("first_name") + " " + rs.getString("last_name"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
