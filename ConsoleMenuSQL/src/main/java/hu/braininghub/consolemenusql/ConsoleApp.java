package hu.braininghub.consolemenusql;

import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author andra
 */
public class ConsoleApp {

    private static final String STOP = "EXIT";

    public static void main(String[] args) {
        HrDao dao = new HrDao();

        try (Scanner sc = new Scanner(System.in)) {
            String line;
            do {
                line = sc.nextLine();

                switch (line) {
                    case "1":
                        dao.findEmployeeWthMaxSalary();
                        break;
                    case "2":
                        dao.findMaxEmployeeDepartment();
                        break;
                    case "3":
                        dao.findEmployeesEarnOverAverage();
                        break;
                    case "4":
                        dao.findEmployeesHiredAfter19910101();
                        break;
                    case "5":
                        dao.findJobTitlesWithClerk();
                        break;
                    case "6":
                        System.out.println("UNSAFE: Give me an ID:");
                        String id = sc.nextLine();
                        dao.findEmployeeByIDUnsafe(id);
                        break;
                    case "7":
                        System.out.println("SAFE: Give me a first name:");
                        String id2 = sc.nextLine();
                        dao.findEmployeeByFirstName(id2);
                        break;

                }
            } while (!STOP.equalsIgnoreCase(line));
        }

    }

}
