/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.junit5example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 *
 * @author andra
 */
class CalculatorTest {
    

    @Test
    void testAdd(){
    //Given
    int op1 = 10;
    int op2 = 15;
    Calculator underTest = new Calculator(op1, op2);
    
    
    //When
    int result = underTest.add();
    
    //Then
    Assertions.assertEquals(op1+op2+1, result, () -> "wrong addition");
    
    }
    
}
