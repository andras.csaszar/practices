/*
 Adott két osztály: Szerző (név: String, születési dátum: String, könyvek: Array) és 
 Könyv (kiadás dátuma: int, cím: String, kiadó: String). Egy Szerzőhöz több Könyv is 
 tartozhat, de egy Könyv csak egy Szerzőhöz tartozik.
 A Szerzőhöz képesek vagyunk új Könyveket hozzáadni.
 Szerzőt létre tudunk hozni név alapján (ebben az esetben a születési dátum alapértelmezetten
 1971.01.01) és létre tudunk hozni név és születési dátum megadásával is.
 Játsszuk el (mainben), hogy van 2 Szerző, mindegyik Szerző rendelkezik valahány Könyvvel. Listázzuk ki

 */
package tizoopfeladat1;

/**
 *
 * @author andra
 */
public class Author {

    private String name;
    private String dateOfBirth;
    private Books[] books;
    private int numberOfBooks;

    public Author(String name, String dateOfBirth) {
        this(name);
        this.setDateOfBirth(dateOfBirth);

    }

    public Author(String name) {
        books = new Books[5];
        this.name = name;
        this.dateOfBirth = "1971.01.01";
    }

    public void addBook(Books book) {
        // increaseMaximumNumberOfBooks

        if (numberOfBooks == books.length) {
            increaseMaximumNumberOFBooks();
        }
        books[numberOfBooks] = book;
        numberOfBooks++;

    }

    private void increaseMaximumNumberOFBooks() {
        Books[] books2 = new Books[(numberOfBooks+1) * 2];

        for (int i = 0; i < books.length; i++) {
            books2[i] = books[i];
        }
        books = books2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;

    }

    public String getDateOfBirth() {
        return this.dateOfBirth;

    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
    
    public void printAuthorsWithBooks(){
        for (int i = 0; i < numberOfBooks; i++) {
            System.out.println(name + "'s book: " + books[i].getTitle());
        }
    }

}
