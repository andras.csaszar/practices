/*
Adott két osztály: Szerző (név: String, születési dátum: String, könyvek: Array) és 
Könyv (kiadás dátuma: int, cím: String, kiadó: String). Egy Szerzőhöz több Könyv is 
tartozhat, de egy Könyv csak egy Szerzőhöz tartozik.
A Szerzőhöz képesek vagyunk új Könyveket hozzáadni.
Szerzőt létre tudunk hozni név alapján (ebben az esetben a születési dátum alapértelmezetten
1971.01.01) és létre tudunk hozni név és születési dátum megadásával is.
Játsszuk el (mainben), hogy van 2 Szerző, mindegyik Szerző rendelkezik valahány Könyvvel. Listázzuk ki

 */
package tizoopfeladat1;

/**
 *
 * @author andra
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Author aut1 = new Author("Artúr");
        Author aut2 = new Author ("Rek úr");
        Books book1 = new Books(1985, "May", "ToPublish");
        Books book2 = new Books(1990, "June", "Pubber");
        Books book3 = new Books(2005, "July", "PaperIssue");
        aut1.addBook(book1);
        aut2.addBook(book2);
        aut2.addBook(book3);

        aut1.printAuthorsWithBooks();
        aut2.printAuthorsWithBooks();
        
        
       
        
    }
    
}
