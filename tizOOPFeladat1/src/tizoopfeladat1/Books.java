/*
 Adott két osztály: Szerző (név: String, születési dátum: String, könyvek: Array) és 
 Könyv (kiadás dátuma: int, cím: String, kiadó: String). Egy Szerzőhöz több Könyv is 
 tartozhat, de egy Könyv csak egy Szerzőhöz tartozik.
 A Szerzőhöz képesek vagyunk új Könyveket hozzáadni.
 Szerzőt létre tudunk hozni név alapján (ebben az esetben a születési dátum alapértelmezetten
 1971.01.01) és létre tudunk hozni név és születési dátum megadásával is.
 Játsszuk el (mainben), hogy van 2 Szerző, mindegyik Szerző rendelkezik valahány Könyvvel. Listázzuk ki

 */
package tizoopfeladat1;

/**
 *
 * @author andra
 */
public class Books {

    private int publicationDate;
    private String title;
    private String publisher;
//    private String author;

    public Books(int publicationDate, String title, String publisher) {
//        this.author = getAuthor();
        setPublicationDate(publicationDate);
        setTitle(title);
        setPublisher(publisher);
        
    }
    
    
    

    public int getPublicationDate() {
        return this.publicationDate;
    }

    public void setPublicationDate(int publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublisher() {
        return this.publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

//    public String getAuthor() {
//        return this.author;
//    }

//    public void setAuthor(String author) {
//        this.author = author;
//    }

}
