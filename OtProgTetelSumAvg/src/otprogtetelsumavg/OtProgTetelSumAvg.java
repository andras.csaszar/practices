/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otprogtetelsumavg;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class OtProgTetelSumAvg {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Scanner sc = new Scanner(System.in);
        int[] numbers = new int[10];

        System.out.println("Adjon meg 10 pozitív számot szóközzel elválasztva");

        for (int i = 0; i < 10; i++) {
            if (sc.hasNextInt()) {
                int n = sc.nextInt();
                if (n > 0) {
                    numbers[i] = n;
                } else {
                    i--;
                }

            } else {
                sc.next();
                i--;
            }

        }
        
        System.out.println("-------------");
        int countDivide3 = 0;
        int count =0;
        int sum=0;
        double avg=0;
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
            if (numbers[i] % 3 != 0) {
                countDivide3++;
            }
            count++;
            sum +=numbers[i];

        }
        avg = sum / (double)count;
        System.out.printf("A számok átlaga: %f\n", avg);

    }

}
