/*
 Egy cégnél vannak Dolgozók (név: String, rendszer id: String, könyvelési egységek: Array), 
 minden Dolgozó képes Könyveléseket (id: String, ország: String, számlák: Array) kezelni. Minden Könyvelési egységhez
 tartozhatnak Számlák (összeg: int, sorszám: String).
 A dolgozókat létre tudjuk hozni Könyvelési egységek megadásával. Minden dolgozó pontosan kettő
 Könyvelési egységet kezel.
 A Könyvelési egységeket id és ország alapján tudjuk létrehozni. A könyvelési egységek
 egy tömbben maximum 10 darab számlát tárolnak.
 A Számla kötelező eleme a sorszám. Az összeg is megadható létrehozáskor, de ha nem
 adjuk meg, akkor 1000 USD értékű a számla.
 A Dolgozó képes az összes hozzátartozó számla 10%-os növelésére vagy csökkentésére
 (ez legyen 2 külön metódus).
 A dolgozó képes kilistázni a hozzátartozó számlákat (sorszám - összeg).
 Példányosítsuk a létrehozott osztályokat, írjuk ki a számlákat (sorszám - összeg).
 Majd növeljük meg minden számla értékét. Írjuk ki az új értékeket.
 */
package tizoopfeladat2;

/**
 *
 * @author andra
 */
public class Employee {

    private String name;
    private String sysID;
    private BookingEntity[] bookingEntities;
    private final int numberOfBookingEntities = 2;

//    public Employee(BookingEntity bookingEntity) {
//        
//        bookingEntities = new BookingEntity[numberOfBookingEntities];
//        
//    }
    public Employee(String name, String sysID, BookingEntity bookingEntity) {
        bookingEntities = new BookingEntity[numberOfBookingEntities];
        setEmployeeName(name);
        setSysID(sysID);

    }

    public void addInvoiceBookingToBookingEntity(Invoice inv, BookingEntity bookEnt) {

        if (bookEnt.getInvoices().length == bookEnt.getMaxNumberOfInvoices()) {
            bookEnt.setMaxNumberOfInvoices(bookEnt.getMaxNumberOfInvoices() + 1);
        }
        bookEnt.addInvoices(inv);
    }

    public void increaseNumberOfInvoices() {

        for (BookingEntity bkngEnts : bookingEntities) {
//            if (bkngEnts != null) {
                bkngEnts.setMaxNumberOfInvoices(bkngEnts.getMaxNumberOfInvoices() * 1.1);
//            }
        }

    }

    public void decreaseNumberOfInvoices() {

        for (BookingEntity bkngEnts : bookingEntities) {
            if (bkngEnts != null) {
                bkngEnts.setMaxNumberOfInvoices(bkngEnts.getMaxNumberOfInvoices() * 0.9);
            }
        }

    }

    public void listInvoicesOwned() {

        for (BookingEntity bookEnt : bookingEntities) {
//            if (bookEnt != null) {
                bookEnt.listInvoices();
            }
//        }

    }

    public String getEmployeeName() {
        return this.name;

    }

    public void setEmployeeName(String name) {
        this.name = name;
    }

    public String getSysID() {
        return this.sysID;
    }

    public void setSysID(String sysID) {
        this.sysID = sysID;
    }

}
