/*
 Egy cégnél vannak Dolgozók (név: String, rendszer id: String, könyvelési egységek: Array), 
 minden Dolgozó képes Könyveléseket (id: String, ország: String, számlák: Array) kezelni. Minden Könyvelési egységhez
 tartozhatnak Számlák (összeg: int, sorszám: String).
 A dolgozókat létre tudjuk hozni Könyvelési egységek megadásával. Minden dolgozó pontosan kettő
 Könyvelési egységet kezel.
 A Könyvelési egységeket id és ország alapján tudjuk létrehozni. A könyvelési egységek
 egy tömbben maximum 10 darab számlát tárolnak.
 A Számla kötelező eleme a sorszám. Az összeg is megadható létrehozáskor, de ha nem
 adjuk meg, akkor 1000 USD értékű a számla.
 A Dolgozó képes az összes hozzátartozó számla 10%-os növelésére vagy csökkentésére
 (ez legyen 2 külön metódus).
 A dolgozó képes kilistázni a hozzátartozó számlákat (sorszám - összeg).
 Példányosítsuk a létrehozott osztályokat, írjuk ki a számlákat (sorszám - összeg).
 Majd növeljük meg minden számla értékét. Írjuk ki az új értékeket.
 */
package tizoopfeladat2;

/**
 *
 * @author andra
 */
public class Invoice {

    private int amount;
    private String invNumber;

    public Invoice(String invNumber) {
    this(invNumber,1000);
    }

    public Invoice(String invNumber, int amount) {
       // this.invNumber=invNumber;
        setInvNumber(invNumber);
        setAmount(amount);
    }

    public String getInvNumber() {
        return this.invNumber;
    }

    public void setInvNumber(String invNumber) {
        this.invNumber = invNumber;
    }

    public int getAmount() {
        return this.amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
