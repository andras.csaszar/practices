/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tizoopfeladat2;

/**
 *
 * @author andra
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        
        Invoice inv1 = new Invoice("Inv-1", 15_000);
        Invoice inv2 = new Invoice("Inv-2");
        
        BookingEntity bkEnt = new BookingEntity("First", "HU");
        Employee emp1 = new Employee("József","tN4",bkEnt);
        emp1.addInvoiceBookingToBookingEntity(inv2, bkEnt);
        emp1.addInvoiceBookingToBookingEntity(inv1, bkEnt);
        bkEnt.addInvoices(inv1);
        bkEnt.addInvoices(inv2);
        
        emp1.listInvoicesOwned();
        
        emp1.increaseNumberOfInvoices();
        emp1.listInvoicesOwned();
        
        emp1.decreaseNumberOfInvoices();
        emp1.listInvoicesOwned();
        

    }

}
