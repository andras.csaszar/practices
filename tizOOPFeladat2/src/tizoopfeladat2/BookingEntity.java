/*
 Egy cégnél vannak Dolgozók (név: String, rendszer id: String, könyvelési egységek: Array), 
 minden Dolgozó képes Könyveléseket (id: String, ország: String, számlák: Array) kezelni. Minden Könyvelési egységhez
 tartozhatnak Számlák (összeg: int, sorszám: String).
 A dolgozókat létre tudjuk hozni Könyvelési egységek megadásával. Minden dolgozó pontosan kettő
 Könyvelési egységet kezel.
 A Könyvelési egységeket id és ország alapján tudjuk létrehozni. A könyvelési egységek
 egy tömbben maximum 10 darab számlát tárolnak.
 A Számla kötelező eleme a sorszám. Az összeg is megadható létrehozáskor, de ha nem
 adjuk meg, akkor 1000 USD értékű a számla.
 A Dolgozó képes az összes hozzátartozó számla 10%-os növelésére vagy csökkentésére
 (ez legyen 2 külön metódus).
 A dolgozó képes kilistázni a hozzátartozó számlákat (sorszám - összeg).
 Példányosítsuk a létrehozott osztályokat, írjuk ki a számlákat (sorszám - összeg).
 Majd növeljük meg minden számla értékét. Írjuk ki az új értékeket.
 */
package tizoopfeladat2;

/**
 *
 * @author andra
 */
public class BookingEntity {

    private String id;
    private String country;
    private Invoice[] invoices;
    private double maxNumberOfInvoices = 10.0;
    private int invoiceCounter = 0;

    public BookingEntity(String id, String country) {
        invoices = new Invoice[(int) maxNumberOfInvoices + 1];
        setID(id);
        setCountry(country);
    }

    public String getID() {
        return this.id;
    }

    public Invoice[] getInvoices() {
        return invoices;
    }

    public void addInvoices(Invoice invoice) {
        invoices[invoiceCounter] = invoice;
        invoiceCounter++;
    }

    public void setID(String id) {
        this.id = id;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getMaxNumberOfInvoices() {
        return maxNumberOfInvoices;
    }

    public void setMaxNumberOfInvoices(double maxNumberOfInvoices) {
        this.maxNumberOfInvoices = maxNumberOfInvoices;

    }

    public void listInvoices() {
        for (Invoice inv : invoices) {
//            if (inv != null) {
                System.out.println(inv.getInvNumber() + " " + inv.getAmount());
//            }
        }
    }
}
