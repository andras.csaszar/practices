/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h6f7arraymemberdividers;

/**
 *
 * @author andra
 */
public class H6F7ArrayMemberDividers {

    /**
     * @param args the command line arguments
     */
    static int generateRandomInt(int to) {

        return (int) (Math.random() * to);
    }

    /**
     *
     * @param num1 is divider AND
     * @param num2 is not a divider of elements of the array given as parameter
     * @return
     */
    static int getNumberOfDividers(int[] arr, int num1, int num2) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % num1 == 0 && arr[i] % num2 != 0) {
                count++;
               // System.out.println(arr[i]+"--");
            }
        }
        return count;
    }

    public static void main(String[] args) {
        // TODO code application logic here

        int sizeOfArray = 10;
        int numbersTo = 90;
        int divider = 3;
        int nonDivider = 5;
        int[] array = new int[sizeOfArray];
        
        for (int i = 0; i < array.length; i++) {
            array[i] = generateRandomInt(numbersTo);
            System.out.println(array[i]);
        }

        System.out.printf("There are %d number of numbers in the array of which 3 is a divider AND 5 is not.\n", getNumberOfDividers(array, divider, nonDivider));
    }

}
