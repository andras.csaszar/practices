/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this characterAsNumlate file, choose Tools | Templates
 * and open the characterAsNumlate in the editor.
 */
package h6f5methodcharlowertoupper;

import java.util.Scanner;
//9. Feladat
//Írj egy függvényt, ami a paraméterként átvett kis betűnek megfelelő
// nagybetűt adja vissza!

/**
 *
 * @author andra
 */
public class H6F5MethodCharLowerToUpper {

    final static Scanner SCANNER = new Scanner(System.in, "cp1250");
    static int characterAsNum;

    static char charLowerToUpper(int charac) {
        int characterAsNumDiff;  //97-65  //ASCII A - 65; a - 97  //
        //windows eastern eu 1250   0061 -007A
        if (charac == 369 || charac == 337) {
            characterAsNumDiff = 1;
        } else {
            characterAsNumDiff = 32;
        }

        int oldChar = charac;
        int newChar = charac - characterAsNumDiff;
        char result = (char) newChar;

        System.out.println(result);
        return result;

        //250 -> 218 = 32 diff
        //225 -> 193 = 32 diff
    }

    static void swallowNonChar() {

        SCANNER.next();
    }

    static int getCharFromUser() {

        do {
            System.out.println("Give me a lower case character, I give you it's capital case char.");
            if (SCANNER.hasNext()) {

                //String ast = "\'"+SCANNER.next().charAt(0)+"\'";
                
                characterAsNum = (int) (SCANNER.next().charAt(0));
                boolean isLetter = (
                        characterAsNum >= 97 && characterAsNum <= 122) ||
                        characterAsNum == 225 /*á*/ ||  /*-->193*/
                        characterAsNum == 233 /*é*/ ||  /*-->201*/
                        characterAsNum == 237 /*í*/ ||  /*-->205*/
                        characterAsNum == 243 /*ó*/ ||  /*-->211*/
                        characterAsNum == 337 /*ő*/ ||  /*-->...*/
                        characterAsNum == 246 /*ö*/ ||  /*-->...*/
                        characterAsNum == 250 /*ú*/ ||  /*-->...*/
                        characterAsNum == 369 /*ű*/ ||  /*-->...*/
                        characterAsNum == 252 /*ü*/;    /*-->...*/

                if (isLetter) {

                    return characterAsNum;
                }
            } else {
                swallowNonChar();
            }

        } while (true);

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        charLowerToUpper(getCharFromUser());
    }

}
