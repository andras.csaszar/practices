/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package immutableperson;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.Objects;

/**
 *
 * @author andra
 */
public final class ImmutableParent implements Comparable<ImmutableParent> {

    private final String name;
    private final int age;
    private final List<Child> children;

    public ImmutableParent(String name, int age, List<Child> children) {
        this.name = name;
        this.age = age;
        this.children = new ArrayList<>();
        copyChildElements(children);
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public List<Child> getChildren() {
        return Collections.unmodifiableList(children);
    }

    private void copyChildElements(List<Child> children) {
        for (Child c : children) {
            Child newChild = new Child(c.getAge(), c.getName());
            this.children.add(newChild);
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.name);
        hash = 97 * hash + this.age;
        hash = 97 * hash + Objects.hashCode(this.children);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ImmutableParent other = (ImmutableParent) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (this.age != other.age) {
            return false;
        }
        if (!Objects.equals(this.children, other.children)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ImmutableParent{" + "name=" + name + ", age=" + age + ", children=" + children + '}';
    }

    @Override
    public int compareTo(ImmutableParent p) {
        int nameComp = this.name.compareTo(p.name);
        if (nameComp == 0) {
            return this.age - p.age;
        }
        return nameComp;
    }

}
