/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package immutableperson;

import java.util.Comparator;

/**
 *
 * @author andra
 */
public class ChildAgeComparator implements Comparator<Child> {

    @Override
    public int compare(Child c1, Child c2) {
        return c1.getAge()-c2.getAge();
    }

}
