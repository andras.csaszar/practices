/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package immutableperson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author andra
 */
public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<Child> children = new ArrayList<>();
        children.add(new Child(10, "Jancsi"));
        children.add(new Child(9, "Julcsi"));

        ImmutableParent parent1 = new ImmutableParent("Anna", 30, children);
        ImmutableParent parent2 = new ImmutableParent("Bela", 30, children);
        ImmutableParent parent3 = new ImmutableParent("AnnaBelle", 40, children);

        // parent.getChildren().add(new Child(15,"Bela"));
        Set<ImmutableParent> parents = new TreeSet<>();
        parents.add(parent1);
        parents.add(parent2);
        parents.add(parent3);

        for (ImmutableParent p : parents) {
            System.out.println(p);
        }

        Child c1 = new Child(10, "Jancsi");
        Child c2 = new Child(10, "Juliska");
        Child c3 = new Child(5, "Bela");

        Set<Child> childrenTreeSetByAge = new TreeSet<>(new ChildAgeComparator());
        childrenTreeSetByAge.add(c1);
        childrenTreeSetByAge.add(c2);
        childrenTreeSetByAge.add(c3);

        for (Child child : childrenTreeSetByAge) {
            System.out.println(child);
        }

        System.out.println("***********************");

        Set<Child> childrenTreeSetByName = new TreeSet<>(new ChildNameComparator());
        childrenTreeSetByName.add(c1);
        childrenTreeSetByName.add(c2);
        childrenTreeSetByName.add(c3);

        for (Child child : childrenTreeSetByName) {
            System.out.println(child);
        }

        //Keressük meg a legidősebb gyereket
        Set<Child> kids = new HashSet<>();
        kids.add(c1);
        kids.add(c2);
        kids.add(c3);

        System.out.println("***********************");
        Child oldest = Collections.max(kids, new ChildAgeComparator());

        System.out.println(oldest + "A legidősebb");

        System.out.println("The oldest has been removed: ");
        //Töröljük ki a legidősebb gyereket
        Iterator<Child> ic = kids.iterator();
        while (ic.hasNext()) {
            Child child = ic.next();
            if (child.equals(oldest)) {
                ic.remove();
            }
        }

        for (Child kid : kids) {
            System.out.println(kid);
        }
    }

}
