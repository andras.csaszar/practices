/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;

import hero.AbstractHero;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author andra
 */
public class Ship {

    private static final int MAX_SIZE = 4;
    private final List<AbstractHero> heroes = new ArrayList<>();

    public void add(AbstractHero hero) {
        if (isEmptySeat()) {
            heroes.add(hero);
        }
    }

    public boolean isEmptySeat() {
        return heroes.size() < MAX_SIZE;
    }

    @Override
    public String toString() {
        return "Ship{" + "heroes=" + heroes + '}';
    }
    
}
