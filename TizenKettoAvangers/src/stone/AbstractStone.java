/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stone;

/**
 *
 * @author andra
 */
public abstract class AbstractStone {

    private static final int MIN_POWER = 2;
    private static final int MAX_POWER =10;

    private final String color;
    private final int power;

    public AbstractStone(String color) {
        this.color = color;
        this.power = generatePower();
    }

    public String getColor() {
        return color;
    }

    public int getPower() {
        return power;
    }

    @Override
    public String toString() {
        return "AbstractStone{" + "color=" + color + ", power=" + power + '}';
    }
    
    private int generatePower(){
    return (int)Math.random()*(MAX_POWER - MIN_POWER)+MIN_POWER;
    }
    

}
