/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hero;

/**
 *
 * @author andra
 */
public class IdentityCard {

    private final int number;

    public IdentityCard() {
        number = (int)(Math.random() * 10_000 + 10_000);
    }

    public int getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return "IdentityCard{" + "number=" + number + '}';
    }

}
