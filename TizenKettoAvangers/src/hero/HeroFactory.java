/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hero;

import stone.AbstractStone;
import stone.Time;

/**
 *
 * @author andra
 */
public class HeroFactory {
////név;erő;kő;földi-e
//Captain America;3.5;Soul Stone;0
//Hulk, 9, Soul Stone;1

    private static final String EARTH = "0";
    private static final String NO_EARTH = "1";

    public static AbstractHero create(String[] parameters) {

        String name = parameters[0];
        int power = Integer.parseInt(parameters[1]);
        // a stone- kezelését szerdán teljesítjük, addig úgy vesszük, hogy mindenki
        // az időkövet kapja

        AbstractStone stone = new Time();

        if (EARTH.equals(parameters[3])) {
            return new BornOnEarth(name, power, stone, new IdentityCard());
        } else {
            return new NotBornOnEarth(name, power, stone);
        }

    }
}
