/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bag;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author andra
 */


public class Bag<T> extends AbstractCollection<T>{

    private List<T> items = new ArrayList<>();

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    @Override
    public Iterator<T> iterator() {
        return items.iterator();
         }

    @Override
    public int size() {
       return items.size();
    }

    @Override
    public boolean remove(Object o) {
        return super.remove(o); 
    }
    @Override
    public boolean add(T e) {
        return super.add(e); 
    }

    
   
    

}
