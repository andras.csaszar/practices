/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bag;

/**
 *
 * @author andra
 */
public class BagApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Bag<String> bg = new Bag<>();

        bg.add("roten");
        bg.add("append");
        bg.add("harmadik");

        System.out.println(bg);

        Bag<Number> bgNum = new Bag<>();

        bgNum.add(4);
        bgNum.add(78.0);
        bgNum.add(456L);

        System.out.println(bgNum);

    }

}
