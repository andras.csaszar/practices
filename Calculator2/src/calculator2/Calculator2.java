/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator2;

import controller.Controller;
import modell.Model;
import view.View;

/**
 *
 * @author andra
 */
public class Calculator2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        View v = new View();
        Model m = new Model();
        Controller c = new Controller(v,m);

        v.showWindow();
    }

}
