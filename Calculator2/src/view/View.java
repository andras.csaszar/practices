/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import modell.Operator;

/**
 *
 * @author andra
 */
public class View extends JFrame {

    private Controller controller;
    private JTextField jt = new JTextField(30);

    public void setController(Controller controller) {
        this.controller = controller;
    }

    public void showWindow() {
        add(buildNorth(), BorderLayout.NORTH);
        add(buildCenter(), BorderLayout.CENTER);
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

    }

    public void setText(String expr) {
        jt.setText(expr);
    }

    private JPanel buildNorth() {
        jt.setEditable(false);
        JPanel jp = new JPanel();
        jp.add(jt);
        return jp;
    }

    private JPanel buildCenter() {
        JPanel jp = new JPanel();
        jp.setLayout(new GridLayout(4, 3));

        JButton jb0 = new JButton(String.valueOf(0));
        jp.add(jb0);
        jb0.addActionListener(l -> {
            JButton button = (JButton) l.getSource();
            int nr = Integer.parseInt(button.getText());
            controller.setNumber(nr);
        });

        int value = 1;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                JButton jb = new JButton(String.valueOf(value++));
                jp.add(jb);

                jb.addActionListener(l -> {
                    JButton button = (JButton) l.getSource(); 
                    //mivel ezt az action listener-t adtuk meg több gombnak is
                    //itt vissza kellett kapnunk, hogy melyik gomb-ból jöttünk ide
                    //tudjuk hogy gomb, ezért cast-oltuk is
                    //ennek aztán elkérjük a text-jét, ami itt a szám
                    //aztán ezt használjuk fel
                    
                    int nr = Integer.parseInt(button.getText());

                    controller.setNumber(nr);
                });

            }
        }

        addOperators(jp);

        return jp;
    }

    private void addOperators(JPanel jp) {
        JButton plus = new JButton("+");
        JButton subtract = new JButton("-");
        JButton multiply = new JButton("*");
        JButton equal = new JButton("=");

        plus.addActionListener(l -> controller.handleOperator(Operator.ADD));
        subtract.addActionListener(l -> controller.handleOperator(Operator.SUBTRACT));
        multiply.addActionListener(l -> controller.handleOperator(Operator.MULTIPLY));
        equal.addActionListener(l -> controller.handleOperator(Operator.EQ));

        jp.add(plus);
        jp.add(subtract);
        jp.add(multiply);
        jp.add(equal);

    }

}
