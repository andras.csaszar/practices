/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modell;

/**
 *
 * @author andra
 */
public enum Operator {

    ADD("+") {
                @Override
                public int calc(int a, int b) {
                    return a + b;
                }
            },
    SUBTRACT("-") {

                @Override
                public int calc(int a, int b) {
                    return a - b;
                }

            },
    MULTIPLY("*") {
                @Override
                public int calc(int a, int b) {
                    return a * b;
                }
            },
    EQ("=") {
                @Override
                public int calc(int a, int b) {
                    return 0;
                }
            };

    private final String character;

    private Operator(String character) {
        this.character = character;
    }

    public String getCharacter() {
        return character;
    }

    public abstract int calc(int a, int b);

}
