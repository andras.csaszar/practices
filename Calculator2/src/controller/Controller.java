/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import modell.Model;
import modell.Operator;
import view.View;

/**
 *
 * @author andra
 */
public class Controller {

    private View v;
    private Model m;

    public Controller(View v, Model m) {
        this.v = v;
        this.m = m;
        v.setController(this);
        m.setController(this);
    }

    public void setNumber(int number) {
        if (m.getOperator() == null) {
            m.setNumber1(number);
        } else {
            m.setNumber2(number);
        }

        m.setExpression(generateExpression());

    }

    public void handleOperator(Operator operator) {

        if (m.getNumber1() >= 0 && operator != Operator.EQ) {
            m.setOperator(operator);
            m.setExpression(generateExpression());
        } else if (m.getNumber1() >= 0 && m.getNumber2() >= 0 && m.getOperator() != null && operator == Operator.EQ) {
            reset();
        }
    }

    private void reset() {
        m.setExpression(generateExpression() + "=" + m.getOperator().calc(m.getNumber1(), m.getNumber2()));
        m.setNumber1(0);
        m.setNumber2(0);
        m.setOperator(null);
    }

    private String generateExpression() {
        StringBuilder sb = new StringBuilder(String.valueOf(m.getNumber1()));

        if (m.getOperator() != null) {
            sb.append(m.getOperator().getCharacter());
        }

        if (m.getOperator() != null && m.getNumber2() != 0) {
            sb.append(m.getNumber2());
        }
        return sb.toString();
    }

    public void notifyView() {
        v.setText(m.getExpression());

    }

}
