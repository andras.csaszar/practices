/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h6f6squarearea;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class H6F6SquareArea {

    final static Scanner SCANNER = new Scanner(System.in);

    static double getNumber() {

        do {
            System.out.println("Give me a number");
            if (SCANNER.hasNextDouble()) {
                double number = SCANNER.nextDouble();
                if (number > 0) {
                    return number;
                }
            } else {
                SCANNER.next();
            }
        } while (true);
    }

    static double getAreaOfSquare(double a, double b) {
        return a * b;

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        System.out.printf("Area of the square is %.2f.\n", getAreaOfSquare(getNumber(), getNumber()));
    }

}
