/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negyarraybehaviours;

/**
 *
 * @author andra
 */
public class NegyArrayBehaviours {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        System.out.println("Amikor karray = numbers és karray-ban változtatunk elemet");
        int [] numbers = {2, 3, 4};
        int [] karray = numbers;
        karray[1]++;
        
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(karray[i]+"-"+numbers[i]);
        }
        System.out.println("Amikor karray-ba bemásoljuk numbers-t elemenként for-ral");
        karray = new int[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            karray[i] = numbers[i];
        }
        karray[1]++;
        
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(karray[i] + "-" + numbers[i]);
        }
        
        
    }
    
}
