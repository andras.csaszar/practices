/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package whileexample;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class WhileExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        //byte b= 0;
        //while (true){System.out.println(b++);}  // végtelen ciklus
        Scanner sc = new Scanner (System.in);
        int i = 0;
        int osszeg = 0;
        while (i<5){
          int  n = sc.nextInt();
           osszeg +=n;
            i++;
        }
        System.out.println("N: " + osszeg);
    }
    
}
