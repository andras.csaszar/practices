/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tizenegyinheritance;

import Person.Person;
import student.Student;

/**
 *
 * @author andra
 */
public class TizenEgyInheritance {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Object o = new Person ("Bela");
        
        
        if(o instanceof Person){
        Person p = (Person) o;
            System.out.println(p.getName());
        
        }
        
        Object oo = new Student();
        Student s = new Student();
      
        
        System.out.println(oo);
        System.out.println(s);
        
    }
    
}
