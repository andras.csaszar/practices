/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package student;

import Person.Person;

/**
 *
 * @author andra
 */
public class Student extends Person {

    private String neptun;

    public Student(String neptun) {
        super("Császár András");
        this.neptun = neptun;
    }

    public Student() {
        this("aaer9h");
    }

    public void setNeptun(String neptun) {
        this.neptun = neptun;
    }

    public String getNeptun() {
        return neptun;
    }
    
    @Override
    public String toString(){
    return super.toString() +" - "  + neptun;
    }
}
