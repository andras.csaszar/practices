/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package a3f1;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class A3F1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //addig kérjünk be a felhasználótól számot, amíg nem 1-5-ig ad meg.

        Scanner sc = new Scanner(System.in);

        boolean asking = true;
        int n = -1;
        do {
            System.out.println("Adjon meg egy számot 1-5-ig, megmondom, mi a római szám párja");

            if (sc.hasNextInt()) {
                n = sc.nextInt();
                if (n >= 1 && n <= 5) {
                    asking = false;
                }
            } else {
                sc.next();
            }

            switch (n) {
                case 1:
                    System.out.println("I");
                    break;
                case 2:
                    System.out.println("II");
                    break;
                case 3:
                    System.out.println("III");
                    break;
                case 4:
                    System.out.println("IV");
                    break;
                case 5:
                    System.out.println("V");
                    break;

            }

        } while (asking);

    }

}
