/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h11f1patisserie;

import java.util.Set;

/**
 *
 * @author andra
 */
public class Cake extends Sweets {

    private Set<String> components;
    private static final int NUMBER_OF_CAKE_COMPONENTS = 5;

    public Cake(Set<String> components) {

        setComponents(components);

    }

    @Override
    public String wayOfServing() {
        return "By slices";
    }

    public Set<String> getComponents() {
        return components;
    }

    public void setComponents(Set<String> components) {
        if (components.size() > 0 && components.size() < 6) {
            this.components.addAll(components);
        }
    }

    @Override
    public String toString() {
        return "Cake{" + "components=" + components + '}';
    }

}
