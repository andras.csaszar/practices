/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h11f1patisserie;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author andra
 */
public class BirthdayCake extends Cake {

    private String birthdayWish;
    private Set<String> components;

    public BirthdayCake(String birthdayWish, Set<String> components) {

        super(components);

    }

    public String getBirthdayWish() {
        return birthdayWish;
    }

    public void setBirthdayWish(String birthdayWish) {
        this.birthdayWish = birthdayWish;
    }

    @Override
    public void setComponents(Set<String> components) {
        this.components = new HashSet<>();
            this.components.addAll(components);
    }

    @Override
    public String toString() {
        return "BirthdayCake{" + "birthdayWish=" + birthdayWish + ", components=" + components + '}';
    }

}
