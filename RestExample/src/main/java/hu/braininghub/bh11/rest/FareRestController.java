/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.rest;

import hu.braininghub.bh11.rest.dto.Fare;
import hu.braininghub.bh11.rest.dto.FareRequest;
import java.util.Arrays;
import javax.ejb.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author andra
 */
@Path("fare_service")
@Singleton
public class FareRestController {

    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response fares() {

        Fare fare = new Fare("BUD", "FRA", "LH", 1500);

        return Response.ok(Arrays.asList(fare)).build();
    }

    @POST
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response faresFiltered(FareRequest req) {

        Fare fare = new Fare(req.getOrigin(), req.getDestination(), "LH", 1500);
        /*  Fare fare1 = new Fare("FRA", "BUD", "AM", 3000);
    Fare fare2 = new Fare("CHA", "WAR", "MA", 2345);
    Fare fare3 = new Fare("BUD", "HOH", "AM", 1200);
         */
        return Response.ok(Arrays.asList(fare)).build();

    }

    @GET
    @Path("hello")
    @Produces(value = MediaType.TEXT_PLAIN)
    public String hello() {
        return "Hello World!";
    }
}
