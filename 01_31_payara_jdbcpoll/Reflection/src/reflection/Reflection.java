/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reflection;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andra
 */
public class Reflection {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Class c = "foo".getClass();
        Method[] strMethods = c.getDeclaredMethods();
        for (Method m : strMethods) {
            System.out.println("Looking at the Method " + m.getName());

            Class<?> parameterType[] = m.getParameterTypes();
            for (int i = 0; i < parameterType.length; i++) {
                System.out.println("\tParameter" + "" + (i + 1)
                        + "Parameter type: " + parameterType[i].getName());
            }
        }

        TestClass example = new TestClass();
        Class c1 = example.getClass();
        Field[] fields = c1.getDeclaredFields();
        for (Field f : fields) {
            System.out.println("\"" + f.getName() + "\"" + " is a "
                    + Modifier.toString(f.getModifiers()) + " field");
        }
        try {
            Field zap = c1.getDeclaredField("zap");
            zap.setAccessible(true);
            String whatsInZap = (String) zap.get(example);
            System.out.println("Information zap is holding: " + whatsInZap);

            zap.set(example, "asdfasdfadsf");
            whatsInZap = (String) zap.get(example);
            System.out.println("Inofrmation zap is holding after manipulation: " + whatsInZap);

        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            Logger.getLogger(Reflection.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            Class c2 = Class.forName("reflection.TestClass");
            Object o2 = c2.getDeclaredConstructor().newInstance();
            Method m2 = c2.getDeclaredMethod("bar", String.class);
            m2.setAccessible(true);
            m2.invoke(o2, "valamivalamivalami");
            

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Reflection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(Reflection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(Reflection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(Reflection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(Reflection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(Reflection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(Reflection.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
