package reflection;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author andra
 */


public final class TestClass {

    private int foobar = 42;
    private String zap = "Hehe, you will not see me ever";

    public int foo() {
        return 1;
    }

    private String bar(String xray) {
        System.out.println(xray);
        return xray;
       
    }

}
