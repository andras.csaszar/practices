/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threadpoolexamples;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author andra
 */
public class ThreadPoolExamples {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ExecutorService es = Executors.newFixedThreadPool(3);
        es.execute(() -> System.out.println("alma"));
        es.shutdown();
    }

}
