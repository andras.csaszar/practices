<%-- 
    Document   : empnames
    Created on : 2020. jan. 31., 20:54:59
    Author     : andra
--%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>


    </head>
    <body>
        <h1>Hello World!</h1>
        <div>
            <form method="post" action="EmployeeServlet">
                Search in names by typing and pushing Search:
                Name contains: <input type="text" name="part_of_name">
                <input type="submit" value="Search"/>
            </form>
        </div>
        First table:
        <table border="1">
            <%
                List<String> names = (List<String>) request.getAttribute("names");

                for (String s : names) {
                    out.print("<tr><td>" + s + "</td></tr>");
                }

            %>

        </table> 

        Second table:
        <table>
            <c:forEach var="name" items="${names}">
                <tr><td><c:out value="${name}"/></td></tr>  
            </c:forEach>
        </table>


    </body>
</html>
