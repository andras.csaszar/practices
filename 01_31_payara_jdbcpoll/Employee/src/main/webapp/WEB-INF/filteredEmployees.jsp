<%-- 
    Document   : filteredEmployees
    Created on : 2020. febr. 5., 19:22:55
    Author     : andra
--%>
<%@page import="hu.braininghub.bh11.employee.service.dto.EmployeesDto"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

        <script type="text/javascript">
            function setColorToRed(itemID) {
                var tableRow = document.getElementById(itemID);
                tableRow.style.backgroundColor = "red";
            }

            function changeColor() {
                document.getElementById("tr1").style.backgroundColor = "red";

            }
        </script>


    </head>
    <body>
        <h1>Filtered Employees</h1>
        <table>
            <%
                List<EmployeesDto> employees = (List<EmployeesDto>) request.getAttribute("employees");

                for (EmployeesDto e : employees) {
                    out.print("<tr><td>" + e.getFirstName() + "</td><td>"
                            + e.getLastName() + "</td><td>"+
                            e.getJobHistory().size() + "</td></tr>"
                    );
                }

            %>
        </table>

        <div>
            JSTL-el
            <table border ="1">
                <c:forEach var="emp" items="${employees}">
                    <tr id="tr1" <c:if test="${emp.salary > 10000}"> background-color="red" </c:if>>
                        <c:choose>
                            <c:when test="${emp.salary > 10000}">

                            <script>setColorToRed((this.id).toString());</script>
                        </c:when>
                        <c:otherwise>

                        </c:otherwise>

                    </c:choose>


                    <td><c:out value="${emp.firstName}"/></td>
                    <td><c:out value="${emp.lastName}"/></td>
                    <td><c:out value="${emp.salary}"/></td>
                    </tr>
                </c:forEach>
            </table>
        </div>

    </body>
</html>
