/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.employee.repository;

import hu.braininghub.bh11.employee.repository.entity.Employees;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andra
 */
@Singleton
public class EmployeeDao {

    @PersistenceContext
    private EntityManager em;

    @PostConstruct
    public void init() {
        System.out.println("It has been initialized.");
    }

    @PreDestroy
    public void destroy() {
        System.out.println("It has been destroyed.");
    }

    public List<Employees> getNames() {

//        List<Employees> employees = new ArrayList<>();
        return em.createQuery("SELECT e FROM Employees e ", Employees.class)
                .setMaxResults(10)
                .getResultList();

        /* try ( Statement stm = ds.getConnection().createStatement()) {
            String sql = "SELECT first_name, last_name, salary FROM employees LIMIT 10";
            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {
                employees.add(Employees.of(
                        rs.getString("first_name"),
                        rs.getString("last_name"),
                        rs.getInt("salary")));
            }

        }*/
//        return employees;
    }

    public List<Employees> findByName(String str) {
//
//        List<Employees> employees = new ArrayList<>();

        return em.createQuery("SELECT e FROM Employees e WHERE e.firstName LIKE :pattern", Employees.class)
                .setParameter("pattern", "%" + str + "%")
                .getResultList();

//        String sql = "SELECT first_name, last_name, salary FROM employees WHERE first_name LIKE ?";
//        try ( PreparedStatement pstm = ds.getConnection().prepareStatement(sql)) {
//            pstm.setString(1, "%" + str + "%");
//            ResultSet rs = pstm.executeQuery();
//            while (rs.next()) {
//                employees.add(Employees.of(
//                        rs.getString("first_name"),
//                        rs.getString("last_name"),
//                        rs.getInt("salary")));
//            }
//        }
//        return employees;
    }

}
