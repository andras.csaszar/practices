/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.employee.servlet;


import hu.braininghub.bh11.employee.service.EmployeeService;
import hu.braininghub.bh11.employee.service.dto.EmployeesDto;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author andra
 */
@WebServlet(name = "EmployeeServlet", urlPatterns = {"/EmployeeServlet"})
public class EmployeeServlet extends HttpServlet {

    @Inject
    private EmployeeService service;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<String> names;
//
//        if (request.getParameter("part_of_name") != null) {
//            String str = (String) request.getParameter("part_of_name");
//            names = service.getSearchedNames(str);
//        } else {
//            names = service.getNames();
//        }
        names = service.getNames();
        for (String s : names) {
            System.out.println(s);
        }
        request.setAttribute("names", names);
        request.getRequestDispatcher("/WEB-INF/empnames.jsp").forward(request, response);

//a forward mindig az adott alkalmazás-szerveren belül van:
        //a dispatcher-ben 
        //lehet servlet url is
        //vagy jsp
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<EmployeesDto> employees = service.getEmployees(request.getParameter("part_of_name"));
 

        request.setAttribute("employees", employees);
        request.getRequestDispatcher("/WEB-INF/filteredEmployees.jsp").forward(request, response);

    }

}
