/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.login;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author andra
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {

    private static final String ADMIN_USERNAME = "admin";
    private static final String ADMIN_PASSWORD = "admin";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (request.getSession().getAttribute("loggedin") != null && (boolean) request.getSession().getAttribute("loggedin")) {
            request.getRequestDispatcher("WEB-INF/welcome.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("WEB-INF/login.jsp").forward(request, response);
           
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getSession().setAttribute("belepve", "hollo");
        response.addCookie(new Cookie("test", "Elek"));

        if (ADMIN_USERNAME.equals(request.getParameter("login_name"))
                && ADMIN_PASSWORD.equals(request.getParameter("pass"))) {
            request.getSession().setAttribute("loggedin", true);
            request.getSession().setAttribute("login_name", request.getParameter("login_name"));
            request.getRequestDispatcher("WEB-INF/welcome.jsp").forward(request, response);
        } else {
            request.getSession().setAttribute("error_message", "Wrong Username or Password");
            request.getRequestDispatcher("WEB-INF/login.jsp").forward(request, response);
        }

    }

}
