<%-- 
    Document   : welcome
    Created on : 2020. febr. 7., 19:35:20
    Author     : andra
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Welcome, <%=session.getAttribute("login_name")%></h1>
        <p> <a href="LogoutController">Logout</a></p>
    </body>
</html>
