/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;

import hero.AbstractHero;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author czirjak_zoltan
 */
public class Ship {
    private static final int MAX_SIZE = 4;
    private static final int INDEX_IN_NAME = 1;

    private final List<AbstractHero> heroes = new ArrayList<>();

    public void add(AbstractHero hero) {
        if (isEmptySeat()) {
            heroes.add(hero);
        }
        
        orderBySecondCharacterOfName();
    }
    
    public boolean isEmptySeat() {
        return heroes.size() < MAX_SIZE;
    }

    @Override
    public String toString() {
        return "Ship{" + "heroes=" + heroes + '}';
    }
    
    private void orderBySecondCharacterOfName() {
        Comparator<AbstractHero> cmp = new Comparator<AbstractHero>() {
            public int compare(AbstractHero h1, AbstractHero h2) {
                return h1.getName().charAt(INDEX_IN_NAME) - 
                        h2.getName().charAt(INDEX_IN_NAME);
            }
        };
        
        Collections.sort(heroes, cmp);
    }
    
    private void orderBySecondCharacterOfName2() {        
        Collections.sort(heroes, new Comparator<AbstractHero>() {
            public int compare(AbstractHero h1, AbstractHero h2) {
                return h1.getName().charAt(INDEX_IN_NAME) - 
                        h2.getName().charAt(INDEX_IN_NAME);
            }
        });
    }
    
    private void orderBySecondCharacterOfName3() {        
        Collections.sort(heroes, (AbstractHero h1, AbstractHero h2) -> {
            return h1.getName().charAt(INDEX_IN_NAME) - h2.getName().charAt(INDEX_IN_NAME);
        });
    }
//Reporter házihoz
    public List<AbstractHero> getHeroes() {
        return heroes;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.heroes);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ship other = (Ship) obj;
        if (!Objects.equals(this.heroes, other.heroes)) {
            return false;
        }
        return true;
    }
    
  
    
}
