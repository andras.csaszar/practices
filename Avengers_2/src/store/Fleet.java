/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;

import hero.AbstractHero;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author czirjak_zoltan
 */
public class Fleet {

    private final List<Ship> ships = new ArrayList<>();

    public void add(AbstractHero hero) {
        findShip().add(hero);
    }

    @Override
    public String toString() {
        return "Fleet{" + "ships=" + ships + '}';
    }

    private Ship findLastShip() {
        if (ships.isEmpty()) {
            ships.add(new Ship());
        }

        return ships.get(ships.size() - 1);
    }

    private Ship findShip() {
        Ship ship = findLastShip();

        if (ship.isEmptySeat()) {
            return ship;

        } else {
            ships.add(new Ship());
            return ships.get(ships.size() - 1);
        }
    }
//Reporting házihoz
    public List<Ship> getShips() {
        return ships;
    }

}
