/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import hero.AbstractHero;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
import store.Fleet;

/**
 *
 * @author andra
 */
public class Loader {

    private Fleet fleet;
    private static final String HEROES_FILENAME = "HeroesSaved.txt";
    private static final String FLEET_FILENAME = "FleetSaved.txt";

    public Loader(Fleet fleet) {
        this.fleet = fleet;
    }

    public List<AbstractHero> deSerializeHeroes() {
        List<AbstractHero> heroList = null;

        try (FileInputStream fis = new FileInputStream(HEROES_FILENAME);
                ObjectInputStream ois = new ObjectInputStream(fis)) {
            heroList = (ArrayList<AbstractHero>) ois.readObject();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }

        return heroList;
    }

    public Fleet deSerializeFleet() {
        Fleet fleet = null;

        try (FileInputStream fis = new FileInputStream(FLEET_FILENAME);
                ObjectInputStream ois = new ObjectInputStream(fis)) {
            fleet = (Fleet) ois.readObject();

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return fleet;   
    }

}
