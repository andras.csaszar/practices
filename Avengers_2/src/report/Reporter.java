/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import hero.AbstractHero;
import hero.BornOnEarth;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import stone.StoneType;
import store.Fleet;
import store.Ship;

/**
 *
 * @author czirjak_zoltan
 */
public class Reporter {

    private Fleet fleet;

    public Reporter(Fleet fleet) {
        this.fleet = fleet;
    }

    public void generateReports(Fleet fleetGiven) {
        printCountOfStonesByShip();

        List<Ship> fleet = fleetGiven.getShips();

        //Report1
        //Hány földi születésű hős indul a harcba?
        long countBornOnEarth = fleet.stream()
                .map(s -> s.getHeroes())
                .filter(h -> h instanceof BornOnEarth)
                .count();
        System.out.println("Born on Earth: " + countBornOnEarth);

        //Report2
        //Ki rendelkezik a legnagyobb erővel?
        String maxPowerHero = fleet.stream()
                .map(s -> s.getHeroes())
                .flatMap(h -> h.stream())
                .sorted((p1, p2) -> p1.getPower() - p2.getPower())
                .findFirst().get().toString();
        System.out.println(maxPowerHero);

        //Report3
        //Listázzuk ki a felhasznált útlevelek sorszámát!
        fleet.stream()
                .map(s -> s.getHeroes())
                .flatMap(h -> h.stream())
                .filter(h -> h instanceof BornOnEarth)
                .map(h -> (BornOnEarth) h)
                .map(h -> h.getIdentityCard())
                .forEach(System.out::println);

    }

    private void printCountOfStonesByShip() {
        countOfStoneByShip().entrySet().stream()
                .forEach(i -> {
                    System.out.println("******* Ship: " + i);
                    i.getValue()
                    .forEach((j, k) -> System.out.println("Stone: " + j + "Count: " + k));

                });
    }

    //Hajónként melyik kőből mennyi van?
    private Map<StoneType, Integer> countOfStones(Ship ship) {
        return ship.getHeroes().stream()
                .map(AbstractHero::getStone) //List<Stone>
                .collect(Collectors.toMap(
                                k -> k, // az öszzegyűjtendő paraméterekből hogyan képezönk kulcsot
                                v -> 1,
                                (v1, v2) -> v1 + v2,
                                HashMap::new
                        )
                );

    }

    private Map<Ship, Map<StoneType, Integer>> countOfStoneByShip() {
        return fleet.getShips().stream()
                .collect(Collectors.toMap(
                                s -> s,
                                this::countOfStones, // v -> countOfStones(v)   
                                (x, y) -> x, // ha ütközik a két kulcs, tartsa meg a régit
                                HashMap::new
                        ));
    }

        //két csoport hajó
    //1. olyan hajók, amikben csak földi hősök vannak
    //2. olyan hajük, amikben nem csak földiek vannak
    private boolean containsOnlyBornOnEarth(Ship ship) {
        return ship.getHeroes().stream()
                .allMatch(a -> a instanceof BornOnEarth);
    }

    private Map<Boolean, List<Ship>> partitionByContainsOnlyBornOnEarth() {
        return fleet.getShips().stream()
                .collect(Collectors.partitioningBy(
                                this::containsOnlyBornOnEarth
                        ));
    }

    private long numberOfShipsContainingOnlyBornOnEarth(){
    return partitionByContainsOnlyBornOnEarth().get(Boolean.TRUE).size();
    
    }
    
}
