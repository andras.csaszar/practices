/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import hero.AbstractHero;
import hero.BornOnEarth;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import store.Fleet;

/**
 *
 * @author czirjak_zoltan
 */
public class Saver {

    //HF:
    //kiírja a hősöket egy szöveges file-ba
    private Fleet fleet;
    private static final String HEROES_FILENAME = "HeroesSaved.txt";
    private static final String FLEET_FILENAME = "FleetSaved.txt";

    public Saver(Fleet fleet) {
        this.fleet = fleet;
    }

    public void serializeFleet(Fleet fleet) {

        try (FileOutputStream fos = new FileOutputStream(FLEET_FILENAME);
                ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(fleet);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    private static List<AbstractHero> getHeroesFromFleet(Fleet fleet) {
        return fleet.getShips().stream()
                .flatMap(s -> s.getHeroes()
                        .stream())
                .collect(Collectors.toList());

    }

    public void serializeHeroes(Fleet fleet) {
        List<AbstractHero> heroesToSave = getHeroesFromFleet(fleet);

        try (FileOutputStream fos = new FileOutputStream("HeroesSaved.txt");
                ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(heroesToSave);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

}
