/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h5f4arraysumavgoccur;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class H5F4ArraySuMAvgOccur {

    static int randomGenerator(int min, int max) {
        int rnd = (int) (Math.random() * (max + 1 - min) + min);//todo
        return rnd;

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        /*int z;
         do{
         System.out.println(z=randomGenerator(0, 9));
         } while (z!=9);*/
        //50 elemű tömb feltöltése
        //tömb kiírása
        //számok összege és számtani középértéke
        // szám megadása, és kiírása, hogy a tömbben hányszor fordul elő
        int arraySize = 50;
        int[] array = new int[arraySize];
        int sum = 0;
        double avg = 0;
        for (int i = 0; i < array.length; i++) {

            array[i] = randomGenerator(0, 9);
            sum += array[i];
        }
        avg = sum / (double) arraySize;

        for (int i = 1; i < array.length+1; i++) {

            System.out.print(array[i-1] + " ");
            if (i % 5 == 0) {
                System.out.println();
            }
        }
        System.out.printf("A számok összeg: %d. A számok átlaga: %f\n", sum, avg);

        Scanner sc = new Scanner(System.in);

        System.out.println("Adjon meg egy számot 0 és 9 között, megmondom, hogy hányszor van meg a tömbben.");
        int number = -1;
        do {
            if (sc.hasNextInt()) {
                number = sc.nextInt();
                if (number > 9 || number < 0) {
                    System.out.println("0 és 9 közötti számot adjon meg!");
                }
            } else {
                sc.next();
                System.out.println("0 és 9 közötti számot adjon meg!");
            }

        } while (number > 9 || number < 0);//todo 11-re nem működik

        int foundCount = 0;
        for (int i = 0; i < array.length; i++) {
            if (number == array[i]) {
                foundCount++;
            }

        }

        System.out.printf("A megadott szám a véletlen tömbben %d alkalommal fordul elő.\n", foundCount);

    }

}
