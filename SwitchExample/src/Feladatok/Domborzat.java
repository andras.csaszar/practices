/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Feladatok;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class Domborzat {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        if (sc.hasNext()) {
            int number = sc.nextInt();

            if (number < 0) {
                System.out.println("Hibás bemenet!");
            } else if (number <= 200) {
                System.out.println("Alföld");
            } else if (number <= 500) {
                System.out.println("Dombság");
            } else if (number <= 1500) {
                System.out.println("Középhegység");
            } else {
                System.out.println("Hegység");
            }
        }
    }
}
