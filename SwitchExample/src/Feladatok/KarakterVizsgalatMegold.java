/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Feladatok;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class KarakterVizsgalatMegold {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        char c = scanner.next().charAt(0);

        if (c <= 'z' && c >= 'a') {
            System.out.println("Kisbetű");
        }
        if (c <= 'Z' && c >= 'A') {
            System.out.println("Nagybetű");
        }
        if (c <= '0' && c >= '9') {
            System.out.println("Szám");
        }

    }
}
