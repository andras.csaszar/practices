/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Feladatok;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class EgyenloSzaruHaromszog {

    public static void main(String[] args) {
        System.out.println("Adjon meg 3 szakaszhosszt!");

        Scanner sc = new Scanner(System.in);
        System.out.println("a=");
        int n1 = sc.nextInt();
        System.out.println("b=");
        int n2 = sc.nextInt();
        System.out.println("c=");
        int n3 = sc.nextInt();

        if ((n1 + n2) > n3 && (n1 + n3) > n2 && (n2 + n3) > n1) {
            //háromszög lehet
            System.out.println("Három szög lehet, de/és...");
            if (n1 == n2 || n1 == n3 || n2 == n3) {
                System.out.println("Egynlőszárú háromszög lehet.");
            } else {
                System.out.println("Nem lehet egyenlőszárú háromszög");
            }

        } else {
            System.out.println("Nem lehet háromszög.");
        }

    }
}
