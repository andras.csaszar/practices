/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Feladatok;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class Kategorizalas {

    public static void main(String[] args) {
        System.out.println("Adjon meg egy számot 0-tól végtelenig!");
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        String uzenet = "";
        if (number < 0) {
            uzenet = "hiba, negatív szám";
        } else if (number < 201) {
            uzenet = "Alföld";
        } else if (number < 501) {
            uzenet = "Dombság";
        } else if (number < 1501) {
            uzenet = "Középhegység";
        } else {
            uzenet = "Hegység";
        }
        System.out.println(uzenet);
    }

}
