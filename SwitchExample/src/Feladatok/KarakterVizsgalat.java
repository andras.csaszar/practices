/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Feladatok;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class KarakterVizsgalat {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Adjon meg egy karaktert, és megmondom, milyen típus.");
        if(sc.next().length()>1){System.out.println("EGY karaktert kellett volna megadni. Futtassa újra!");} else{
        char c = sc.next().charAt(0);
        int cNum = c;
        //48-57 szám 0-9 között
        //65-90 nagybetű
        //97-122 kisbetű
        if (c < 58) {
            System.out.println("Szám 0-9 között.");
        } else if (c < 91) {
            System.out.println("Nagybetű.");
        } else if (c < 123) {
            System.out.println("Kisbetű.");
        } else {
            System.out.println("Egyik sem.");
        }

    }}
}
