/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package switchexample;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class SzamoloGepSwitch {

    public static void main(String[] args) {
        //II.
        Scanner sc = new Scanner(System.in);
        System.out.println("Adjon meg egy számot 2,147,483,648 és 2,147,483,647 között!");
        int n1 = sc.nextInt();
        System.out.println("Adja meg az elvégezni kívánt műveletet (a megfelelő jellel)");
        String c = sc.next();
        System.out.println("Adjon meg még egy számot 2,147,483,648 és 2,147,483,647 között");
        int n2 = sc.nextInt();
        double eredmeny = 0.0;
        int osszeadas = n1 + n2;
        int kivonas = n1 - n2;
        int szorzas = n1 * n2;
        double osztas = n2 != 0 ? (double) n1 / n2 : 0;
        int maradekos = n2 != 0 ? n1 % n2 : 0;
        String uzenet = "";

        switch (c) {
            case "+":
                eredmeny = osszeadas;
                break;
            case "-":
                eredmeny = kivonas;
                break;
            case "*":
                eredmeny = szorzas;
                break;
            /*case "/": if(n2==0){uzenet = "Nullával nem lehet osztani";} else{ eredmeny = osztas;} break;*/
            case "/":
                switch (n2) {
                    case 0:
                        uzenet = "Nullával nem lehet osztani";
                        break;
                    default:
                        eredmeny = osztas;
                } break;
            /*case "%": if(n2==0){uzenet = "Nullával nem lehet maradekosan osztani";} else{ eredmeny = maradekos;} break;*/
            case "%":
                switch (n2) {
                    case 0:
                        uzenet = "Nullával nem lehet maradékosan osztani";
                        break;
                    default:
                        eredmeny = maradekos;
                } break;
            default:
                uzenet = "Valamit nem jól adtál meg";
        }
        /*
         if (c.equals("+")) {
         eredmeny = osszeadas;
         } else if (c.equals("-")) {
         eredmeny = kivonas;
         } else if (c.equals("*")) {
         eredmeny = szorzas;
         } else if (c.equals("/") && n2 != 0) {
         eredmeny = osztas;
         } else if (c.equals("/") && n2 == 0) {
         uzenet = "Nullával nem lehet osztani!";
         } else if (c.equals("%") && n2 != 0) {
         eredmeny = maradekos;
         } else if (c.equals("%")&& n2 ==0){uzenet = "Nullával nem lehet maradékosan osztani!";}
         else {
         uzenet = "Valamit nem jól adtál meg.";
         }*/
        System.out.println(uzenet.equals("") ? "Eredmény: " + eredmeny : uzenet);
    }
}
