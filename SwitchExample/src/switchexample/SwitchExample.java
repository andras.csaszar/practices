/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package switchexample;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class SwitchExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        
        if (sc.hasNextInt()){
        System.out.println("Adjon meg egy osztályzatot!");

        int number = sc.nextInt();
        switch (number) {
            case 5:
                System.out.println("Jeles");
                break;
            case 4:
                System.out.println("Jó");
                break;
            case 3:
                System.out.println("Közepes");
                break;
            case 2:
                System.out.println("Elégséges");
                break;
            default:
                System.out.println("Elégtelen");

        }

         switch (number) {
            case 5:
                
               
            case 4:
                System.out.println("Megfelelt");
                break;
            case 3:
                
                
            case 2:
            case 1: System.out.println("Nem felelt meg");    
                
            default:
                System.out.println("Nem 1-5-öt adtál meg.");

        }
        
        
        
        
        
        if (number == 5) {
            System.out.println("Jeles");
        } else if (number == 4) {
            System.out.println("Jó");
        } else if (number == 3) {
            System.out.println("Közepes");
        } else if (number == 2) {
            System.out.println("Elégséges");
        } else {
            System.out.println("Elégtelen");
        }
    }

}
}