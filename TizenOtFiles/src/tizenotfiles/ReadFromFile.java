/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tizenotfiles;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author andra
 */
public class ReadFromFile {

    public String readFirstLine() {
        String firstRow = null;
        try (BufferedReader br = new BufferedReader(new FileReader("bh.txt"))) {
            
           firstRow = br.readLine();
           
        } catch (FileNotFoundException ex) {
            System.out.println(ex);
        } catch (IOException ex) {
            System.out.println(ex);
        }
        return firstRow;
    }
}
