/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tizenotfiles;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

/**
 *
 * @author andra
 */
public class WriteToFile {

    public void write(String str, String... line) {
        try (
                FileWriter fw = new FileWriter("bh.txt");
                PrintWriter pw = new PrintWriter(fw);) {
                
            pw.println(str);
            for (String s : Arrays.asList(line)) {
                pw.println(s);

            }
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }
}
