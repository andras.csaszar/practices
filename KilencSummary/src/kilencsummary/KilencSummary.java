/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kilencsummary;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author andra
 */
public class KilencSummary {

    static int sayLongest(int[] array) {
        int count = 0;
        int[] starts = new int[array.length];
        int sizeOf = -1;

        for (int i = 0; i < array.length - 1; i++) {

            if (array[i + 1] < array[i]) {
                starts[i] = i;
            }

        }

        Arrays.sort(starts);

        int[] longness = new int[starts.length];

        for (int i = 0; i < longness.length - 1; i++) {

            longness[i] = starts[i + 1] - starts[i];
        }
        int maxIndex = longness[0];
        int maxLongness = -1;
        for (int i = 0; i < longness.length; i++) {
            if (longness[i] > maxIndex) {
                maxIndex = i;
                maxLongness = longness[i];
            }
        }

        for (int i = maxIndex; i < maxLongness+1; i++){
            System.out.println(array[i]);
        }
        return maxLongness;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        int[] arr = {1, 2, 3, 1, 2, 5, 6, 8, 9, 7, 8, 9, 3, 4, 5, 6, 1, 2, 3};
        sayLongest(arr);
    }

}
