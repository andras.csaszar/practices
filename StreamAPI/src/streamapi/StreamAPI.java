package streamapi;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 *
 * @author andra
 */
public class StreamAPI {

    public static void main(String[] args) {

//        Stream.of("a1", "a2", "a3", "a4", "a5")
//                .filter(p -> {
//                    System.out.println("filter " + p);
//                    return true;
//                })
//                .forEach(p -> {
//                    System.out.println("forEach " + p);
//                });
        //////////////////////
        List<String> myList = Arrays.asList("a1", "a2", "b3");
        Stream<String> stream = myList.stream().filter(p -> p.startsWith("a"));

        stream.filter(p -> p.startsWith("b"));
       // stream.forEach(p -> System.out.println(p));

        // ez így nem megy
        /*
         Ha a stream-et referenciaként átadom, akkor a referencián csak 1 stream műveletet lehet elvégezni.
         Ha még egy et szeretnénk, akkor a stream visszatérést kell új referenciaként átadni.*/
        //csak így:
        System.out.println("Második példa");
        List<String> myList2 = Arrays.asList("a1", "a2", "b3");
        Stream<String> stream2 = myList2.stream().filter(p -> p.startsWith("a"));

        Stream<String> stream3 = stream2.filter(p -> p.startsWith("b"));
        stream3.forEach(p -> System.out.println(p));
                
                //.filter(p -> p.startsWith("b"));
        //stream3.filter(p -> p.startsWith("b")).forEach(p -> System.out.println(p));
///---------

        System.out.println("Harmadik példa");
        List<Integer> intList = Arrays.asList(1, 4, 4, 5, 6, 6, 7, 9, 102, 2, 8, 9, 1, 23, 34);
        long startTime = System.currentTimeMillis();
        intList.stream()
                .forEach(p -> System.out.println(p));
        long duration = System.currentTimeMillis() - startTime;
        System.out.println("duration: " + duration);

        System.out.println("Negyedik példa");

        List<Integer> intList2 = Arrays.asList(1, 4, 4, 5, 6, 6, 7, 9, 102, 2, 8, 9, 1, 23, 34);

        long startTime2 = System.currentTimeMillis();
        intList2.parallelStream()
                .forEach(p -> System.out.println(p));
        long duration2 = System.currentTimeMillis() - startTime;
        System.out.println("duration: " + duration2);

    }

}
