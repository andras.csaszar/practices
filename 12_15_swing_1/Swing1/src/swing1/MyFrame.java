/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swing1;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author czirjak_zoltan
 */
public class MyFrame extends JFrame {

    private final JButton jb = new JButton("Click!");
    private final JTextField jt = new JTextField("Write something!", 50);

    public MyFrame() {
        buildWindow();
    }

    private void buildWindow() {
        JPanel jp = new JPanel();
        jp.add(jb);
        jp.add(jt);

        jt.setEditable(false);

        jb.addActionListener(l -> {
            jt.setEditable(true);
            jt.setText("You can write something");
        });

        add(jp, BorderLayout.SOUTH);
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
}
