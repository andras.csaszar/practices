/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encryption.model;

import encryption.controller.Controller;

/**
 *
 * @author czirjak_zoltan
 */
public class EncryptedText implements Model {
    private Controller controller;
    
    private String result = new String();

    @Override
    public void setController(Controller c) {
        controller = c;
    }

    @Override
    public void setText(String s) {
        result = s;
        controller.notifyView();
    }

    @Override
    public String getText() {
        return result;
    }
    
    
}
