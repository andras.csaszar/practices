/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encryption.model;

import encryption.controller.Controller;

/**
 *
 * @author czirjak_zoltan
 */
public interface Model {
    
     void setController(Controller c);
     
     void setText(String s);
     
     String getText();
    
}
