/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encryption.controller;

/**
 *
 * @author czirjak_zoltan
 */
public interface Controller {
    
    void shiftTextByCharacter(String text, char inputCharacter);
    
    void notifyView();
}
