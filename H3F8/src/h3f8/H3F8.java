/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h3f8;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class H3F8 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*
         8. Olvass be 2 számot. Határzod meg, hogy a 2 szám által meghatározott koordináta melyik síknegyedben van.
         X≥0 és Y≥0 → Síknegyed=1
         X≥0 és Y<0 → Síknegyed=4
         X<0 és Y≥0 → Síknegyed=2
         X<0 és Y<0 → Síknegyed=3
         */

        Scanner sc = new Scanner(System.in);
        int x = 0;
        int y = 0;
        boolean isXValid;
        boolean isYValid;
        int quadrant; //síknegyed

        System.out.println("Adjon meg összesen 2 szmot, kiírom melyik síknegyedben van az általuk meghatározott koordináta.");
        do {
            System.out.println("Adja meg az első számot!(x=)");
            if (sc.hasNextInt()) {
                x = sc.nextInt();
                isXValid = true;
            } else {
                isXValid = false;
                sc.next();
            }

        } while (!isXValid);

        do {
            System.out.println("Adja meg a második számot!(y=)");
            if (sc.hasNextInt()) {
                y = sc.nextInt();
                isYValid = true;
            } else {
                isYValid = false;
                sc.next();
            }

        } while (!isYValid);

        if (x >= 0) {
            quadrant = (y >= 0) ? 1 : 4;
        } else {
            quadrant = (y >= 0) ? 2 : 3;
        }
        
        System.out.printf("A %d. síknegyedben vagyunk.\n", quadrant);
    }

}
