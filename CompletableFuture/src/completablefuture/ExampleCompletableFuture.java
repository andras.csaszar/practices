/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package completablefuture;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andra
 */
public class ExampleCompletableFuture {

    public static void printYe() {
        try {
            Thread.sleep(3000);
            System.out.println("YE");
        } catch (InterruptedException ex) {
            Logger.getLogger(ExampleCompletableFuture.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("printYe run");
    }

    public static void printHaha() {

        try {
            Thread.sleep(7_000);
            System.out.println("HAHA");
        } catch (InterruptedException ex) {
            Logger.getLogger(ExampleCompletableFuture.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("printHaha run");

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        long mainStartWithoutCF = System.currentTimeMillis();
        printYe();
        printHaha();
        long mainEndWithoutCF = System.currentTimeMillis();
        long mainDurationWithoutCF = mainEndWithoutCF - mainStartWithoutCF;
        System.out.println("Duration without CF: " + mainDurationWithoutCF);

        System.out.println("All");
        /*S*/ long mainStartWithCF = System.currentTimeMillis();

        long mainEndWithCF;
        long mainDurationWithCF;

        CompletableFuture<Void> cfAll2 = CompletableFuture.allOf(
                CompletableFuture.runAsync(ExampleCompletableFuture::printYe),
                CompletableFuture.runAsync(ExampleCompletableFuture::printHaha));

        mainEndWithCF = System.currentTimeMillis();
        mainDurationWithCF = mainEndWithCF - mainStartWithCF;
        System.out.println("MainDuration With CF: " + mainDurationWithCF);
        
        
        long getStartTime = System.currentTimeMillis();
        try {
            cfAll2.get();
        } catch (InterruptedException | ExecutionException ex) {
            Logger.getLogger(ExampleCompletableFuture.class.getName()).log(Level.SEVERE, null, ex);
        }
        long getEndTime = System.currentTimeMillis();
        long getDuration = getEndTime-getStartTime;
        System.out.println("GetDuration of CF: " + getDuration);
        

//        try {
//            Thread.sleep(7050);
//        } catch (InterruptedException ex) {
//            Logger.getLogger(ExampleCompletableFuture.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

}
