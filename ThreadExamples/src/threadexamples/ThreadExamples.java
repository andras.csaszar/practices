/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threadexamples;

/**
 *
 * @author andra
 */
public class ThreadExamples {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        StringStore stringStore = new StringStore();
    Producer p1 = new Producer(stringStore);
    Producer p2 = new Producer(stringStore);
    Producer p3 = new Producer(stringStore);
    
    Consumer c1 = new Consumer(stringStore);
    Consumer c2 = new Consumer(stringStore);
    Consumer c3 = new Consumer(stringStore);
    Consumer c4 = new Consumer(stringStore);
    Consumer c5 = new Consumer(stringStore);
    
    p1.start();
    p2.start();
    p3.start();
    
    c1.start();
    c2.start();
    c3.start();
    c4.start();
    c5.start();
    
    }
    
}
