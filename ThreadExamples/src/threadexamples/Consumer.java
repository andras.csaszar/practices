/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threadexamples;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andra
 */
public class Consumer extends Thread {
    
    private StringStore stringStore;
    
    public Consumer(StringStore stringStore) {
        
        this.stringStore = stringStore;
        
    }
    
    @Override
    public void run() {
        while (true) {
            
            try {
                Thread.sleep(2000);
                String s = "Consumer: " + System.currentTimeMillis() + " " + currentThread().getId() + " "
                        + stringStore.remove();
                
                System.out.println(s);
            } catch (InterruptedException ex) {
                Logger.getLogger(Consumer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
    
}
