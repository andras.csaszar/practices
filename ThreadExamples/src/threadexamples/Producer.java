/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threadexamples;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andra
 */
public class Producer extends Thread {

    private StringStore stringStore;
    private int counter;

    public Producer(StringStore stringStore) {

        this.stringStore = stringStore;

    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(1000);
                
                String s = System.currentTimeMillis() + " " + currentThread().getId()
                        + " " + counter++;
                stringStore.add(s);
                System.out.println("Producer: " + s);
            } catch (InterruptedException ex) {
                Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
