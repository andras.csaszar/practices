/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.ropdolgozat_2020_03_22;

/**
 *
 * @author andra
 */
public final class A {

    private int age =3;
    private String name = "singleton";
    private String category ="unique";
    private static A instance;

    private A() {
    }

    public static A getInstance() {
        if (instance == null) {
            instance = new A();
        }

        return instance;

    }

}
