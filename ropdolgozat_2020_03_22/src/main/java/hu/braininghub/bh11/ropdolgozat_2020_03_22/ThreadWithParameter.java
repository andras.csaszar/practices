/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.ropdolgozat_2020_03_22;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;

/**
 *
 * @author andra
 */
class nameWriterThread implements Runnable {

    private int numberOfTimes;

    public nameWriterThread(int numberOfTimes) {
        this.numberOfTimes = numberOfTimes;
    }

    @Override
    public void run() {
        for (int i = 0; i < numberOfTimes; i++) {

            System.out.println("Császár András");
        }
    }

}

public class ThreadWithParameter {

    public static void main(String[] args) {
        Runnable r = new nameWriterThread(4);
       // ExecutorService es = new ThreadPoolExecutor();

        Thread t = new Thread(r);
        t.start();

    }
}
