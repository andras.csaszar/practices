public class Car {

private String model;
private String plateNum;
private int maxSpeed;
private int length;
private int width;

		public class CarBuilder {
		private String model;
		private String plateNum;
		private int maxSpeed;
		private int length;
		private int width;
			
		public CarBuilder (int maxSpeed, int length){
			this.maxSpeed = maxSpeed;
			this.length = length;
			
			
		}	

		public CarBuilder setModel ( String model){
			
			this.model = model;
			return this;
			
		}

		public CarBuilder setPlateNum(String plateNum){
			this.plateNum = plateNum;
			return this;
			
			
		}
		public CarBuilder setWidth(int width){
				this.width = width;
				return this;
		}

		public Car build(CarBuilder carBuilder){
			return new Car(this);
			
		}
		
	private Car( CarBuilder carBuilder){
		this.model = carBuilder.model;
		this.plateNum = carBuilder.plateNum;
		this.maxSpeed = carBuilder.maxSpeed;
		this.length = carBuilder.length;
		this.width = carBuilder.width;
		
	}
		
	
}


}