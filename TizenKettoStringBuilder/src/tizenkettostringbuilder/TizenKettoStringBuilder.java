/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tizenkettostringbuilder;

/**
 *
 * @author andra
 */
public class TizenKettoStringBuilder {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        String s1 = " alma \t Korte\t";

        System.out.println("|" + s1 + "|");

        System.out.println("|" + s1.trim() + "|");

        System.out.println("|" + s1.trim().replace('a', 'X') + "|");

        System.out.println("|" + s1.trim().replace('a', 'X').indexOf('a') + "|");
        System.out.println("|" + s1.trim().replace('a', 'X').indexOf('t') + "|");

    }

}
