/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h3f5;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class H3F5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //  5. Írj programot, mely addig olvas be számokat a billentyűzetről, ameddig azok kisebbek, mint tíz.
        //    Írd ki ezek után a beolvasott számok összegét!

        Scanner sc = new Scanner(System.in);

        int number = 0;
        int sum = 0;
        boolean isInputValid;
        while (number < 10) {
            do {
                isInputValid = true;
                System.out.println("Adjon meg 10-nél kisebb számokat, bármennyit, és megadom az összegüket!");
                if (sc.hasNextInt()) {
                    number = sc.nextInt();
                } else {
                    isInputValid = false;
                    sc.next();
                }
            } while (/*number >= 10 ||*/!isInputValid);

            //System.out.println("Number: " + number);
            if (number < 10) {
                sum += number;
            }
            //System.out.println("Sum: "+ sum);
        }
        System.out.println("Sum: " + sum);

    }

}
