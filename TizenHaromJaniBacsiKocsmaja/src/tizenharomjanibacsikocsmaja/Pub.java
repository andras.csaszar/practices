/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tizenharomjanibacsikocsmaja;

/**
 *
 * @author andra
 */
public class Pub {

    private int availableSodaInML;
    private int availableWineInML;
    private final int MAX_DRUNK_ALCOHOL_IN_ML = 40;
    private final int MIN_AGE_TO_DRINK = 18;
    private final int SODA_PRICE_PER_ML = 5;
    private final int WINE_PRICE_PER_ML = 30;

    public Pub(int availableSodaInML, int availableWineInML) {

        setAvailableSodaInML(availableSodaInML);
        setAvailableWineInML(availableWineInML);

    }

    public void serveDrink(Person person, Drink drink) {
        int price = drink.getAmountOfSodaInML() * SODA_PRICE_PER_ML + drink.getAmountOfWineInML() * WINE_PRICE_PER_ML;    //Soda: 50 Ft / dl Wine: 300 Ft / dl

        boolean outOfStock = availableSodaInML < drink.getAmountOfSodaInML()
                || availableWineInML < drink.getAmountOfWineInML();

        boolean ageTooLow = person.getAge() < MIN_AGE_TO_DRINK && drink.getAlcoholContentInML() > 0;
        boolean isDrunk = person.getDrunkAlcohol() > MAX_DRUNK_ALCOHOL_IN_ML;
        boolean hasNoMoney = person.getMoney() < price;

        boolean canServe = !ageTooLow && !outOfStock && !isDrunk && !hasNoMoney;

        if (canServe) {
            availableSodaInML -= drink.getAmountOfSodaInML();
            availableWineInML -= drink.getAmountOfWineInML();

            person.setDrunkAlcohol(person.getDrunkAlcohol() + (double) (drink.getAlcoholContentInML()));
            person.decreaseMoney(price);
        } else {
            if (outOfStock) {
                System.out.println("OutOfStock - cannot serve this drink.");
            }
            if (ageTooLow) {
                System.out.printf("Person %s is under eligible age to drink Alcohol.\n", person);
            }
            if (isDrunk) {
                System.out.printf("Person %s is already drunk (%d) - cannot serve him/her.\n", person, person.getDrunkAlcohol());
            }

            if (hasNoMoney) {
                System.out.printf("Person %s has not enough money to pay this drink - cannot serve.\n", person);
            }

        }

    }

    public int getAvailableSodaInML() {
        return availableSodaInML;
    }

    public int getAvailableWineInML() {
        return availableWineInML;
    }

    public void setAvailableSodaInML(int availableSodaInML) {
        this.availableSodaInML = availableSodaInML;
    }

    public void setAvailableWineInML(int availableWineInML) {
        this.availableWineInML = availableWineInML;
    }

    @Override
    public String toString() {
        return "Pub{" + "availableSodaInML=" + availableSodaInML + ", availableWineInML=" + availableWineInML + ", maxDrunkAlcoholInML=" + MAX_DRUNK_ALCOHOL_IN_ML + ", minAgeToDrink=" + MIN_AGE_TO_DRINK + '}';
    }

}
