/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tizenharomjanibacsikocsmaja;

/**
 *
 * @author andra
 */
public class Person {

    private int age;
    private int money;
    private double drunkAlcohol;

    public Person(int age, int money, double drunkAlcohol) {
        setAge(age);
        setMoney(money);
        setDrunkAlcohol(drunkAlcohol);
    }

    public void decreaseMoney(int price) {
        setMoney(getMoney() - price);
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public double getDrunkAlcohol() {
        return drunkAlcohol;
    }

    public void setDrunkAlcohol(double drunkAlcohol) {
        this.drunkAlcohol = drunkAlcohol;
    }

    @Override
    public String toString() {
        return "Person("+"age="+age+", money="+money+" drunkAlcohol="+drunkAlcohol+')'; //To change body of generated methods, choose Tools | Templates.
    }

}
