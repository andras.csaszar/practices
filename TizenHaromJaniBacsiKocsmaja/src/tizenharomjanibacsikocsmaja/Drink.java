/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tizenharomjanibacsikocsmaja;

/**
 *
 * @author andra
 */
public class Drink {

    private int sizeInDL;
    private int amountOfSodaInML;
    private int amountOfWineInML;

    public Drink(int amountOfSodaInML, int amountOfWineInML) {
        setAmountOfSodaInML(amountOfSodaInML);
        setAmountOfWineInML(amountOfWineInML);
        setSizeInDL(getAmountOfSodaInML() / 10 + getAmountOfWineInML() / 10);
    }

    public double getAlcoholContentInML() {
        return (amountOfWineInML * 0.1) / (amountOfSodaInML + amountOfWineInML);
    }

    public int getSizeInDL() {
        return sizeInDL;
    }

    public void setSizeInDL(int sizeInDL) {
        this.sizeInDL = sizeInDL;
    }

    public int getAmountOfSodaInML() {
        return amountOfSodaInML;
    }

    public void setAmountOfSodaInML(int amountOfSodaInML) {
        this.amountOfSodaInML = amountOfSodaInML;
    }

    public int getAmountOfWineInML() {
        return amountOfWineInML;
    }

    public void setAmountOfWineInML(int amountOfWineInML) {
        this.amountOfWineInML = amountOfWineInML;
    }

    @Override
    public String toString() {
        return "Drink{" + "sizeInDL=" + sizeInDL + ", amountOfSodaInML=" + amountOfSodaInML + ", amountOfWineInML=" + amountOfWineInML + '}';
    }

}
