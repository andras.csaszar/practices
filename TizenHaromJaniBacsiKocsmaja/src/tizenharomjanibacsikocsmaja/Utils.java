/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tizenharomjanibacsikocsmaja;

/**
 *
 * @author andra
 */
public class Utils {

    public Utils() {
    }

    public static Drink getDrinkWithLowestAlcholContent(Drink[] drinks) {

        if (drinks == null || drinks.length == 0) {
            return null;
        }
        Drink[] drinksToExamine = new Drink[drinks.length];
        for (int i = 0; i < drinks.length; i++) {
            drinksToExamine[i] = drinks[i];
        }
        double minAlcCont = drinksToExamine[0].getAlcoholContentInML();
        int indexOfMinAlcCont = -1;
        for (int i = 0; i < drinksToExamine.length; i++) {
            if (minAlcCont > drinksToExamine[i].getAlcoholContentInML()) {
                minAlcCont = drinksToExamine[i].getAlcoholContentInML();
                indexOfMinAlcCont = i;
            }
        }
        return drinksToExamine[indexOfMinAlcCont];

    }

    public static Person getYoungestPerson(Person[] persons) {
        if (persons == null || persons.length == 0) {
            return null;
        }

        Person[] personsToExamine = new Person[persons.length];
        for (int i = 0; i < persons.length; i++) {
            personsToExamine[i] = persons[i];
        }
        int minAge = personsToExamine[0].getAge();
        int indexOfMinAge = -1;

        for (int i = 0; i < personsToExamine.length; i++) {
            if (minAge > personsToExamine[i].getAge()) {
                minAge = personsToExamine[i].getAge();
                indexOfMinAge = i;
            }
        }
        return personsToExamine[indexOfMinAge];
    }

}
