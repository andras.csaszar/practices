/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tizenharomjanibacsikocsmaja;

/**
 *
 * @author andra
 */
public class AppPub {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Pub JancsiBa = new Pub(1_000, 3_000);

        Person young = new Person(18, 50_000, 0);
        Person tooYoung = new Person(17, 4_000, 0);
        Person older = new Person(60, 34_000, 10);

        Drink soda = new Drink(30, 0);
        Drink froccs = new Drink(20, 30);
        Drink wine = new Drink(0, 40);

        System.out.println("\nBefore first round\n");

        System.out.println(JancsiBa);
        System.out.println(young);
        System.out.println(tooYoung);
        System.out.println(older);
        System.out.println(soda);
        System.out.println(froccs);
        System.out.println(wine);

        //first round
        JancsiBa.serveDrink(young, wine);
        JancsiBa.serveDrink(tooYoung, froccs);
        JancsiBa.serveDrink(young, froccs);
        JancsiBa.serveDrink(tooYoung, soda);
        JancsiBa.serveDrink(older, wine);
        JancsiBa.serveDrink(older, wine);

        System.out.println("\nAfter first round\n");

        System.out.println(JancsiBa);
        System.out.println(young);
        System.out.println(tooYoung);
        System.out.println(older);
        System.out.println(soda);
        System.out.println(froccs);
        System.out.println(wine);

        //second (same) round
        System.out.println("\nBefore second round\n");
        JancsiBa.serveDrink(young, wine);
        JancsiBa.serveDrink(tooYoung, froccs);
        JancsiBa.serveDrink(young, froccs);
        JancsiBa.serveDrink(tooYoung, soda);
        JancsiBa.serveDrink(older, wine);
        JancsiBa.serveDrink(older, wine);

        System.out.println("\nAfter second round\n");
        System.out.println(JancsiBa);
        System.out.println(young);
        System.out.println(tooYoung);
        System.out.println(older);
        System.out.println(soda);
        System.out.println(froccs);
        System.out.println(wine);
        
        //third (same) round
  
        JancsiBa.serveDrink(young, wine);
        JancsiBa.serveDrink(tooYoung, froccs);
        JancsiBa.serveDrink(young, froccs);
        JancsiBa.serveDrink(tooYoung, soda);
        JancsiBa.serveDrink(older, wine);
        JancsiBa.serveDrink(older, wine);

        System.out.println("\nAfter third round\n");
        System.out.println(JancsiBa);
        System.out.println(young);
        System.out.println(tooYoung);
        System.out.println(older);
        System.out.println(soda);
        System.out.println(froccs);
        System.out.println(wine);

    }

}
