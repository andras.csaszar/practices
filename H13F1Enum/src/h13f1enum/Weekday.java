/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h13f1enum;

/**
 *
 * @author andra
 */
public enum Weekday {

    MONDAY() {
            },
    TUESDAY() {
            },
    WEDNESDAY() {
            },
    THURSDAY() {
            },
    FRIDAY() {
            },
    SATURDAY() {
        @Override
        public boolean isWeekDay(){return false;}
            },
    SUNDAY() {
        @Override
        public boolean isWeekDay(){return false;}
            };

    public boolean isWeekDay() {
        return true;
    }

    public boolean isHoliday() {
        return !isWeekDay();
    }

    public void printWeek(Weekday day) {

        if (day.isWeekDay()) {
            System.out.println("Weekday.");
        } else {
            System.out.println("Holiday!!");
        }

    }

}
