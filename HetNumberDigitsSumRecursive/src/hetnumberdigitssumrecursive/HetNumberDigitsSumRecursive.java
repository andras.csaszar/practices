/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hetnumberdigitssumrecursive;

/**
 *
 * @author andra
 */
public class HetNumberDigitsSumRecursive {

    static int sumOfDigitOfNumber(int number) {

        int result = 0;
        String string = Integer.toString(number);
        int powTo = string.length() - 1;

        if (powTo < 0) {
            return result;
        }

        double placeValueOfFirstDigit = Math.pow(10, powTo);

        result += number / placeValueOfFirstDigit;

        sumOfDigitOfNumber(powTo);

        return result;
    }

    static int summNumbers(int num) {
        int res = 0;

        while (num > 0) {
            res += num % 10;
            num /= 10;

        }
        return res;

    }
    
    //Első n szám összege
    
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        System.out.println(summNumbers(101));
        
        System.out.println(sumOfDigitOfNumber(10111));
    }
    
    

}
