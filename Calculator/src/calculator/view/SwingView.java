/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator.view;

import calculator.controller.MyCalcController;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author andra
 */
public class SwingView extends JFrame implements CalcView {

    private MyCalcController controller;
    private final int NUMBER_OF_BUTTONS = 9;
    private final JTextField SCREEN = new JTextField(25);

    public SwingView() {
        buildView();
    }

    private JPanel generateButtons() {
        JPanel jpCentre = new JPanel();
        jpCentre.setLayout(new GridLayout(4, 3));
        int value = 1;
        for (int i = 0; i < NUMBER_OF_BUTTONS; i++) {
            JButton jb = new JButton(String.valueOf(value++));
            jpCentre.add(jb);
        }
        JButton plus = new JButton(Operators.ADD.toString());
        JButton multiply = new JButton(Operators.MULTIPLY.toString());
        JButton equal = new JButton(Operators.EQUAL.toString());
        jpCentre.add(plus);
        jpCentre.add(multiply);
        jpCentre.add(equal);

        return jpCentre;

    }

    private void buildView() {

        SCREEN.setEnabled(false);
        JPanel north = new JPanel();
        north.add(SCREEN);

        add(north, BorderLayout.NORTH);
        add(generateButtons(), BorderLayout.CENTER);

    }

    @Override
    public void setController(MyCalcController c) {

    }

    @Override
    public void setText(String s) {
    }

    @Override
    public void start() {
    }

}
