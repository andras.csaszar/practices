/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator.view;

import calculator.controller.MyCalcController;

/**
 *
 * @author andra
 */
public interface CalcView {

    void setController(MyCalcController c);

    void setText(String s);

    void start();
}
