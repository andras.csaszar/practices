/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator.view;

/**
 *
 * @author andra
 */
public enum Operators {

    ADD() {

                @Override
                public String toString() {
                    return "+";
                }
            },
    SUBTRACT() {
                @Override
                public String toString() {
                    return "-";
                }
            },
    MULTIPLY() {
                @Override
                public String toString() {
                    return "*";
                }
            },
    DIVIDE() {
                @Override
                public String toString() {
                    return "/";
                }
            },
    EQUAL() {
                @Override
                public String toString() {
                    return "=";
                }
            };

    @Override
    public String toString() {
        return "Operations{" + '}';
    }

}

