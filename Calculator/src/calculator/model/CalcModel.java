/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator.model;

import calculator.controller.MyCalcController;

/**
 *
 * @author andra
 */
public interface CalcModel {

    void setController(MyCalcController c);

    void setText(String s);

    String getText();
}
