/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h3f4;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class H3F4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //    4. Kérj be 2 számot a felhasználótól. A kisebbtől a nagyobbig írd ki a 11-re végződő számokat!
        Scanner sc = new Scanner(System.in);
        int number1 = -1;
        int number2 = -1;
        System.out.println("Adjon meg összesen 2 szmot, kiírom a kisebbtől a nagyobbig a 11-re végződőeket.");
        do {
            System.out.println("Adja meg az első számot!");
            if (sc.hasNextInt()) {
                number1 = sc.nextInt();
            } else {
                sc.next();
            }

        } while (number1 < 0);

        do {
            System.out.println("Adja meg a második számot!");
            if (sc.hasNextInt()) {
                number2 = sc.nextInt();
            } else {
                sc.next();
            }

        } while (number2 < 0);

        int fromNum = number1 <= number2 ? number1 : number2;
        int toNum = Math.max(number1, number2);
        int countCases=0;
        for (int i = 0; i < toNum; i++) {
            
            if (i % 100 == 11) {System.out.println(i); countCases++;}
            
            
        }
        if (countCases ==0) {System.out.println("Nincs a megadott intervallumban 11-re végződő szám.");}
    }

}
