/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h4f2;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class H4F2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        /*
         2.kérjük be egy pozitív egész számot (ellenőrzés), majd írjunk ki csillagokat az alábbi formában a megadott szám alapján: 
         a. 
         szam = 7
         *******
         ******
         *****
         ****
         ***
         **
         *
        
         b. 
         szam = 5 (ha páros a szám, vonjunk ki belőle egyet)
         *
         ***
         *****
         */
        //A
        Scanner sc = new Scanner(System.in);
        boolean isInputValid;
        int number = -1;
        do {
            isInputValid = true;
            System.out.println("Adjon meg egy pozitív egész számot!");

            if (sc.hasNextInt()) {
                number = sc.nextInt();
                if (number <= 0) {
                    isInputValid = false;
                }
            } else {
                sc.next();
                isInputValid = false;
            }

        } while (!isInputValid);

        for (int i = number; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                System.out.print("*");
            }
            System.out.println("");
        }

        //B
        int number2 = number % 2 == 0 ? number - 1 : number;
        int midPoint = number / 2+1;
        int plusMinusMidPoint=0;
        for (int i = 1; i < number2 + 1; i += 2) {
            plusMinusMidPoint = (number2-i)/2;
            for (int j = 1; j < number2+1; j++) {
               // System.out.print("j:"+j);
                if (j<plusMinusMidPoint+1 || j>number2-plusMinusMidPoint) {
                    System.out.print(" ");
                } else {System.out.print("*");}
            }
            System.out.println("");
        }
    }

}
