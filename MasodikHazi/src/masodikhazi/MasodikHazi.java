package masodikhazi;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class MasodikHazi {

    public static void main(String[] args) {
        // TODO code application logic here
        //I.
        System.out.println("Adjon meg egymás után 3 számot!");
        Scanner sc = new Scanner(System.in);
        int n1 = sc.nextInt();
        int n2 = sc.nextInt();
        int n3 = sc.nextInt();
        double atlag = (n1 + n2 + n3) / 3d;
        int legnagyobb;
        System.out.println("Átlag: " + atlag);

        if (n1 > n2 && n1 > n3) {
            legnagyobb = n1;
        } else if (n2 > n1 && n2 > n3) {
            legnagyobb = n2;
        } else {
            legnagyobb = n3;
        }
        System.out.println("A legnagyobb: " + legnagyobb);

        if (((n1 + n2) > n3) && ((n1 + n3) > n2) && ((n2 + n3) > n1)) {
            System.out.println("Háromszög lehet belőle.");
        } else {
            System.out.println("Háromszög NEM lehet belőle.");
        }
    }

}
