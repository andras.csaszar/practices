/*
 String str = " Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
 - A fenti string hány mondatból áll? 
 - A fenti stringben hányszor szerepel az "it" részlet
 - írjuk ki a fenti szövegben az első és az utolsó "it" közötti szövegrészletet
 - írjunk ki minden második szót
 - cseréljük le az 'a' betűket 'A' betükre
 - írjuk ki fordítva (StringBuilder metódus segítségével)
 */
package h10f1stringbuilder;

/**
 *
 * @author andra
 */
public class H10F1StringBuilder {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        System.out.println("---A fenti string hány mondatból áll?---\n");

        String text = " Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
        String repText = text.replace('.', '@');
        String delimiter = "@";

        String[] sentences = repText.split(delimiter, -5);
        System.out.printf("This text contains %d sentences.\n", sentences.length);
        for (String sentence : sentences) {
            System.out.println(sentence);
        }

        System.out.println("\n---A fenti stringben hányszor szerepel az \"it\" részlet?---\n");
        String[] itContainer = text.split("it", -5);
        int numberOfIt = itContainer.length - 1;
        System.out.println(numberOfIt);

        System.out.println("\n---írjuk ki a fenti szövegben az első és az utolsó \"it\" közötti szövegrészletet---\n");

        System.out.println(text.subSequence(text.indexOf("it") + "it".length(), text.lastIndexOf("it")));

        System.out.println("\n---írjunk ki minden második szót---\n");
        String[] words = text.trim().split(" ", -5);
        for (int i = 1; i < words.length; i = i + 2) {
            System.out.println(words[i].replace(".", "").replace(",", ""));
        }

        System.out.println("\n---cseréljük le az 'a' betűket 'A' betükre---\n");
        String aToA = text.replace("a", "A");
        System.out.println(aToA);

        System.out.println("\n---írjuk ki fordítva (StringBuilder metódus segítségével)---\n");
        StringBuilder sb = new StringBuilder(text).reverse();
        
        System.out.println(sb);
       
    }

}
