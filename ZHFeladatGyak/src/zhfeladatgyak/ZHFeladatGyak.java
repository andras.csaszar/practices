//Feladat
 /*
 10 x 10 -es tömb
 5 csillag legyen véletlen helyen (egy helyen lehet több csillag is, de az 1-nek számít)
 3 db kérdőjel ? egy helyen csak egy ? lehet
 fel kell szedni az elemeket j,b,l,f moozgássa
 addig nem lehet felszedni kérdőjeleket, amíg van fent * a pályán
 */
package zhfeladatgyak;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class ZHFeladatGyak {

    final static int NUMBER_OF_PLAYER = 1;
    final static int NUMBER_OF_ASTERIX = 5;
    final static int NUMBER_OF_QUESTIONMARK = 3;
    final static int SIZE = 10;

    final static char PLAYER = 'T';
    final static char ASTERIX = '*';
    final static char QUESTION_MARK = '?';
    final static char EMPTY = '\0';
    final static Scanner SCANNER = new Scanner(System.in);

    static boolean steppedOnBannedItem = false;
    static int playerX = 0;
    static int playerY = 0;
    static char[][] field = new char[SIZE][SIZE];

    static int drawNumberInRange(int from, int to) {
        return (int) (Math.random() * (to - from) + from);
    }

    static boolean isValidInput(int inputCharacter) {
        return inputCharacter == 'L' || inputCharacter == 'l' || inputCharacter == 'R'
                || inputCharacter == 'r' || inputCharacter == 'U' || inputCharacter == 'u'
                || inputCharacter == 'D' || inputCharacter == 'd';
    }

    static int getMoveFromUser() {
        //L - 76, l - 108
        //R - 82, r - 114 (n+32)
        //U - 85
        //D - 68
        int characterASCII = -1;

        do {
            if (SCANNER.hasNext()) {
                characterASCII = SCANNER.next().charAt(0);
            } else {
                SCANNER.nextLine();
            }

        } while (!isValidInput(characterASCII));
        return characterASCII;
    }

    static void placeAnItem(int x, int y, char character) {
        field[x][y] = character;
    }

    static void setPlayer() {
        field[playerX][playerY] = PLAYER;
    }

    static void setInitialPositions(int numberOfInstance, char character, boolean isRepeatable) {
        for (int i = 0; i < numberOfInstance; i++) {
            int x = drawNumberInRange(0, SIZE);
            int y = drawNumberInRange(0, SIZE);

            if (field[x][y] == EMPTY) {
                placeAnItem(x, y, character);
            } else if (field[x][y] == character && isRepeatable) {
                placeAnItem(x, y, character);
            } else if (field[x][y] == character && !isRepeatable) {
                i--;
            }
        }
    }

    static boolean isPlayerInLastRow() {
        return playerX == field.length - 1;
    }

    static boolean isPlayerInFirstRow() {
        return playerX == 0;
    }

    static boolean isPlayerInLastCol() {
        return playerY == field[0].length - 1;
    }

    static boolean isPlayerInFirstCol() {
        return playerY == 0;
    }

    static boolean isPlayerOnVerticalEdge() {

        return isPlayerInLastRow() || isPlayerInFirstRow();
    }

    static boolean isPlayerOnHorizontalEdge() {

        return isPlayerInLastRow() || isPlayerInFirstRow();
    }

    static boolean hasFoundCharacterOnField(char character) {

        boolean characterFound = false;
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (field[i][j] == character) {
                    characterFound = true;
                }
            }
        }
        return characterFound;
    }

    static boolean isBannedItemOnNextPlace(int x, int y) {
        return hasFoundCharacterOnField(ASTERIX) && field[x][y] == QUESTION_MARK;
    }

    static void handleUp() {

        if (!isPlayerInFirstRow()) {

            if (isBannedItemOnNextPlace(playerX - 1, playerY)) {

                placeAnItem(playerX, playerY, EMPTY);
                playerX--;
                placeAnItem(playerX, playerY, PLAYER);
                steppedOnBannedItem = true;

            } else {

                if (steppedOnBannedItem) {
                    placeAnItem(playerX, playerY, QUESTION_MARK);
                } else {
                    placeAnItem(playerX, playerY, EMPTY);
                }

                playerX--;
                placeAnItem(playerX, playerY, PLAYER);
                steppedOnBannedItem = false;
            }

        }
    }

    static void handleDown() {
        if (!isPlayerInLastRow()) {

            if (isBannedItemOnNextPlace(playerX + 1, playerY)) {

                placeAnItem(playerX, playerY, EMPTY);
                playerX++;
                placeAnItem(playerX, playerY, PLAYER);
                steppedOnBannedItem = true;

            } else {

                if (steppedOnBannedItem) {
                    placeAnItem(playerX, playerY, QUESTION_MARK);
                } else {
                    placeAnItem(playerX, playerY, EMPTY);
                }

                playerX++;
                placeAnItem(playerX, playerY, PLAYER);
                steppedOnBannedItem = false;
            }

        }

    }

    static void handleLeft() {
        if (!isPlayerInFirstCol()) {

            if (isBannedItemOnNextPlace(playerX, playerY - 1)) {

                placeAnItem(playerX, playerY, EMPTY);
                playerY--;
                placeAnItem(playerX, playerY, PLAYER);
                steppedOnBannedItem = true;

            } else {

                if (steppedOnBannedItem) {
                    placeAnItem(playerX, playerY, QUESTION_MARK);
                } else {
                    placeAnItem(playerX, playerY, EMPTY);
                }

                playerY--;
                placeAnItem(playerX, playerY, PLAYER);
                steppedOnBannedItem = false;
            }

        }

    }

    static void handleRight() {
        if (!isPlayerInLastCol()) {

            if (isBannedItemOnNextPlace(playerX, playerY + 1)) {

                placeAnItem(playerX, playerY, EMPTY);
                playerY++;
                placeAnItem(playerX, playerY, PLAYER);
                steppedOnBannedItem = true;

            } else {

                if (steppedOnBannedItem) {
                    placeAnItem(playerX, playerY, QUESTION_MARK);
                } else {
                    placeAnItem(playerX, playerY, EMPTY);
                }

                playerY++;
                placeAnItem(playerX, playerY, PLAYER);
                steppedOnBannedItem = false;
            }

        }

    }

    static void performMove(char character) {

        switch (character) {

            case 'u':
                handleUp();
                break;
            case 'd':
                handleDown();
                break;
            case 'l':
                handleLeft();
                break;
            case 'r':
                handleRight();
                break;

        }

    }

    static void printField() {
        System.out.println("----------------");
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (field[i][j] == EMPTY) {
                    System.out.print("_");
                }else{
                System.out.print(field[i][j]);}
            }
            System.out.println();
        }
        System.out.println("----------------");
    }

    static void errorCheckFieldContent() {

        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {

            }
        }
    }

    static void play() {

        //printField();
        while (hasFoundCharacterOnField(QUESTION_MARK)) {

            printField();
            performMove((char) getMoveFromUser());
        }
        SCANNER.close();
        printField();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        //setInitialPositions(NUMBER_OF_PLAYER, PLAYER, false);
        setPlayer();
        setInitialPositions(NUMBER_OF_QUESTIONMARK, QUESTION_MARK, false);
        setInitialPositions(NUMBER_OF_ASTERIX, ASTERIX, true);
        play();
    }

}
