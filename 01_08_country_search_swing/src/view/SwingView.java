package view;

import controller.Controller;
import java.awt.BorderLayout;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class SwingView extends JFrame implements View {
    private Controller controller;
    
    private JTextArea jTextArea;
    private JButton jButton;
    private JTextField jTextField;
    
    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void update(List<String> countries) {
        jTextArea.setText(countries.stream()
                .collect(Collectors.joining("\n")));
    }

    @Override
    public void enableView() {
        construct();
        
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
    
    private void construct() {
        jTextArea = new JTextArea(20, 100);
        JScrollPane scroll = new JScrollPane(jTextArea);
        jButton = new JButton("SEARCH");
        jTextField = new JTextField(20);
        
        JPanel p1 = new JPanel();
        p1.add(jTextField);
        p1.add(jButton);
        
        JPanel p2 = new JPanel();
        p2.add(scroll);
        
        add(p1, BorderLayout.NORTH);
        add(p2, BorderLayout.CENTER);
        
        jButton.addActionListener(e -> 
                controller.handleSearchButton(jTextField.getText()));
    }
    
}
