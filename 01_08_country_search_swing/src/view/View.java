package view;

import controller.Controller;
import java.util.List;


public interface View {
    
    void setController(Controller controller);
    void update(List<String> countries);
    void enableView();
}
