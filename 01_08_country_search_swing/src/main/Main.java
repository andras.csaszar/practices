
package main;

import controller.Controller;
import model.Model;
import view.SwingView;
import view.View;

/**
 *
 * @author Marton Petrekanics
 */
public class Main {
    public static void main(String[] args) {
        View v = new SwingView();
        Model m = new Model();
        Controller c = new Controller(v, m);
        
        v.enableView();
        
    }
}
