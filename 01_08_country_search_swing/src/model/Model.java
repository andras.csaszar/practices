package model;

import controller.Controller;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Model {

    private Controller controller;

    
    private List<String> countries = new ArrayList<>();
    private List<String> foundCountries = new ArrayList<>();

    public Model() {
        countries = read("countries.txt");

    }

    private List<String> read(String path) {
        File file = new File(path);
        List<String> names = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            names = br.lines().collect(Collectors.toList());
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            System.out.println("File not found!!!");

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return names;
    }

    public List<String> getCountries() {
        return countries;
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

    
    public List<String> getFoundCountries() {
        return foundCountries;
    }

    public void setFoundCountries(List<String> foundCountries) {
        this.foundCountries = foundCountries;
        controller.notifyView();

    }

}
