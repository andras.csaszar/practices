package controller;

import java.util.List;
import java.util.stream.Collectors;
import model.Model;

import view.View;

public class Controller {

    private View v;
    private Model m;

    public Controller(View v, Model m) {
        this.v = v;
        this.m = m;

        v.setController(this);
        m.setController(this);
    }

    public void notifyView() {
        v.update(m.getFoundCountries());
    }

    public void handleSearchButton(String text) {
        List<String> countries = m.getCountries();
        m.setFoundCountries(countries.stream()
                .filter(c -> c.contains(text))
                .collect(Collectors.toList())
        );

    }

}
