/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.ViewController;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author andra
 */
public class SwingView extends JFrame implements View {

    private ViewController controller;
    private JTextArea jTextArea;
    private JButton jButton;
    private JTextField jTextField;

    @Override
    public void setController(ViewController controller) {
        this.controller = controller;
    }

    @Override
    public void update() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void enableView() {
        construct();
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setVisible(true);
    }

    private void construct() {

        jTextArea = new JTextArea(500, 500);
        jButton = new JButton("GO");
        jTextField = new JTextField(20);

        JPanel jp1 = new JPanel();
        jp1.add(jTextField);
        jp1.add(jButton);

        JPanel jp2 = new JPanel();
        jp2.add(jTextArea);

        add(jp1, BorderLayout.NORTH);
        add(jp2, BorderLayout.CENTER);
        
        jButton.addActionListener(e-> controller.handleGoButton(jTextField.getText()));
    }
}
