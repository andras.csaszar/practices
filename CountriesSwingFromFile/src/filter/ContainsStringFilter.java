/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter;

/**
 *
 * @author andra
 */
public class ContainsStringFilter implements Filter {

    @Override
    public boolean filter(String input, String stored) {
        return stored.contains(input);
    }

}
