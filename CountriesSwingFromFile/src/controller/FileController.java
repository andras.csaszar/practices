/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import filter.ContainsStringFilter;
import java.util.List;
import java.util.stream.Collectors;
import model.Model;
import view.View;

/**
 *
 * @author andra
 */
public class FileController implements ViewController, ModelController {

    private final View view;
    private final Model model;

    public FileController(View view, Model model) {
        this.view = view;
        this.model = model;

        model.setController(this);
        view.setController(this);
    }

    @Override
    public void handleGoButton(String str) {
        getFilteredCountries(str).stream().forEach(System.out::println);
    }

    private List<String> getFilteredCountries(String input) {
        final ContainsStringFilter stf = new ContainsStringFilter();
        return model.getCountryNames().stream()
                .filter(cn -> stf.filter(input, cn)).collect(Collectors.toList());
    }

    @Override
    public void notifyView() {

    }

}
