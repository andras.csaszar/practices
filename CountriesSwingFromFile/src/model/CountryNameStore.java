/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.ModelController;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author andra
 */
public class CountryNameStore implements Model {

    private ModelController controller;
    private List<String> countries = new ArrayList<>();
    private List<String> filteredCountries = new ArrayList<>();

    @Override
    public void setController(ModelController controller) {
        this.controller = controller;
    }

    @Override
    public List<String> getCountryNames() {
        if (countries != null) {
            return countries;
        }
        return null;
    }

    @Override
    public void setCountryNames(List<String> countries) {
        this.countries = countries;
        
        controller.notifyView();
    }

}
