/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kilencprogfel21;

/**
 *
 * @author andra
 */
public class KilencProgFel21 {

    static int getRandomNumber(int from, int inclusiveTo) {

        return (int) (Math.random() * (inclusiveTo + 1 - from) + from);

    }

    static int[][] create2DArray(int size) {

        int[][] array = new int[size][size];
        return array;
    }

    static double[][] create2DArrayDouble(int size) {

        double[][] array = new double[size][size];
        return array;
    }

    static void fillUp2DArray(int[][] arr) {
        int from = 0;
        int to = 255;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {

                arr[i][j] = getRandomNumber(from, to);
            }

        }
    }

    static void printArray(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }
    
    static void printArray(double[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int[][] array = create2DArray(getRandomNumber(10, 15));
        fillUp2DArray(array);

        double[][] array2 = create2DArrayDouble(array.length);

        for (int i = 0; i < array2.length; i++) {

            for (int j = 0; j < array2[i].length; j++) {
                if ((i == 0 || i == array2.length - 1) || (j == 0 || j == array2[i].length - 1)) {
                    array2[i][j] = array[i][j];
                } else {
                    double avg = 0;
                    int sum = 0;
                    int count = 0;
                    for (int k = i-1; k < i+2; k++) {
                        for (int l = j-1; l < j+2; l++) {
                            sum += array[k][l];
                            count++;
                        }
                    }

                    avg = (double)sum / count ;
                    array2[i][j] = avg;

                }
            }
        }
        printArray(array);
        System.out.println("\n");
        printArray(array2);

    }

}
