/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.event.ActionEvent;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import model.Model;
import view.SwingView;

/**
 *
 * @author andra
 */
public class Controller {

    private Model m;
    private SwingView v;

    public Controller(Model m, SwingView v) {
        this.m = m;
        this.v = v;

        m.setController(this);
        v.setController(this);

    }

    public void handleSearch() {

        updateModel(getLogEntry());
        updateView(m.getEvents());  //mvp
    }

    private String getLogEntry() {

        String actionEvent = "Button pushed";
        String dateTime = LocalDateTime.now().toString();

        StringBuilder sb = new StringBuilder();

        sb.append(dateTime);
        sb.append(" ");
        sb.append(actionEvent.toString());
//        if (isLastHourInLogSame(dateTime)) {
//            sb.append("\n");
//        }
        return sb.toString();

    }

    private boolean isLastHourInLogSame(String dateTime) {
        String str;
        if (!m.getEvents().isEmpty()) {
            str = m.getEvents().get(m.getEvents().size() - 1);
        } else {
            str = "aaaT";
        }
        String[] stringArray = str.split("T");
        return stringArray[2].charAt(0) == dateTime.charAt(0) && stringArray[2].charAt(1) == dateTime.charAt(1);
    }

    private void updateModel(String newStrings) {
        m.addEvent(newStrings);
    }

    public void updateView(List<String> newStrings) {
        v.setResults(newStrings);
    }

}
