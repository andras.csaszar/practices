/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hetfibonacci;

/**
 *
 * @author andra
 */
public class HetFibonacci {

    static int recursiveFibon(int number) {
        if (number == 0) {
            return 0;
        }
        if (number == 1 || number == 2) {
            return 1;
        }
        
       return recursiveFibon(number-1)+recursiveFibon(number-2); 
        
    }
/*
    rf(5)
        rf(4)+rf(3)
            rf(3)
    */
    static int fibon(int number) {

        int tmp1 = 0;
        int tmp2 = 1;
        int sum = 0;

        for (int i = 0; i < number; i++) {

            sum = tmp1 + tmp2;
            tmp1 = tmp2;
            tmp2 = sum;

        }

        if (number == 0) {
            return 0;
        }
        if (number == 1 || number == 2) {
            return 1;
        }

        return sum;

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int number = 5;
        int tmp1 = 0;
        int tmp2 = 1;
        int sum = 0;

        for (int i = 0; i < number; i++) {

            sum = tmp1 + tmp2;
            tmp1 = tmp2;
            tmp2 = sum;

        }
        System.out.println(fibon(number));
        System.out.println(recursiveFibon(5));
    }

}
