/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package removespacefromstringwithloop;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.Iterator;
import java.util.stream.Collectors;

/**
 *
 * @author andra
 */
public class RemoveSpaceFromStringWithLoop {

    public static String removeSpaces(String str) {
        StringBuilder sb = new StringBuilder(str);
     
        while (sb.chars().filter(c -> Character.isSpaceChar(c)).findAny().isPresent()) {
            for (int i = 0; i < sb.length(); i++) {
                if (Character.isSpaceChar(sb.charAt(i))) {
                    sb.deleteCharAt(i);
                }
                System.out.printf(sb + "%d \n");

            }
        }

        return sb.toString();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        String str = removeSpaces(" sdf sd sdf sdf sdfsdfe ewewlkoi wepoierpo ö9823245ö9 epoksdf ékjdsf  eoijudsf lljds gg  s dsoiu   sfd élkj");
        System.out.println(str);

    }

}
