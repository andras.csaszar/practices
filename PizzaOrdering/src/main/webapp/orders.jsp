<%-- 
    Document   : orders
    Created on : 2020.01.24., 20:11:14
    Author     : andra
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h2> Order pizza</h2>
        <form method ="get" action="PizzaOrderingServlet">
            <p> Your name: <input type="text" name="name"/></p>
            <p> Delivery address: <input type="text" name="address"/></p>
            <p>
                <select name="pizzaname">
                    <option value="diablo">Diablo</option>
                    <option value="ham">Ham</option>
                    <option value="hawaii">Hawaii</option>
                    <option value="salami">Salami</option>
                </select>

            </p>
            <p>
                <input type="radio" name="size" value="small" checked> Small(20cm)<br>
                <input type="radio" name="size" value="mid"> Mid(25cm)<br>
                <input type="radio" name="size" value="big"> Big(35cm)
            </p>
            <input type="submit" value="Order pizza"/>

        </form>

        <% String orders = (String) request.getAttribute("orders");%>
        <table method="get" action="ListOrdersServlet">

            <thead>
                <tr>
                    <th>Orders</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td> <%= orders%></td>
                </tr>
            </tbody> 
        </table>


    </body>
</html>
