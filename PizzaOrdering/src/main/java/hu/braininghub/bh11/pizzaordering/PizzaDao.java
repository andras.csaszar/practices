/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.pizzaordering;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andra
 */
public class PizzaDao {

    private static final String URL = "jdbc:mysql://localhost:3306/pizzaorders?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String USER = "root";
    private static String PW = "Vilagbeke2020";
    private static String USE_DB_COMMAND = "use pizzaorders;\n";
    private static StringBuilder sb = new StringBuilder();

    public PizzaDao() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(PizzaDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void storeOrderInDB(String values) {

        String[] valuesArray = values.split("\\|");
        StringBuilder sbSql = new StringBuilder();

        sb = sb.delete(0, sb.length());
        //  sb.append(USE_DB_COMMAND);

        String sqlStart = "INSERT into orders (customer_name, delivery_address, ordered_pizza_name, ordered_pizza_size) VALUES(";

        for (int i = 0; i < valuesArray.length; i++) {
            sbSql.append("'" + valuesArray[i] + "'");
            if (i != valuesArray.length - 1) {
                sbSql.append(", ");
            }
        }

        String sqlMid = sbSql.toString();

        String sqlEnd = ");\n";

        String sql = sb.append(sqlStart).append(sqlMid).append(sqlEnd).toString();

        try (
                Connection conn = DriverManager.getConnection(URL, USER, PW);
                Statement stm = conn.createStatement();) {
            stm.executeUpdate(sql);

        } catch (SQLException ex) {
            Logger.getLogger(PizzaDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            showAllOrders();
        }

    }

    public String showAllOrders() {

        StringBuilder sb = new StringBuilder();

        String sql = "SELECT * FROM orders";

        try (
                Connection conn = DriverManager.getConnection(URL, USER, PW);
                Statement stm = conn.createStatement();) {
            ResultSet rs = stm.executeQuery(sql);
            sb.append("<table border= '1'>"
                    + "<thead> <tr>"
                    + "<th> Order ID </th>"
                    + "<th> Customer name </th>"
                    + "<th> Delivery address </th>"
                    + "<th> Ordered pizza name </th>"
                    + "<th> Ordered pizza size </th>"
                    + "</tr> </thead>"
            );
            sb.append("<tbody>");
            while (rs.next()) {
                sb
                        .append("<tr>")
                        //sb.append("Order ID: ")
                        .append("<td>")
                        .append(rs.getString("order_id"))
                        .append("</td>")
                        //.append("Customer name: ")
                        .append("<td>")
                        .append(rs.getString("customer_name"))
                        .append("</td>")
                        //.append("Delivery address: ")
                        .append("<td>")
                        .append(rs.getString("delivery_address"))
                        .append("</td>")
                        //.append("Ordered pizza name: ")
                        .append("<td>")
                        .append(rs.getString("ordered_pizza_name"))
                        .append("</td>")
                        //.append("Ordered pizza size: ")
                        .append("<td>")
                        .append(rs.getString("ordered_pizza_size"))
                        .append("</td>")
                        .append("</tr>");
            }
            sb.append("</tbody>"
                    + "</table>");
        } catch (SQLException ex) {
            Logger.getLogger(PizzaDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sb.toString();
    }

}
