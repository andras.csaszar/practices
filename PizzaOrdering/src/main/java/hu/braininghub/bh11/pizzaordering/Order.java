/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.pizzaordering;

/**
 *
 * @author andra
 */
public class Order {
    
    private String customerName;
    private String deliveryAddress;
    private String pizzaName;
    private String pizzaSize;

    public Order(String customerName, String deliveryAddress, String pizzaName, String pizzaSize) {
        this.customerName = customerName;
        this.deliveryAddress = deliveryAddress;
        this.pizzaName = pizzaName;
        this.pizzaSize = pizzaSize;
    }


    public String getCustomerName() {
        return customerName;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public String getPizzaName() {
        return pizzaName;
    }

    public String getPizzaSize() {
        return pizzaSize;
    }
    
    
}
