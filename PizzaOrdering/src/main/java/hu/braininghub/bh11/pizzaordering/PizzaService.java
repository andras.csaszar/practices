/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh11.pizzaordering;

import java.util.List;

/**
 *
 * @author andra
 */
public class PizzaService {
    
 private PizzaDao dao = new PizzaDao();
 private List<String> orders;
 
    public String getOrders(){
      return dao.showAllOrders();
    
    }
    
    public void placeOrder(Order order){
        dao.storeOrderInDB(order.getCustomerName()+"|"+order.getDeliveryAddress()+"|"+order.getPizzaName()+"|"+order.getPizzaSize());
    }
}
