package hu.braininghub.bh11.pizzaordering;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author andra
 */
public class PizzaOrderingServlet extends HttpServlet {
 private PizzaService pizzaService = new PizzaService();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
       
        
       request.setAttribute("orders", this.pizzaService.getOrders());
        this.pizzaService.placeOrder(
                new Order(
                        
                request.getParameter("name"),
                      //  request.getParameterValues("name")[0],
                request.getParameter("address"),
                request.getParameter("pizzaname"),
                request.getParameter("size")
                        
                ));
                
       request.setAttribute("orders", this.pizzaService.getOrders());
       request.getRequestDispatcher("orders.jsp").forward(request, response);
       
    }



}
