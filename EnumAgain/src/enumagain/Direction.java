/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumagain;

/**
 *
 * @author andra
 */
public enum Direction implements Step {

    LEFT("LEFT") {
                @Override
                public void step() {
                }

                public void m() {
                }

            },
    RIGHT("RIGHT") {
                @Override
                public void step() {
                }
            },
    UP("UP") {
                @Override
                public void step() {
                }
            },
    DOWN("DOWN") {
                @Override
                public void step() {
                }
            };

    private final String text;

    private Direction(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

}
