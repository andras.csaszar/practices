/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collector;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author andra
 */
public class FileCollector {

    public List<File> collect(File path) {
        if (path == null || !path.isDirectory()) {
            return new ArrayList<>();
        }

        return Arrays.stream(path.listFiles())
                .filter(File::isFile)
                .collect(Collectors.toList());

    }
}
