/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter.handler;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andra
 */
public class AgeLimitedFilter implements Filter {

    private static final String USER_HOME = System.getProperty("user.home");

    @Override
    public boolean filter(File file) {
        return  convertToDayfromMilliSec(file.lastModified()) < 2D;
    }

    @Override
    public boolean test(File file) {

        return file.getPath().contains(USER_HOME);

    }

    private double convertToDayfromMilliSec(long value) {
        return value / 1000D / 60D / 60D / 24D;
    }

}
