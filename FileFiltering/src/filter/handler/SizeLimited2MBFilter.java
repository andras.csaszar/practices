/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter.handler;

import java.io.File;

/**
 *
 * @author andra
 */
public class SizeLimited2MBFilter implements Filter {

    @Override
    public boolean filter(File file) {
        return convertToMegabyteFromByte(file.length()) < 2D;
    }

    private double convertToMegabyteFromByte(long value) {
        return value / 1024D / 1024D;
    }

    @Override
    public boolean test(File file) {
        return true;
    }

}
