/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankinghub;

/**
 *
 * @author andra
 */
public abstract class BankAccount {

    private final Client client;
    protected int cashWithdrawalFee = BankCard.cashWithdrawal;
    protected int accountManagementFee; 
    protected int transferFee;
    protected double balance;
    static protected int accountNumber;
            
    protected final int CASH_DEPOSIT_FEE = 0;
    protected final int ACCOUNT_OPENING_FEE = 500;

    public static int getAccountNumber() {
        return accountNumber;
    }

    public static void setAccountNumber(int accountNumber) {
        BankAccount.accountNumber = accountNumber;
    }
    
    public BankAccount(Client client){
        this.client = client;
        setAccountNumber(accountNumber++);
    }
    
    public Client getClient() {
        return client;
    }

    public int getCashWithdrawalFee() {
        return cashWithdrawalFee;
    }

    public void setCashWithdrawalFee(int cashWithdrawalFee) {
        this.cashWithdrawalFee = cashWithdrawalFee;
    }

    public int getAccountManagementFee() {
        return accountManagementFee;
    }

    public void setAccountManagementFee(int accountManagementFee) {
        this.accountManagementFee = accountManagementFee;
    }

    public int getTransferFee() {
        return transferFee;
    }

    public void setTransferFee(int transferFee) {
        this.transferFee = transferFee;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

}
