/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otsentencestoread;

import java.util.Scanner;

/**
 *
 * @author andra
 */
public class OtSentencesToRead {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Scanner sc = new Scanner(System.in);

        String actualInput = "";
        String allInputs = "";
        int count = 0;
        String maxWord = "";
        int posOfMaxWord = 0;

        do {
            actualInput = "";
            if (sc.hasNext()) {
                actualInput = sc.next();
                allInputs += actualInput;

                count++;
                if (actualInput.length() > maxWord.length()) {
                    maxWord = actualInput;
                    posOfMaxWord = count;
                }
            } else {
                sc.next();

            }

        } while (!actualInput.equals("exit"));

        System.out.printf("Leghosszabb szó:" + maxWord + "- ez a %d. helyen állt", posOfMaxWord);

    }

}
