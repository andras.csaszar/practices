/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kilenckulonfel100szamgen;

import java.util.Arrays;

/**
 *
 * @author andra
 */
public class KilencKulonFel100SzamGen {

    static int generateNumber() {
        return (int) (Math.random() * 100);
    }

    static void printArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {

            System.out.println(arr[i]);
        }

    }

    static int[] fillUpArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {

            int current = generateNumber();

            if (current <= arr[i]) {
                i--;
            } else {
                arr[i] = current;
            }
            sortArray(arr);
        }
        return arr;
    }

    static void sortArray(int[] arr) {
        //  printArray(arr);

        Arrays.sort(arr);
        // printArray(arr);

        for (int i = 0; i < arr.length; i++) {
            int tmp = arr[i];
            arr[i] = arr[arr.length - i - 1];
            arr[arr.length - i - 1] = tmp;
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int[] arrayMine = new int[10];

        System.out.println("1.array");
        printArray(arrayMine);
        fillUpArray(arrayMine);
        System.out.println("2.array");
        printArray(arrayMine);

    }

}
