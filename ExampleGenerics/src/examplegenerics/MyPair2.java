/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examplegenerics;

/**
 *
 * @author andra
 */
public class MyPair2<T, U extends Number> {
    
    private T t;
    private U u;

    public MyPair2(T t, U u) {
        this.t = t;
        this.u = u;
    }

    public T getLeft() {
        return t;
    }

    public void setLeft(T t) {
        this.t = t;
    }

    public U getRight() {
        return u;
    }

    public void setRight(U u) {
        this.u = u;
    }
    
    
    
    
}
